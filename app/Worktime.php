<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worktime extends Model
{
    public function hasUser()
    {
        return $this->hasOne(\App\User::class,'id','user_id');
    }
}
