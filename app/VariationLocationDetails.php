<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariationLocationDetails extends Model
{
    public function has_variation()
    {
        return $this->belongsTo(\App\Variation::class,'variation_id','id');
    }
}
