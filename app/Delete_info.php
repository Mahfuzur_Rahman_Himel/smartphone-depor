<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delete_info extends Model
{
    public function hasDeleted()
    {
        return $this->belongsTo(\App\User::class, 'delete_by');
    }
    public function scopeSell($query)
    {
        return $query->where('delete_for', 'sell');
    }
    public function scopeSellReturn($query)
    {
        return $query->where('delete_for', 'sell_return');
    }
}
