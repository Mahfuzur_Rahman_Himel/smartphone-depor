<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_transaction_list extends Model
{
    public function scopePurchase($query)
    {
        return $query->where('transaction_for', 'purchase');
    }
    public function contact()
    {
        return $this->belongsTo(\App\Contact::class, 'contact_id');
    }
    public function hasDeleted()
    {
        return $this->belongsTo(\App\User::class, 'deleted_by');
    }
    public function transaction_items()
    {
        return $this->hasMany(\App\Deleted_transaction_item::class,'deleted_transaction_lists_id','id');
    }
    public function payments()
    {
        return $this->hasMany(\App\Deleted_payment::class,'deleted_transaction_lists_id','id');
    }

}
