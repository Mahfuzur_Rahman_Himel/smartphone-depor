<?php

namespace App\Http\Controllers;

use App\Deleted_transaction_item_return;
use Illuminate\Http\Request;

class DeletedTransactionItemReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deleted_transaction_item_return  $deleted_purchase_item_return
     * @return \Illuminate\Http\Response
     */
    public function show(Deleted_transaction_item_return $deleted_purchase_item_return)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deleted_transaction_item_return  $deleted_purchase_item_return
     * @return \Illuminate\Http\Response
     */
    public function edit(Deleted_transaction_item_return $deleted_purchase_item_return)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deleted_transaction_item_return  $deleted_purchase_item_return
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deleted_transaction_item_return $deleted_purchase_item_return)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deleted_transaction_item_return  $deleted_purchase_item_return
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deleted_transaction_item_return $deleted_purchase_item_return)
    {
        //
    }
}
