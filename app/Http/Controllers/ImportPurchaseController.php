<?php

namespace App\Http\Controllers;

use App\AccountTransaction;
use App\Business;
use App\BusinessLocation;
use App\Contact;
use App\CustomerGroup;
use App\Product;
use App\PurchaseLine;
use App\TaxRate;
use App\Transaction;
use App\User;
use App\Utils\BusinessUtil;

use App\Utils\ModuleUtil;
use App\Utils\ProductUtil;
use App\Utils\TransactionUtil;

use App\Variation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;


use App\Brands;
use App\Category;
use App\Unit;
use App\VariationValueTemplate;

use Excel;

class ImportPurchaseController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $productUtil;
    protected $transactionUtil;
    protected $moduleUtil;

    private $barcode_types;

    /**
     * Constructor
     *
     * @param ProductUtils $product
     * @return void
     */
    public function __construct(ProductUtil $productUtil, TransactionUtil $transactionUtil, BusinessUtil $businessUtil, ModuleUtil $moduleUtil)
    {
        $this->productUtil = $productUtil;
        $this->transactionUtil = $transactionUtil;
        $this->businessUtil = $businessUtil;
        $this->moduleUtil = $moduleUtil;

        $this->dummyPaymentLine = ['method' => 'cash', 'amount' => 0, 'note' => '', 'card_transaction_number' => '', 'card_number' => '', 'card_type' => '', 'card_holder_name' => '', 'card_month' => '', 'card_year' => '', 'card_security' => '', 'cheque_number' => '', 'bank_account_number' => '',
            'is_return' => 0, 'transaction_no' => ''];

        //barcode types
        $this->barcode_types = $this->productUtil->barcode_types();

    }

    /**
     * Display import product screen.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('purchase.create')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');

        //Check if subscribed or not
        if (!$this->moduleUtil->isSubscribed($business_id)) {
            return $this->moduleUtil->expiredResponse();
        }

        $taxes = TaxRate::where('business_id', $business_id)
            ->get();
        $orderStatuses = $this->productUtil->orderStatuses();
        $business_locations = BusinessLocation::forDropdown($business_id);

        $currency_details = $this->transactionUtil->purchaseCurrencyDetails($business_id);

        $default_purchase_status = null;
        if (request()->session()->get('business.enable_purchase_status') != 1) {
            $default_purchase_status = 'received';
        }

        $types = [];
        if (auth()->user()->can('supplier.create')) {
            $types['supplier'] = __('report.supplier');
        }
        if (auth()->user()->can('customer.create')) {
            $types['customer'] = __('report.customer');
        }
        if (auth()->user()->can('supplier.create') && auth()->user()->can('customer.create')) {
            $types['both'] = __('lang_v1.both_supplier_customer');
        }
        $customer_groups = CustomerGroup::forDropdown($business_id);

        $business_details = $this->businessUtil->getDetails($business_id);
        $shortcuts = json_decode($business_details->keyboard_shortcuts, true);

        $payment_line = $this->dummyPaymentLine;
        $payment_types = $this->productUtil->payment_types();

        //Accounts
        $accounts = $this->moduleUtil->accountsDropdown($business_id, true);

        return view('purchase.import')
            ->with(compact('taxes', 'orderStatuses', 'business_locations', 'currency_details', 'default_purchase_status', 'customer_groups', 'types', 'shortcuts', 'payment_line', 'payment_types', 'accounts'));
    }

    /**
     * Imports the uploaded file to database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function purchase_store(Request $request)
    {
        if (!auth()->user()->can('purchase.create')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $business_id = $request->session()->get('user.business_id');

            //Check if subscribed or not
            if (!$this->moduleUtil->isSubscribed($business_id)) {
                return $this->moduleUtil->expiredResponse(action('PurchaseController@index'));
            }

            $transaction_data = $request->only(['ref_no', 'status', 'contact_id', 'transaction_date', 'total_before_tax', 'location_id', 'discount_type', 'discount_amount', 'tax_id', 'tax_amount', 'shipping_details', 'shipping_charges', 'final_total', 'additional_notes', 'exchange_rate']);

            $exchange_rate = $transaction_data['exchange_rate'];

            //Reverse exchange rate and save it.
            //$transaction_data['exchange_rate'] = $transaction_data['exchange_rate'];

            //TODO: Check for "Undefined index: total_before_tax" issue
            //Adding temporary fix by validating
            $request->validate([
                'status' => 'required',
                'contact_id' => 'required',
                'transaction_date' => 'required',
                'total_before_tax' => 'required',
                'location_id' => 'required',
                'final_total' => 'required',
                'document' => 'file|max:' . (config('constants.document_size_limit') / 1000)
            ]);

            $user_id = $request->session()->get('user.id');
            $enable_product_editing = $request->session()->get('business.enable_editing_product_from_purchase');

            //Update business exchange rate.
            Business::update_business($business_id, ['p_exchange_rate' => ($transaction_data['exchange_rate'])]);

            $currency_details = $this->transactionUtil->purchaseCurrencyDetails($business_id);

            //unformat input values
            $transaction_data['total_before_tax'] = $this->productUtil->num_uf($transaction_data['total_before_tax'], $currency_details) * $exchange_rate;

            // If discount type is fixed them multiply by exchange rate, else don't
            if ($transaction_data['discount_type'] == 'fixed') {
                $transaction_data['discount_amount'] = $this->productUtil->num_uf($transaction_data['discount_amount'], $currency_details) * $exchange_rate;
            } elseif ($transaction_data['discount_type'] == 'percentage') {
                $transaction_data['discount_amount'] = $this->productUtil->num_uf($transaction_data['discount_amount'], $currency_details);
            } else {
                $transaction_data['discount_amount'] = 0;
            }

            $transaction_data['tax_amount'] = $this->productUtil->num_uf($transaction_data['tax_amount'], $currency_details) * $exchange_rate;
            $transaction_data['shipping_charges'] = $this->productUtil->num_uf($transaction_data['shipping_charges'], $currency_details) * $exchange_rate;
            $transaction_data['final_total'] = $this->productUtil->num_uf($transaction_data['final_total'], $currency_details) * $exchange_rate;

            $transaction_data['business_id'] = $business_id;
            $transaction_data['created_by'] = $user_id;
            $transaction_data['type'] = 'purchase';
            $transaction_data['payment_status'] = 'due';
            $transaction_data['transaction_date'] = $this->productUtil->uf_date($transaction_data['transaction_date'], true);

            //upload document
            $transaction_data['document'] = $this->transactionUtil->uploadFile($request, 'document', 'documents');

            DB::beginTransaction();

            //Update reference count
            $ref_count = $this->productUtil->setAndGetReferenceCount($transaction_data['type']);
            //Generate reference number
            if (empty($transaction_data['ref_no'])) {
                $transaction_data['ref_no'] = $this->productUtil->generateReferenceNumber($transaction_data['type'], $ref_count);
            }

            $transaction = Transaction::create($transaction_data);

            $purchase_lines = [];
            $purchases = $request->input('purchases');

            $this->productUtil->createOrUpdatePurchaseLines($transaction, $purchases, $currency_details, $enable_product_editing);

            //Add Purchase payments
            $this->transactionUtil->createOrUpdatePaymentLines($transaction, $request->input('payment'));

            //update payment status
            $this->transactionUtil->updatePaymentStatus($transaction->id, $transaction->final_total);

            //Adjust stock over selling if found
            $this->productUtil->adjustStockOverSelling($transaction);

            DB::commit();

            $output = ['success' => 1,
                'msg' => __('purchase.purchase_add_success')
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            $output = ['success' => 0,
                'msg' => __('messages.something_went_wrong')
            ];
        }

        return redirect('purchases')->with('status', $output);
    }

    public function store(Request $request)
    {
//        dd($request->all());
        if (!auth()->user()->can('purchase.create')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            //Set maximum php execution time
            ini_set('max_execution_time', 0);

            $totalBeforeTax = 0;
            $finalTotal = 0;
            $business_id = $request->session()->get('user.business_id');

            $transaction_data = $request->only(['ref_no', 'status', 'contact_id', 'transaction_date', 'total_before_tax', 'location_id', 'discount_type', 'discount_amount', 'tax_id', 'tax_amount', 'shipping_details', 'shipping_charges', 'final_total', 'additional_notes', 'exchange_rate']);

            $exchange_rate = $transaction_data['exchange_rate'];

            //Reverse exchange rate and save it.
            //$transaction_data['exchange_rate'] = $transaction_data['exchange_rate'];

            //TODO: Check for "Undefined index: total_before_tax" issue
            //Adding temporary fix by validating
            $request->validate([
                'status' => 'required',
                'contact_id' => 'required',
                'transaction_date' => 'required',
                'total_before_tax' => 'required',
                'location_id' => 'required',
                'final_total' => 'required',
                'document' => 'file|max:' . (config('constants.document_size_limit') / 1000),
                'products_csv' => 'file|max:' . (config('constants.document_size_limit') / 1000)
            ]);

            $enable_product_editing = $request->session()->get('business.enable_editing_product_from_purchase');

            //Update business exchange rate.
            Business::update_business($business_id, ['p_exchange_rate' => ($transaction_data['exchange_rate'])]);

            $currency_details = $this->transactionUtil->purchaseCurrencyDetails($business_id);

            if ($request->hasFile('products_csv')) {
                $file = $request->file('products_csv');
                $imported_data = Excel::load($file->getRealPath())
                    ->noHeading()
                    ->skipRows(1)
                    ->get()
                    ->toArray();

                $user_id = $request->session()->get('user.id');
                $default_profit_percent = $request->session()->get('business.default_profit_percent');

                $formated_data = [];

                $is_valid = true;
                $error_msg = '';

                DB::beginTransaction();
                foreach ($imported_data as $key => $value) {

                    //Check if any column is missing
                    if (count($value) != 6) {
                        $is_valid = false;
                        $error_msg = "Some of the columns are missing. Please, use latest CSV file template.";
                        break;
                    }

                    $row_no = $key + 1;
                    $product_array = [];
                    $product_array['business_id'] = $business_id;
                    $product_array['created_by'] = $user_id;

                    //quantity
                    $prdctQuantity = trim($value[2]);
                    if (!empty($prdctQuantity)) {
                    } else {
                        $is_valid = false;
                        $error_msg = "Quantity is required in row no. $row_no";
                        break;
                    }

                    //unit price
                    $prdctUnitPrice = trim($value[3]);
                    if (!empty($prdctUnitPrice)) {
                    } else {
                        $is_valid = false;
                        $error_msg = "Unit price(before discount) is required in row no. $row_no";
                        break;
                    }
                    //sku
                    $product_sku = trim($value[0]);
                    $productID = '';
                    if (!empty($product_sku)) {
                        $chkSKU = Product::with(['variations' => function ($query) {
                            $query->orderby('id', 'desc');
                            $query->limit(1);
                        }])
                            ->where('business_id', $business_id)
                            ->where('sku', $product_sku)
                            ->first();

                        if (!empty($chkSKU)) {
                            $productID = $chkSKU->id;
                            if (sizeof($chkSKU->variations) == 0) {
                                $is_valid = false;
                                $error_msg = "PRODUCT variation info does not found on System for row no. $row_no";
                                break;
                            } else {
                                $variation_default_purchase_price = $chkSKU->variations[0]->default_purchase_price;
                                $variation_dpp_inc_tax = $chkSKU->variations[0]->dpp_inc_tax;
                                $variation_profit_percent = $chkSKU->variations[0]->profit_percent;
                                $variation_default_sell_price = $chkSKU->variations[0]->default_sell_price;
                                $variation_sell_price_inc_tax = $chkSKU->variations[0]->sell_price_inc_tax;
                            }
                        } else {
                            $is_valid = false;
                            $error_msg = "PRODUCT SKU does not found on System in row no. $row_no";
                            break;
                        }
                    } else {
                        $is_valid = false;
                        $error_msg = "PRODUCT SKU is required in row no. $row_no";
                        break;
                    }

                    $product = Product::where('id', $productID)->first();

                    $isIMEII = 0;
                    $tmpCategoryInfo = Category::where('id', $product->category_id)->first();
                    if ($tmpCategoryInfo) {
                        $isIMEII = $tmpCategoryInfo->is_imei;
                    }

                    if ($isIMEII == 1) {
                        //IMEI
                        $product_imei = trim($value[1]);
                        if (!empty($product_imei)) {

                            $chkDuplicate = false;
                            $inner_row = 0;
                            //check duplicate value on csv
                            foreach ($imported_data as $inner_key => $inner_value) {
                                if ($key != $inner_key && $product_imei == trim($inner_value[1])) {
                                    $chkDuplicate = true;
                                    $inner_row = $inner_key + 1;
                                }
                            }

                            if ($chkDuplicate) {
                                $is_valid = false;
                                $error_msg = "Same PRODUCT IMEI in row no. $row_no and row no. $inner_row ";
                                break;
                            } else {
                                $chkIMEI = VariationValueTemplate::where('name', $product_imei)
                                    ->first();
                                if (empty($chkIMEI)) {
                                    if ($product->type == 'single') {
                                        $product->type = 'variable';
                                        $product->save();
                                    }
                                } else {
                                    $is_valid = false;
                                    $error_msg = "PRODUCT IMEI in row no. $row_no already exist on System";
                                    break;
                                }
                            }
                        } else {
                            $is_valid = false;
                            $error_msg = "PRODUCT IMEI is required in row no. $row_no";
                            break;
                        }

                        $tax_amount = $product->tax;
                        $tax_type = $product->tax_type;
                        $enable_stock = $product->enable_stock;

                        $variation_name = 'IMEI';
                        $variation_values_string = array($product_imei);
                        //Variation values
                        $variation_values = $variation_values_string;
                        $dpp_inc_tax = [];
                        $dpp_exc_tax = [];
                        $selling_price = [];
                        $profit_margin = [];
                        foreach ($variation_values as $k => $v) {
                            $dpp_inc_tax[$k] = $variation_dpp_inc_tax;
                            $dpp_exc_tax[$k] = $variation_dpp_inc_tax - $tax_amount;
                            $selling_price[$k] = $variation_default_sell_price;
                            $profit_margin[$k] = $variation_profit_percent;
                        }
                        $product_array['prdctID'] = $product->id;
                        $product_array['variation']['name'] = $variation_name;

                        //Check if variation exists or create new
                        $variation = $this->productUtil->createOrNewVariation($business_id, $variation_name);
                        $product_array['variation']['variation_template_id'] = $variation->id;

                        foreach ($variation_values as $k => $v) {
                            $variation_prices = $this->calculateVariationPrices($dpp_exc_tax[$k], $dpp_inc_tax[$k], $selling_price[$k], $tax_amount, $tax_type, $profit_margin[$k]);

                            //get variation value
                            $variation_value = $variation->values->filter(function ($item) use ($v) {
                                return strtolower($item->name) == strtolower($v);
                            })->first();

                            if (empty($variation_value)) {
                                $variation_value = VariationValueTemplate::create([
                                    'name' => $v,
                                    'variation_template_id' => $variation->id
                                ]);
                            }

                            //Assign Values
                            $product_array['variation']['variations'][] = [
                                'value' => $v,
                                'variation_value_id' => $variation_value->id,
                                'default_purchase_price' => $variation_prices['dpp_exc_tax'],
                                'dpp_inc_tax' => $variation_prices['dpp_inc_tax'],
                                'profit_percent' => $this->productUtil->num_f($profit_margin[$k]),
                                'default_sell_price' => $variation_prices['dsp_exc_tax'],
                                'sell_price_inc_tax' => $variation_prices['dsp_inc_tax']
                            ];
                        }

                        //Opening stock
                        if ($enable_stock == 1) {
                            $variation_os = array(0);

                            //$product_array['opening_stock_details']['quantity'] = $variation_os;

                            $location = BusinessLocation::where('business_id', $business_id)->first();
                            $product_array['variation']['opening_stock_location'] = $location->id;

                            foreach ($variation_os as $k => $v) {
                                $product_array['variation']['variations'][$k]['opening_stock'] = $v;
                                $product_array['variation']['variations'][$k]['opening_stock_exp_date'] = null;
                            }
                        }
                        //Assign to formated array
                        $formated_data[] = $product_array;
                    } else {
                        $product_imei = null;
                    }


                    //assign to purchase
                    $my_purchase['product_id'] = $product->id;
                    $my_purchase['variation_id'] = $product_imei;
                    if ($isIMEII == 1) {
                        $my_purchase['quantity'] = 1;
                        $prdctQuantity = 1;
                    } else {
                        $my_purchase['quantity'] = $prdctQuantity;
                    }

                    //discount percent
                    $prdctDiscountPercent = trim($value[4]);
                    if (empty($prdctDiscountPercent)) {
                        $prdctDiscountPercent = 0;
                    }
                    $prdctPriceBeforeTax = $prdctUnitPrice - ($prdctUnitPrice * $prdctDiscountPercent / 100);
                    $totalBeforeTax += $prdctPriceBeforeTax;
                    $finalTotal += $prdctPriceBeforeTax * $prdctQuantity;
                    //selling price
                    $prdctSellingPrice = trim($value[5]);
                    $prdctProfitMargin = 0;
                    if (!empty($prdctSellingPrice)) {
                    } else {
                        $prdctSellingPrice = 0;
                    }
                    $profitS = $prdctSellingPrice - $prdctPriceBeforeTax;
                    $prdctProfitMargin = ($profitS / $prdctPriceBeforeTax) * 100;

                    $my_purchase['product_unit_id'] = $product->unit_id;
                    $my_purchase['pp_without_discount'] = $prdctUnitPrice;
                    $my_purchase['discount_percent'] = $prdctDiscountPercent;
                    $my_purchase['purchase_price'] = $prdctPriceBeforeTax;
                    $my_purchase['purchase_line_tax_id'] = null;
                    $my_purchase['item_tax'] = 0;
                    $my_purchase['purchase_price_inc_tax'] = $prdctPriceBeforeTax + 0;
                    $my_purchase['profit_percent'] = $prdctProfitMargin;
                    $my_purchase['default_sell_price'] = $prdctSellingPrice;

                    $purchase_data[] = $my_purchase;
                }
//                dd($purchase_data);
                if (!$is_valid) {
                    throw new \Exception($error_msg);
                }

                if (!empty($formated_data)) {
                    foreach ($formated_data as $index => $product_data) {
                        $pdct = $product_data['prdctID'];
                        $variation_data = $product_data['variation'];
                        unset($product_data['variation']);

                        $opening_stock = null;
                        if (!empty($product_data['opening_stock_details'])) {
                            $opening_stock = $product_data['opening_stock_details'];
                        }
                        if (isset($product_data['opening_stock_details'])) {
                            unset($product_data['opening_stock_details']);
                        }

                        //Create variable product variations
                        $this->productUtil->createVariableProductVariations(
                            $pdct,
                            [$variation_data],
                            $business_id
                        );

                        if ($enable_stock == 1) {
                            $this->addOpeningStockForVariable($variation_data, $product, $business_id);
                        }
                    }
                }

                if (!empty($purchase_data)) {
                    $tmpIMEICategoryInfo = Category::where('is_imei', 1)->get(['id']);
                    foreach ($purchase_data as $index => $purchase_val) {
                        $p_tmp = Product::where('id', $purchase_val['product_id'])
                            ->whereIn('category_id', $tmpIMEICategoryInfo)
                            ->first();

                        if ($p_tmp) {
                            $tmp = VariationValueTemplate::where('name', $purchase_val['variation_id'])->first();
                            $variationValueID = $tmp->id;
                            $variationID = Variation::where('variation_value_id', $variationValueID)
//                                ->where('product_id', $purchase_val['product_id'])
                                ->first();
                            if ($variationID) {
                                $purchase_data[$index]['variation_id'] = $variationID->id;
                            } else {
                                throw new \Exception($error_msg);
                            }
                        } else {
                            $variationID = Variation::where('product_id', $purchase_val['product_id'])
                                ->first();
                            $purchase_data[$index]['variation_id'] = $variationID->id;
                        }
                    }
                }

                $final_total = $finalTotal - $transaction_data['discount_amount'] + $transaction_data['shipping_charges'] + $transaction_data['tax_amount'];

                $transaction_data['total_before_tax'] = $totalBeforeTax;
                $transaction_data['final_total'] = $final_total;
                //unformat input values
                $transaction_data['total_before_tax'] = $this->productUtil->num_uf($transaction_data['total_before_tax'], $currency_details) * $exchange_rate;

                // If discount type is fixed them multiply by exchange rate, else don't
                if ($transaction_data['discount_type'] == 'fixed') {
                    $transaction_data['discount_amount'] = $this->productUtil->num_uf($transaction_data['discount_amount'], $currency_details) * $exchange_rate;
                } elseif ($transaction_data['discount_type'] == 'percentage') {
                    $transaction_data['discount_amount'] = $this->productUtil->num_uf($transaction_data['discount_amount'], $currency_details);
                } else {
                    $transaction_data['discount_amount'] = 0;
                }

                $transaction_data['tax_amount'] = $this->productUtil->num_uf($transaction_data['tax_amount'], $currency_details) * $exchange_rate;
                $transaction_data['shipping_charges'] = $this->productUtil->num_uf($transaction_data['shipping_charges'], $currency_details) * $exchange_rate;
                $transaction_data['final_total'] = $this->productUtil->num_uf($transaction_data['final_total'], $currency_details) * $exchange_rate;

                $transaction_data['business_id'] = $business_id;
                $transaction_data['created_by'] = $user_id;
                $transaction_data['type'] = 'purchase';
                $transaction_data['payment_status'] = 'due';
                $transaction_data['transaction_date'] = $this->productUtil->uf_date($transaction_data['transaction_date'], true);

                //upload document
                $transaction_data['document'] = $this->transactionUtil->uploadFile($request, 'document', 'documents');

                //Update reference count
                $ref_count = $this->productUtil->setAndGetReferenceCount($transaction_data['type']);

                //Generate reference number
                if (empty($transaction_data['ref_no'])) {
                    $transaction_data['ref_no'] = $this->productUtil->generateReferenceNumber($transaction_data['type'], $ref_count);
                }

                //add restore val
                if($request->has('is_restore')){
                    $transaction_data['restore_parent_id']=$request->input('restore_for');
                }

                $transaction = Transaction::create($transaction_data);


                $purchase_lines = [];
                $purchases = $purchase_data;

//                echo '<pre>';
//                print_r($transaction);
//                print_r($purchases);
//                print_r($currency_details);
//                print_r($enable_product_editing);
                $this->productUtil->createOrUpdatePurchaseLines($transaction, $purchases, $currency_details, $enable_product_editing);
                //Add Purchase payments
                $this->transactionUtil->createOrUpdatePaymentLines($transaction, $request->input('payment'));

                //update payment status
                $this->transactionUtil->updatePaymentStatus($transaction->id, $transaction->final_total);

                //Adjust stock over selling if found
                $this->productUtil->adjustStockOverSelling($transaction);
            }

            $output = ['success' => 1,
                'msg' => __('product.file_imported_successfully')
            ];

            DB::commit();
            return redirect('purchases/import')->with('status', $output);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            $output = ['success' => 0,
                'msg' => $e->getMessage()
            ];
            return redirect('purchases/import')->with('notification', $output);
        }

    }

    private function calculateVariationPrices($dpp_exc_tax, $dpp_inc_tax, $selling_price, $tax_amount, $tax_type, $margin)
    {

        //Calculate purchase prices
        if ($dpp_inc_tax == 0) {
            $dpp_inc_tax = $this->productUtil->calc_percentage(
                $dpp_exc_tax,
                $tax_amount,
                $dpp_exc_tax
            );
        }

        if ($dpp_exc_tax == 0) {
            $dpp_exc_tax = $this->productUtil->calc_percentage_base($dpp_inc_tax, $tax_amount);
        }

        if ($selling_price != 0) {
            if ($tax_type == 'inclusive') {
                $dsp_inc_tax = $selling_price;
                $dsp_exc_tax = $this->productUtil->calc_percentage_base(
                    $dsp_inc_tax,
                    $tax_amount
                );
            } elseif ($tax_type == 'exclusive') {
                $dsp_exc_tax = $selling_price;
                $dsp_inc_tax = $this->productUtil->calc_percentage(
                    $selling_price,
                    $tax_amount,
                    $selling_price
                );
            }
        } else {
            $dsp_exc_tax = $this->productUtil->calc_percentage(
                $dpp_exc_tax,
                $margin,
                $dpp_exc_tax
            );
            $dsp_inc_tax = $this->productUtil->calc_percentage(
                $dsp_exc_tax,
                $tax_amount,
                $dsp_exc_tax
            );
        }

        return [
            'dpp_exc_tax' => $this->productUtil->num_f($dpp_exc_tax),
            'dpp_inc_tax' => $this->productUtil->num_f($dpp_inc_tax),
            'dsp_exc_tax' => $this->productUtil->num_f($dsp_exc_tax),
            'dsp_inc_tax' => $this->productUtil->num_f($dsp_inc_tax)
        ];
    }

    /**
     * Adds opening stock of a single product
     *
     * @param array $opening_stock
     * @param obj $product
     * @param int $business_id
     * @return void
     */
    private function addOpeningStock($opening_stock, $product, $business_id)
    {
        $user_id = request()->session()->get('user.id');

        $variation = Variation::where('product_id', $product->id)
            ->first();

        $total_before_tax = $opening_stock['quantity'] * $variation->dpp_inc_tax;

        $transaction_date = request()->session()->get("financial_year.start");
        $transaction_date = \Carbon::createFromFormat('Y-m-d', $transaction_date)->toDateTimeString();
        //Add opening stock transaction
        $transaction = Transaction::create(
            [
                'type' => 'opening_stock',
                'opening_stock_product_id' => $product->id,
                'status' => 'received',
                'business_id' => $business_id,
                'transaction_date' => $transaction_date,
                'total_before_tax' => $total_before_tax,
                'location_id' => $opening_stock['location_id'],
                'final_total' => $total_before_tax,
                'payment_status' => 'paid',
                'created_by' => $user_id
            ]
        );
        //Get product tax
        $tax_percent = !empty($product->product_tax->amount) ? $product->product_tax->amount : 0;
        $tax_id = !empty($product->product_tax->id) ? $product->product_tax->id : null;

        $item_tax = $this->productUtil->calc_percentage($variation->default_purchase_price, $tax_percent);

        //Create purchase line
        $transaction->purchase_lines()->create([
            'product_id' => $product->id,
            'variation_id' => $variation->id,
            'quantity' => $opening_stock['quantity'],
            'item_tax' => $item_tax,
            'tax_id' => $tax_id,
            'pp_without_discount' => $variation->default_purchase_price,
            'purchase_price' => $variation->default_purchase_price,
            'purchase_price_inc_tax' => $variation->dpp_inc_tax,
            'exp_date' => !empty($opening_stock['exp_date']) ? $opening_stock['exp_date'] : null
        ]);
        //Update variation location details
        $this->productUtil->updateProductQuantity($opening_stock['location_id'], $product->id, $variation->id, $opening_stock['quantity']);
    }


    private function addOpeningStockForVariable($variations, $product, $business_id)
    {
        $user_id = request()->session()->get('user.id');

        $transaction_date = request()->session()->get("financial_year.start");
        $transaction_date = \Carbon::createFromFormat('Y-m-d', $transaction_date)->toDateTimeString();

        $total_before_tax = 0;
        $location_id = $variations['opening_stock_location'];
        if (isset($variations['variations'][0]['opening_stock'])) {
            //Add opening stock transaction
            $transaction = Transaction::create(
                [
                    'type' => 'opening_stock',
                    'opening_stock_product_id' => $product->id,
                    'status' => 'received',
                    'business_id' => $business_id,
                    'transaction_date' => $transaction_date,
                    'total_before_tax' => $total_before_tax,
                    'location_id' => $location_id,
                    'final_total' => $total_before_tax,
                    'payment_status' => 'paid',
                    'created_by' => $user_id
                ]
            );

            foreach ($variations['variations'] as $variation_os) {
                if (!empty($variation_os['opening_stock'])) {
                    $variation = Variation::where('product_id', $product->id)
                        ->where('name', $variation_os['value'])
                        ->first();
                    if (!empty($variation)) {
                        $opening_stock = [
                            'quantity' => $variation_os['opening_stock'],
                            'exp_date' => $variation_os['opening_stock_exp_date'],
                        ];

                        $total_before_tax = $total_before_tax + ($variation_os['opening_stock'] * $variation->dpp_inc_tax);
                    }

                    //Get product tax
                    $tax_percent = !empty($product->product_tax->amount) ? $product->product_tax->amount : 0;
                    $tax_id = !empty($product->product_tax->id) ? $product->product_tax->id : null;

                    $item_tax = $this->productUtil->calc_percentage($variation->default_purchase_price, $tax_percent);

                    //Create purchase line
                    $transaction->purchase_lines()->create([
                        'product_id' => $product->id,
                        'variation_id' => $variation->id,
                        'quantity' => $opening_stock['quantity'],
                        'item_tax' => $item_tax,
                        'tax_id' => $tax_id,
                        'purchase_price' => $variation->default_purchase_price,
                        'purchase_price_inc_tax' => $variation->dpp_inc_tax,
                        'exp_date' => !empty($opening_stock['exp_date']) ? $opening_stock['exp_date'] : null
                    ]);
                    //Update variation location details
                    $this->productUtil->updateProductQuantity($location_id, $product->id, $variation->id, $opening_stock['quantity']);
                }
            }

            $transaction->total_before_tax = $total_before_tax;
            $transaction->final_total = $total_before_tax;
            $transaction->save();
        }
    }

    private function rackDetails($rack_value, $row_value, $position_value, $business_id, $product_id, $row_no)
    {
        if (!empty($rack_value) || !empty($row_value) || !empty($position_value)) {
            $locations = BusinessLocation::forDropdown($business_id);
            $loc_count = count($locations);

            $racks = explode('|', $rack_value);
            $rows = explode('|', $row_value);
            $position = explode('|', $position_value);

            if (count($racks) > $loc_count) {
                $error_msg = "Invalid value for RACK in row no. $row_no";
                throw new \Exception($error_msg);
            }

            if (count($rows) > $loc_count) {
                $error_msg = "Invalid value for ROW in row no. $row_no";
                throw new \Exception($error_msg);
            }

            if (count($position) > $loc_count) {
                $error_msg = "Invalid value for POSITION in row no. $row_no";
                throw new \Exception($error_msg);
            }

            $rack_details = [];
            $counter = 0;
            foreach ($locations as $key => $value) {
                $rack_details[$key]['rack'] = isset($racks[$counter]) ? $racks[$counter] : '';
                $rack_details[$key]['row'] = isset($rows[$counter]) ? $rows[$counter] : '';
                $rack_details[$key]['position'] = isset($position[$counter]) ? $position[$counter] : '';
                $counter += 1;
            }

            if (!empty($rack_details)) {
                $this->productUtil->addRackDetails($business_id, $product_id, $rack_details);
            }
        }
    }

}
