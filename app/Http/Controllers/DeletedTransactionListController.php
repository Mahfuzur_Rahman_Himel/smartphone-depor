<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessLocation;
use App\Deleted_payment;
use App\Deleted_transaction_item;
use App\Deleted_transaction_item_return;
use App\Deleted_transaction_list;
use Illuminate\Http\Request;
use DB;

class DeletedTransactionListController extends Controller
{
    public function index()
    {
        return view('purchase.index')
            ->with(compact('business_locations', 'suppliers', 'orderStatuses'));
    }

    /***********************************************************************
     ***************************PURCHASE INFO*******************************
     ***********************************************************************/
    public function getPurchaseList()
    {
        $page_data['purchaseList'] = Deleted_transaction_list::with('contact', 'hasDeleted')
            ->purchase()->get();
//        echo 'purchase list';
//        dd($page_data);
        return view('delete_info.purchase.list', $page_data);
    }

    public function showPurchaseList($id)
    {
        $page_data['purchase'] = Deleted_transaction_list::with('contact', 'hasDeleted')
            ->with('transaction_items.hasDeleted')
            ->with('payments')
            ->purchase()
            ->where('id', $id)
            ->first();
        $page_data['business'] = BusinessLocation::where('id',1)
            ->first();
//        dd($page_data);
        return view('delete_info.purchase.show', $page_data);
    }

    public function destroyPurchaseList($id)
    {
//        dd($id);
        if (!auth()->user()->can('purchase.delete')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();
            $tmp = Deleted_transaction_list::where('id', $id)
                ->where('transaction_for', 'purchase')
                ->firstOrFail();
            //delete trasaction list
            Deleted_transaction_list::where('id', $id)
                ->delete();
            //delete trasaction item
            Deleted_transaction_item::where('deleted_transaction_lists_id', $id)
                ->delete();
            //delete transaction payment
            Deleted_payment::where('deleted_transaction_lists_id', $id)
                ->delete();
            DB::commit();
            $output = ['success' => true,
                'msg' => __('lang_v1.purchase_delete_success')
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            $output = ['success' => false,
                'msg' => $e->getMessage()
            ];
        }

        return back()->with('status',$output);
    }

    public function getPurchaseReturnList()
    {
        $page_data['purchaseList'] = Deleted_transaction_list::with('contact', 'hasDeleted')
            ->purchase()->get();
//        echo 'purchase list';
//        dd($page_data);
        return view('delete_info.purchase.return_list', $page_data);
    }

    public function showPurchaseReturnList($id)
    {
        $page_data['purchase'] = Deleted_transaction_list::with('contact', 'hasDeleted')
            ->with('transaction_items.hasDeleted')
            ->with('payments')
            ->purchase()
            ->where('id', $id)
            ->first();
        $page_data['business'] = BusinessLocation::where('id',1)
            ->first();
//        dd($page_data);
        return view('delete_info.purchase.show', $page_data);
    }
}
