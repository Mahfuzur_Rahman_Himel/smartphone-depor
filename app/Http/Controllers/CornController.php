<?php

namespace App\Http\Controllers;

use \Notification;
use App\Notifications\CustomerNotification;
use App\Notifications\SupplierNotification;
use App\NotificationTemplate;
use App\Transaction;
use App\Utils\NotificationUtil;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Utils\Util;

use DB;

class CornController extends Controller
{
    protected $commonUtil;
    protected $notificationUtil;

    /**
     * Constructor
     *
     * @param Util $commonUtil
     * @return void
     */
    public function __construct(Util $commonUtil)
    {
        $this->commonUtil = $commonUtil;

        $notificationUtil = new NotificationUtil();
        $this->notificationUtil = $notificationUtil;
    }

    protected function daily_sms()
    {
        $business_id = 1;
        $today = \Carbon::now()->format("Y-m-d H:i:s");

        $query = Transaction::join(
            'contacts as c',
            'transactions.contact_id',
            '=',
            'c.id'
        )
            ->leftJoin(
                'transaction_payments as tp',
                'transactions.id',
                '=',
                'tp.transaction_id'
            )
            ->where('transactions.business_id', $business_id)
            ->where('transactions.type', 'sell')
            ->where('transactions.status', 'final')
            ->where('transactions.payment_status', '!=', 'paid')
            ->whereNotNull('transactions.pay_term_number')
            ->whereNotNull('transactions.pay_term_type')
            ->whereRaw("DATEDIFF( DATE_ADD( transaction_date, INTERVAL IF(transactions.pay_term_type = 'days', transactions.pay_term_number, 30 * transactions.pay_term_number) DAY), '$today') <= 7");


        $dues = $query->select(
            'transactions.id as id',
            'c.name as customer',
            'c.email as customer_email',
            'transactions.invoice_no',
            'final_total',
            DB::raw('SUM(tp.amount) as total_paid')
        )
            ->groupBy('transactions.id')->get();
//        dd($dues);
        foreach ($dues as $due) {

            try {
                $customer_notifications = NotificationTemplate::customerNotifications();
                $supplier_notifications = NotificationTemplate::supplierNotifications();

//                $data = $request->only(['to_email', 'subject', 'email_body', 'mobile_number', 'sms_body', 'notification_type']);

                $tmpNotificationInfo = NotificationTemplate::where('id', 3)->first();

                $data['to_email'] = $due->customer_email;
                $data['subject'] = $tmpNotificationInfo->subject;
                $data['email_body'] = $tmpNotificationInfo->email_body;

                $data['mobile_number'] = '';
                $data['sms_body'] = '';
                $data['notification_type'] = 'email_only';

                $transaction_id = $due->id;

                $orig_data = [
                    'email_body' => $data['email_body'],
                    'sms_body' => $data['sms_body'],
                    'subject' => $data['subject']
                ];

                $tag_replaced_data = $this->notificationUtil->replaceTags($business_id, $orig_data, $transaction_id);
                $data['email_body'] = $tag_replaced_data['email_body'];
                $data['sms_body'] = $tag_replaced_data['sms_body'];
                $data['subject'] = $tag_replaced_data['subject'];
                $data['tranID'] = $transaction_id;

                $data['email_settings'] = request()->session()->get('business.email_settings');

                $data['sms_settings'] = request()->session()->get('business.sms_settings');

                $notification_type = $data['notification_type'];

                if (array_key_exists('payment_reminder', $customer_notifications)) {
                    if ($notification_type == 'email_only') {

                        Notification::route('mail', $data['to_email'])
                            ->notify(new CustomerNotification($data));
                    } elseif ($notification_type == 'sms_only') {
                        $this->notificationUtil->sendSms($data);
                    } elseif ($notification_type == 'both') {
                        Notification::route('mail', $data['to_email'])
                            ->notify(new CustomerNotification($data));

                        $this->notificationUtil->sendSms($data);
                    }
                } elseif (array_key_exists('payment_reminder', $supplier_notifications)) {
                    if ($notification_type == 'email_only') {
                        Notification::route('mail', $data['to_email'])
                            ->notify(new SupplierNotification($data));
                    } elseif ($notification_type == 'sms_only') {
                        $this->notificationUtil->sendSms($data);
                    } elseif ($notification_type == 'both') {
                        Notification::route('mail', $data['to_email'])
                            ->notify(new SupplierNotification($data));

                        $this->notificationUtil->sendSms($data);
                    }
                }

                $output[] = ['success' => 1, 'msg' => __('lang_v1.notification_sent_successfully')];

            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

                $output[] = ['success' => 0,
                    'msg' => __('messages.something_went_wrong')
                ];
            }
        }
        return $output;
    }
}
