<?php

namespace App\Http\Controllers;

use App\Delete_info;
use Illuminate\Http\Request;
use DB;

class DeleteInfoController extends Controller
{
    public function getSell()
    {
        $page_data['sellList'] = Delete_info::with('hasDeleted')
            ->sell()->get();
        return view('delete_info.sell.list', $page_data);
    }
    public function getReturnSell()
    {
        $page_data['sellList'] = Delete_info::with('hasDeleted')
            ->SellReturn()->get();
//        dd($page_data);
        return view('delete_info.sell.sell_return_list', $page_data);
    }

    public function deleteSellList($id)
    {
        if (!auth()->user()->can('purchase.delete')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();
            $tmp = Delete_info::where('id', $id)
                ->where('delete_for', 'sell')
                ->firstOrFail();
            $tmp->delete();
            DB::commit();
            $output = ['success' => true,
                'msg' => 'Sell has been deleted successfully'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            $output = ['success' => false,
                'msg' => $e->getMessage()
            ];
        }
//        dd($output);
        return back()->with('status',$output);
    }

    public function deleteReturnSellList($id)
    {
        if (!auth()->user()->can('purchase.delete')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();
            $tmp = Delete_info::where('id', $id)
                ->where('delete_for', 'sell_return')
                ->firstOrFail();
            $tmp->delete();
            DB::commit();
            $output = ['success' => true,
                'msg' => 'Return Sell has been deleted successfully'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            $output = ['success' => false,
                'msg' => $e->getMessage()
            ];
        }
//        dd($output);
        return back()->with('status',$output);
    }
}
