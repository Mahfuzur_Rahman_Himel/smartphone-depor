<?php

namespace App\Http\Controllers;

use App\Deleted_transaction_item;
use Illuminate\Http\Request;

class DeletedTransactionItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deleted_transaction_item  $deleted_purchase_item
     * @return \Illuminate\Http\Response
     */
    public function show(Deleted_transaction_item $deleted_purchase_item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deleted_transaction_item  $deleted_purchase_item
     * @return \Illuminate\Http\Response
     */
    public function edit(Deleted_transaction_item $deleted_purchase_item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deleted_transaction_item  $deleted_purchase_item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deleted_transaction_item $deleted_purchase_item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deleted_transaction_item  $deleted_purchase_item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deleted_transaction_item $deleted_purchase_item)
    {
        //
    }
}
