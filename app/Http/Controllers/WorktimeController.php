<?php

namespace App\Http\Controllers;

use App\BusinessLocation;
use App\User;
use App\Utils\TransactionUtil;
use App\Worktime;
use Illuminate\Http\Request;
use DB;
use Yajra\DataTables\Facades\DataTables;

class WorktimeController extends Controller
{
    protected $transactionUtil;

    public function __construct(TransactionUtil $transactionUtil)
    {
        $this->transactionUtil = $transactionUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');

            $worktime = Worktime::leftJoin('users AS U', 'worktimes.user_id', '=', 'U.id')
                ->select(
                    'worktimes.id as w_id',
                    'worktimes.created_at as d_date',
                    'worktimes.in_time as in_t',
                    'worktimes.out_time as out_t',
                    DB::raw("CONCAT(COALESCE(U.surname, ''),' ',COALESCE(U.first_name, ''),' ',COALESCE(U.last_name,'')) as records_for")
                )->groupBy('worktimes.id');

            //Add condition for expense for,used in sales representative expense report & list of expense
            if (request()->has('user_id')) {
                $s_userID = request()->get('user_id');
                if (!empty($s_userID)) {
                    $worktime->where('worktimes.user_id', $s_userID);
                }
            }

            //Add condition for start and end date filter, uses in sales representative expense report & list of expense
            if (!empty(request()->start_date) && !empty(request()->end_date)) {
                $start = request()->start_date;
                $end = request()->end_date;
                $worktime->whereDate('worktimes.created_at', '>=', $start)
                    ->whereDate('worktimes.created_at', '<=', $end);
            }

            return Datatables::of($worktime)
                ->removeColumn('w_id')
                ->editColumn('d_date', '{{@format_datetime($d_date)}}')
                ->editColumn('records_for', function ($row) {
                    return $row->records_for;
                })
                ->editColumn('in_t', function ($row) {
                    $link='<a class="pull-right edit_in_time_modal"><i class="fa fa-edit"></i></a>';
                    return $row->in_t.' '.$link;
                })
                ->editColumn('out_t', function ($row) {
                    $link='<a class="pull-right edit_out_time_modal"><i class="fa fa-edit"></i></a>';
                    return $row->out_t.' '.$link;
                })
                ->rawColumns(['action','in_t','out_t'])
                ->make(true);
        }

        $business_id = request()->session()->get('user.business_id');

        $users = User::forDropdown($business_id, false, true, true);

        $business_locations = BusinessLocation::forDropdown($business_id, true);

        return view('worktime.index')
            ->with(compact('business_locations', 'users'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->session()->get('user.id');

        $worktime = Worktime::where('user_id', $user_id)
            ->where('created_at', 'like', '%' . date('Y-m-d') . '%')
            ->orderBy('id', 'desc')
            ->first();
        if ($worktime) {
            $worktime->out_time = date('H:i:s', time());
            $worktime->status = 'close';
            $worktime->save();

            $request->session()->put('worktime', 'closed');

            $output = ['success' => 1,
                'msg' =>'Time Records Closed Successfully'
            ];
        } else {
            $worktime = new Worktime();
            $worktime->user_id = $user_id;
            $worktime->in_time = date('H:i:s', time());
            $worktime->status = 'open';
            $worktime->save();

            $request->session()->put('worktime', 'opened');
            $output = ['success' => 1,
                'msg' =>'Time Records Opened Successfully'
            ];
        }

        return back()->with('status',$output);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Worktime $worktime
     * @return \Illuminate\Http\Response
     */
    public function show(Worktime $worktime)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Worktime $worktime
     * @return \Illuminate\Http\Response
     */
    public function edit(Worktime $worktime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Worktime $worktime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worktime $worktime)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Worktime $worktime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worktime $worktime)
    {
        //
    }
}
