<?php

namespace App\Http\Controllers;

use App\Deleted_payment;
use Illuminate\Http\Request;

class DeletedPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deleted_payment  $deleted_payment
     * @return \Illuminate\Http\Response
     */
    public function show(Deleted_payment $deleted_payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deleted_payment  $deleted_payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Deleted_payment $deleted_payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deleted_payment  $deleted_payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deleted_payment $deleted_payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deleted_payment  $deleted_payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deleted_payment $deleted_payment)
    {
        //
    }
}
