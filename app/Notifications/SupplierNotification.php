<?php

namespace App\Notifications;

use App\TaxRate;
use App\Transaction;
use App\Utils\NotificationUtil;
use App\Utils\ProductUtil;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use PDF;

use Illuminate\Notifications\Notification;

class SupplierNotification extends Notification
{
    use Queueable;

    protected $notificationInfo;
    protected $productUtil;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificationInfo)
    {
        $this->notificationInfo = $notificationInfo;

        $notificationUtil = new NotificationUtil();
        $notificationUtil->configureEmail($notificationInfo);

        $productUtil=new ProductUtil();
        $this->productUtil = $productUtil;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        dd($this->notificationInfo);
        $data = $this->notificationInfo;

        $id=$data['tranID'];
        $business_id = request()->session()->get('user.business_id');
        $taxes = TaxRate::where('business_id', $business_id)
            ->pluck('name', 'id');
        $purchase = Transaction::where('business_id', $business_id)
            ->where('id', $id)
            ->with(
                'contact',
                'purchase_lines',
                'purchase_lines.product',
                'purchase_lines.variations',
                'purchase_lines.variations.product_variation',
                'location',
                'payment_lines'
            )
            ->first();
        $payment_methods = $this->productUtil->payment_types();

        $pdf = PDF::loadView('mail.supplier.purchase_receipt', compact('taxes', 'purchase', 'payment_methods'));
        return (new MailMessage)
            ->subject($data['subject'])
            ->view(
                'emails.plain_html',
                ['content' => $data['email_body']]
            )->attachData($pdf->output(), 'invoice.pdf', [
                'mime' => 'application/pdf',
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
