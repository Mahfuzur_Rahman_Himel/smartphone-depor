<?php

namespace App\Notifications;

use App\BusinessLocation;
use App\Transaction;
use App\Utils\BusinessUtil;
use App\Utils\NotificationUtil;
use App\Utils\TransactionUtil;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use PDF;

class CustomerNotification extends Notification
{
    use Queueable;

    protected $notificationInfo;
    protected $businessUtil;
    protected $transactionUtil;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificationInfo)
    {
        $this->notificationInfo = $notificationInfo;
        $notificationUtil = new NotificationUtil();
        $notificationUtil->configureEmail($notificationInfo);

        $businessUtil = new BusinessUtil();
        $this->businessUtil = $businessUtil;
        $transactionUtil = new TransactionUtil();
        $this->transactionUtil = $transactionUtil;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->notificationInfo;

        $business_id = request()->session()->get('user.business_id');
        $transaction_id = $data['tranID'];
        $transaction = Transaction::where('business_id', $business_id)
            ->where('id', $transaction_id)
            ->with(['location'])
            ->first();

        $location_id = $transaction->location_id;
        $printer_type = null;
        $is_package_slip = false;
        $from_pos_screen = true;

        $business_details = $this->businessUtil->getDetails($business_id);
        $location_details = BusinessLocation::find($location_id);

        $invoice_layout = $this->businessUtil->invoiceLayout($business_id, $location_id, $location_details->invoice_layout_id);
        $receipt_printer_type = is_null($printer_type) ? $location_details->receipt_printer_type : $printer_type;
        $receipt_details = $this->transactionUtil->getReceiptDetails($transaction_id, $location_id, $invoice_layout, $business_details, $location_details, $receipt_printer_type);

        $currency_details = [
            'symbol' => $business_details->currency_symbol,
            'thousand_separator' => $business_details->thousand_separator,
            'decimal_separator' => $business_details->decimal_separator,
        ];
        $receipt_details->currency = $currency_details;

        $haveVariable = false;
        $p_array = [];
        foreach ($receipt_details->lines as $index => $line) {
            if ($line['type'] == 'variable') {
                $haveVariable = true;
            }
            if ($index == 0) {
                $p_array[] = $line;
            } else {
                $isPush = true;
                foreach ($p_array as $parry) {
                    if ($line['product_id'] == $parry['product_id']) {
                        $isPush = false;
                    }
                }
                if ($isPush) {
                    $p_array[] = $line;
                } else {
                    foreach ($p_array as $keyp => $parry) {
                        if ($line['product_id'] == $parry['product_id']) {
                            $p_array[$keyp]['quantity'] = $line['quantity'] + $parry['quantity'];
                            $p_array[$keyp]['line_total'] = $line['line_total'] + $parry['line_total'];
                        }
                    }
                }
            }
        }

        $pdf = PDF::loadView('mail.customer.invoice', compact('receipt_details', 'p_array', 'haveVariable'));
//        dd($pdf);
        return (new MailMessage)
            ->subject($data['subject'])
            ->view(
                'emails.plain_html',
                ['content' => $data['email_body']]
            )->attachData($pdf->output(), 'invoice.pdf', [
                'mime' => 'application/pdf',
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
