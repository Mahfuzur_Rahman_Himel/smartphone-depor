<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memory extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Return list of memories for a business
     *
     * @param int $business_id
     * @param boolean $show_none = false
     *
     * @return array
     */
    public static function forDropdown($business_id, $show_none = false)
    {
        $memories = Memory::where('business_id', $business_id)
            ->pluck('name', 'id');

        if ($show_none) {
            $memories->prepend(__('lang_v1.none'), '');
        }

        return $memories;
    }
}
