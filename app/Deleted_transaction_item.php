<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_transaction_item extends Model
{
    public function hasDeleted()
    {
        return $this->belongsTo(\App\User::class, 'deleted_by');
    }
}
