<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deleted_payment extends Model
{
    public function hasDeleted()
    {
        return $this->belongsTo(\App\User::class, 'deleted_by');
    }
}
