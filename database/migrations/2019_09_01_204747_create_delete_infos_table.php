<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeleteInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('delete_for')->comment('1=purchase 2=purchase_return 3=sell 4=sell_return');
            $table->longText('details');
            $table->integer('delete_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_infos');
    }
}
