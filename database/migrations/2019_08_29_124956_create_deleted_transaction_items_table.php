<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_transaction_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deleted_transaction_lists_id');
            $table->string('item_name');
            $table->string('item_imei');
            $table->string('item_brand');
            $table->string('item_memory');
            $table->string('item_color');
            $table->string('item_condition');
            $table->double('item_quantity');
            $table->double('item_price');
            $table->double('discount');
            $table->double('net_total');
            $table->string('deleted_at');
            $table->integer('deleted_by');
            $table->timestamps();
            $table->index('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_purchase_items');
    }
}
