<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedTransactionListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_transaction_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_for')->comment('1=purchase 2=purchase_return 3=sell 4=sell_return');
            $table->string('transaction_date');
            $table->string('invoice');
            $table->integer('contact_id');
            $table->string('status');
            $table->string('payment_status');
            $table->double('net_total');
            $table->double('discount')->default(0.0);
            $table->double('tax')->default(0.0);
            $table->double('shipping_charge')->default(0.0);
            $table->string('shipping_details')->default(null)->nullable();
            $table->string('document')->nullable();
            $table->double('grand_total')->default(0.0);
            $table->double('payment_due')->default(0.0);
            $table->string('deleted_by');
            $table->string('current_status')->comment('full delete or partial delete');
            $table->timestamps();
            $table->index('contact_id');
            $table->index('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_purchase_lists');
    }
}
