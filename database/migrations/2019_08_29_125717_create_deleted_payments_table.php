<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeletedPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deleted_transaction_lists_id');
            $table->string('payment_on')->comment('1=purchase 2=purchase return 3=sell 4=sell return');
            $table->string('paid_on');
            $table->string('ref_no')->nullable();
            $table->double('amount');
            $table->string('payment_method');
            $table->string('payment_note')->nullable();

            $table->string('transaction_no')->nullable();
            $table->string('card_transaction_number')->nullable();
            $table->string('card_number')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_holder_name')->nullable();
            $table->string('card_month')->nullable();
            $table->string('card_year')->nullable();
            $table->string('card_security')->nullable();
            $table->string('cheque_number')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('document')->nullable();
            $table->integer('payment_for')->nullable();

            $table->string('deleted_at');
            $table->integer('deleted_by');
            $table->timestamps();
            $table->index('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_payments');
    }
}
