<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('flag')->comment('individual search flag');
            $table->string('imei')->comment('imei');
            $table->string('is_search')->comment('is search imei given by user');
            $table->string('result')->comment('result');
            $table->integer('user_id')->comment('user whom initial search');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_match');
    }
}
