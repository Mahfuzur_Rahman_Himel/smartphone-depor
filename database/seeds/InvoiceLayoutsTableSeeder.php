<?php

use App\InvoiceLayout;
use Illuminate\Database\Seeder;

class InvoiceLayoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvoiceLayout::create([
            'id'=>1,
            'name'=>'Default',
            'header_text'=>null,
            'invoice_no_prefix'=>'Invoice No.',
            'quotation_no_prefix'=>null,
            'invoice_heading'=>'Invoice',
            'sub_heading_line1'=>null,
            'sub_heading_line2'=>null,
            'sub_heading_line3'=>null,
            'sub_heading_line4'=>null,
            'sub_heading_line5'=>null,
            'invoice_heading_not_paid'=>null,
            'invoice_heading_paid'=>null,
            'quotation_heading'=>null,
            'sub_total_label'=>'Subtotal',
            'discount_label'=>'Discount',
            'tax_label'=>'Tax',
            'total_label'=>'Total',
            'total_due_label'=>'Total Due',
            'paid_label'=>'Total Paid',
            'show_client_id'=>0,
            'client_id_label'=>null,
            'client_tax_label'=>null,
            'date_label'=>'Date',
            'date_time_format'=>null,
            'show_time'=>1,
            'show_brand'=>0,
            'show_sku'=>1,
            'show_cat_code'=>1,
            'show_expiry'=>0,
            'show_lot'=>0,
            'show_image'=>0,
            'show_sale_description'=>0,
            'sales_person_label'=>null,
            'show_sales_person'=>0,
            'table_product_label'=>'Product',
            'table_qty_label'=>'Quantity',
            'table_unit_price_label'=>'Unit Price',
            'table_subtotal_label'=>'Subtotal',
            'cat_code_label'=>null,
            'logo'=>null,
            'show_logo'=>1,
            'show_business_name'=>1,
            'show_location_name'=>1,
            'show_landmark'=>1,
            'show_city'=>1,
            'show_state'=>1,
            'show_zip_code'=>1,
            'show_country'=>1,
            'show_mobile_number'=>1,
            'show_alternate_number'=>0,
            'show_email'=>0,
            'show_tax_1'=>1,
            'show_tax_2'=>0,
            'show_barcode'=>1,
            'show_payments'=>1,
            'show_customer'=>1,
            'customer_label'=>'Customer',
            'highlight_color'=>'#000000',
            'footer_text'=>'<p><strong>If you are not satisfied with your purchased items, you may return the product(s) for a full refund or exchange the product(s) within 30 day of purchase. Refunds will be processed through the original forms of payment. Any product you return must be in the same condition you received it and in the original packaging. Please keep the receipt. Please contact sales@smartphone-depot.com for return &amp; exchange inquiries and to receive further instructions on how to ship products back to us.&nbsp;</strong></p>

<p><strong>Smartphone-Depot reserve the rights to accept, deny or exchange any RMA.</strong></p>

<p><strong>If paying via Paypal or Credit Card standard 3.5% fee are applied to the invoice.</strong></p>

<p><strong>Wire Instructions:</strong></p>

<p><strong>Wells Fargo<br />
420 Montgomery Street, San Francisco, CA 94101</strong></p>

<p><strong>Account Number: 9868767352<br />
Routing Number: 121000248<br />
SWIFT/BIC Code: WFBIUS6S (International Wire Only)</strong></p>',
            'module_info'=>null,
            'is_default'=>1,
            'business_id'=>1,
            'design'=>'classic',
            'cn_heading'=>null,
            'cn_no_label'=>null,
            'cn_amount_label'=>null,
            'table_tax_headings'=>null,
            'show_previous_bal'=>0,
            'prev_bal_label'=>null,
            'product_custom_fields'=>'["product_custom_field1","product_custom_field2","product_custom_field3","product_custom_field4"]',
            'contact_custom_fields'=>null,
            'location_custom_fields'=>null,
            'created_at'=>'2017-12-18 06:13:44',
            'updated_at'=>'2017-12-18 06:13:44'
        ]);
    }
}
