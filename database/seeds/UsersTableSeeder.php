<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'=>1,
            'surname'=>'Mr',
            'first_name'=>'Super',
            'last_name'=>'Smartphones',
            'username'=>'admin@gmail.com',
            'email'=>'admin@gmail.com',
            'password'=>'$2y$10$yYUR0dtA31td6TmQMd9ko.Jj.mu9aXVLwLBwZ4lQPt7A7hjdidpEe',
            'language'=>'en',
            'contact_no'=>null,
            'address'=>null,
            'remember_token'=>'4QHwL52UbgE9U0dQ1oKWFi4x4t7SOACDFX6DFnjqHdkShF5HdsdRKHbgq1xL',
            'business_id'=>null,
            'status'=>'active',
            'is_cmmsn_agnt'=>0,
            'cmmsn_percent'=>0.00,
            'selected_contacts'=>0,
            'dob'=>null,
            'marital_status'=>null,
            'blood_group'=>null,
            'contact_number'=>null,
            'fb_link'=>null,
            'twitter_link'=>null,
            'social_media_1'=>null,
            'social_media_2'=>null,
            'permanent_address'=>null,
            'current_address'=>null,
            'guardian_name'=>null,
            'custom_field_1'=>null,
            'custom_field_2'=>null,
            'custom_field_3'=>null,
            'custom_field_4'=>null,
            'bank_details'=>'{"account_holder_name":null,"account_number":null,"bank_name":null,"bank_code":null,"branch":null,"tax_payer_id":null}',
            'id_proof_name'=>null,
            'id_proof_number'=>null,
            'deleted_at'=>null,
            'created_at'=>'2017-12-18 06:13:44',
            'updated_at'=>'2017-12-18 06:13:44'
        ]);
    }
}
