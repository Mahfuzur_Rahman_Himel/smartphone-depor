<?php

use App\BusinessLocation;
use Illuminate\Database\Seeder;

class BusinessLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessLocation::create([
            'id'=>1,
            'business_id'=>1,
            'location_id'=>'BL0001',
            'name'=>'Smartphone-Depot',
            'landmark'=>'7015 Old Keene Mill Rd. #207  Springfield, VA 22150',
            'country'=>'Springfield',
            'state'=>'Springfield',
            'city'=>'Springfield',
            'zip_code'=>' VA 22150',
            'invoice_scheme_id'=>1,
            'invoice_layout_id'=>1,
            'print_receipt_on_invoice'=>1,
            'receipt_printer_type'=>'browser',
            'printer_id'=>null,
            'mobile'=>'571-529-4022',
            'alternate_number'=>null,
            'email'=>'sales@smartphone-depot.com',
            'website'=>'www.smartphone-depot.com',
            'custom_field1'=>null,
            'custom_field2'=>null,
            'custom_field3'=>null,
            'custom_field4'=>null,
            'deleted_at'=>null,
            'created_at'=>'2017-12-18 06:13:44',
            'updated_at'=>'2017-12-18 06:13:44'
        ]);
    }
}
