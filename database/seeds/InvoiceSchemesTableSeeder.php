<?php

use App\InvoiceScheme;
use Illuminate\Database\Seeder;

class InvoiceSchemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvoiceScheme::create([
            'id'=>1,
            'business_id'=>1,
            'name'=>'Default',
            'scheme_type'=>'blank',
            'prefix'=>'',
            'start_number'=>1,
            'invoice_count'=>22,
            'total_digits'=>4,
            'is_default'=>1,
            'created_at'=>'2017-12-18 06:13:44',
            'updated_at'=>'2017-12-18 06:13:44'
        ]);
    }
}
