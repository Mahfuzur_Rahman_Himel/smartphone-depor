<?php

use App\Business;
use Illuminate\Database\Seeder;

class BusinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Business::create([
            'id'=>1,
            'name'=>'Smartphone-Depot',
            'currency_id'=>2,
            'start_date'=>'2019-07-01',
            'tax_number_1'=>null,
            'tax_label_1'=>null,
            'tax_number_2'=>null,
            'tax_label_2'=>null,
            'default_sales_tax'=>null,
            'default_profit_percent'=>25.00,
            'owner_id'=>1,
            'time_zone'=>'America/New_York',
            'fy_start_month'=>1,
            'accounting_method'=>'fifo',
            'default_sales_discount'=>null,
            'sell_price_tax'=>'includes',
            'logo'=>null,
            'sku_prefix'=>null,
            'enable_product_expiry'=>0,
            'expiry_type'=>'add_expiry',
            'on_product_expiry'=>'keep_selling',
            'stop_selling_before'=>0,
            'enable_tooltip'=>1,
            'purchase_in_diff_currency'=>0,
            'purchase_currency_id'=>null,
            'p_exchange_rate'=>1.000,
            'transaction_edit_days'=>30,
            'stock_expiry_alert_days'=>30,
            'keyboard_shortcuts'=>'{"pos":{"express_checkout":"shift+e","pay_n_ckeckout":"shift+p","draft":"shift+d","cancel":"shift+c","recent_product_quantity":"f2","edit_discount":"shift+i","edit_order_tax":"shift+t","add_payment_row":"shift+r","finalize_payment":"shift+f","add_new_product":"f4"}}',
            'pos_settings'=>'{"disable_pay_checkout":0,"disable_draft":0,"disable_express_checkout":0,"hide_product_suggestion":0,"hide_recent_trans":0,"disable_discount":0,"disable_order_tax":0,"is_pos_subtotal_editable":0}',
            'enable_brand'=>1,
            'enable_category'=>1,
            'enable_sub_category'=>1,
            'enable_price_tax'=>1,
            'enable_purchase_status'=>1,
            'enable_lot_number'=>0,
            'default_unit'=>null,
            'enable_racks'=>0,
            'enable_row'=>0,
            'enable_position'=>0,
            'enable_editing_product_from_purchase'=>1,
            'sales_cmsn_agnt'=>null,
            'item_addition_method'=>1,
            'enable_inline_tax'=>1,
            'currency_symbol_placement'=>'before',
            'enabled_modules'=>null,
            'date_format'=>'m/d/Y',
            'time_format'=>12,
            'ref_no_prefixes'=>'{"purchase":"PO","purchase_return":null,"stock_transfer":"ST","stock_adjustment":"SA","sell_return":"CN","expense":"EP","contacts":"CO","purchase_payment":"PP","sell_payment":"SP","expense_payment":null,"business_location":"BL","username":null,"subscription":null}',
            'theme_color'=>null,
            'created_by'=>null,
            'email_settings'=>'{"mail_driver":"smtp","mail_host":"smtp.gmail.com","mail_port":"587","mail_username":"info.smartphone.depot@gmail.com","mail_password":"qwerty123**","mail_encryption":"tls","mail_from_address":"info@geeksntechnology.com","mail_from_name":"Geeksntechnology Ltd"}',
            'sms_settings'=>'{"url":null,"send_to_param_name":"to","msg_param_name":"text","request_method":"post","param_1":null,"param_val_1":null,"param_2":null,"param_val_2":null,"param_3":null,"param_val_3":null,"param_4":null,"param_val_4":null,"param_5":null,"param_val_5":null,"param_6":null,"param_val_6":null,"param_7":null,"param_val_7":null,"param_8":null,"param_val_8":null,"param_9":null,"param_val_9":null,"param_10":null,"param_val_10":null}',
            'is_active'=>1,
            'created_at'=>'2017-12-18 06:13:44',
            'updated_at'=>'2017-12-18 06:13:44'
        ]);
    }
}
