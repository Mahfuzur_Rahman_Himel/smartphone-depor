<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BarcodesTableSeeder::class,
            PermissionsTableSeeder::class,
            CurrenciesTableSeeder::class,
            UsersTableSeeder::class,
            BusinessTableSeeder::class,
            InvoiceSchemesTableSeeder::class,
            InvoiceLayoutsTableSeeder::class,
            BusinessLocationsTableSeeder::class,
            RolesTableSeeder::class,
            ModelHasRolesTableSeeder::class,
            ModelHasRolesTableSeeder::class

        ]);
    }
}
