<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Memory Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Memory CRUD operations.
    |
    */

    'memories' => 'Memories',
    'manage_your_memories' => 'Manage your Memories',
    'all_your_memories' => 'All your Memories',
    'note' => 'Note',
    'memory_name' => 'Memory name',
    'short_description' => 'Short description',
    'added_success' => 'Memory added successfully',
    'updated_success' => 'Memory updated successfully',
    'deleted_success' => 'Memory deleted successfully',
    'add_memory' => 'Add Memory',
    'edit_memory' => 'Edit Memory',

];
