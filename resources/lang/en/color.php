<?php

return [

    /*
    |--------------------------------------------------------------------------
    | color Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for color CRUD operations.
    |
    */

    'colors' => 'Colors',
    'manage_your_colors' => 'Manage your Colors',
    'all_your_colors' => 'All your Colors',
    'note' => 'Note',
    'color_name' => 'Color name',
    'short_description' => 'Short description',
    'added_success' => 'Color added successfully',
    'updated_success' => 'Color updated successfully',
    'deleted_success' => 'Color deleted successfully',
    'add_color' => 'Add Color',
    'edit_color' => 'Edit Color',

];
