<?php

return [

    /*
    |--------------------------------------------------------------------------
    | condition Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for condition CRUD operations.
    |
    */

    'conditions' => 'Conditions',
    'manage_your_conditions' => 'Manage your Conditions',
    'all_your_conditions' => 'All your Conditions',
    'note' => 'Note',
    'condition_name' => 'Condition name',
    'short_description' => 'Short description',
    'added_success' => 'Condition added successfully',
    'updated_success' => 'Condition updated successfully',
    'deleted_success' => 'Condition deleted successfully',
    'add_condition' => 'Add Condition',
    'edit_condition' => 'Edit Condition',

];
