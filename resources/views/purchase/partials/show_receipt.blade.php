<div class="modal-header no-print">
    <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title " id="modalTitle"> @lang('purchase.purchase_receipt') (<b>@lang('purchase.ref_no'):</b>
        #{{ $purchase->ref_no }})
    </h4>
</div>
<div class="modal-body">
    @include('layouts.partials.report-header')
    <div class="row invoice-info">
        <div class="col-sm-7 invoice-col-n">
            @lang('purchase.supplier'):
            <address>
                <strong>{{ $purchase->contact->supplier_business_name }}</strong>
                {{ $purchase->contact->name }}
                @if(!empty($purchase->contact->landmark))
                    <br>{{$purchase->contact->landmark}}
                @endif
                @if(!empty($purchase->contact->city) || !empty($purchase->contact->state) || !empty($purchase->contact->country))
                    <br>{{implode(',', array_filter([$purchase->contact->city, $purchase->contact->state, $purchase->contact->country]))}}
                @endif
                @if(!empty($purchase->contact->tax_number))
                    <br>@lang('contact.tax_no'): {{$purchase->contact->tax_number}}
                @endif
                @if(!empty($purchase->contact->mobile))
                    <br>@lang('contact.mobile'): {{$purchase->contact->mobile}}
                @endif
                @if(!empty($purchase->contact->email))
                    <br>Email: {{$purchase->contact->email}}
                @endif
            </address>
            @if($purchase->document_path)

                <a href="{{$purchase->document_path}}"
                   download="{{$purchase->document_name}}" class="btn btn-sm btn-success pull-left no-print">
                    <i class="fa fa-download"></i>
                    &nbsp;{{ __('purchase.download_document') }}
                </a>
            @endif
        </div>
        <div class="col-sm-5 invoice-col-n">
            <div class="col-sm-12" style="border: 1px solid black;">
                <br>
                <b>@lang('purchase.invoice'):</b> #{{ $purchase->ref_no }}<br/>
                <br>
                <b>Invoice @lang('messages.date'):</b> {{ @format_date($purchase->transaction_date) }}<br/>
                <b>Invoice @lang('purchase.amount'):</b>
                <span class="display_currency" data-currency_symbol="true">{{ $purchase->final_total }}</span><br/>
                @php
                    $p_date='';
                    $p_type='';
                @endphp
                @if(sizeof($purchase->payment_lines)!=0)
                    @foreach($purchase->payment_lines as $payment_line)
                        @if(!$loop->first)
                            @php $p_date.=', ';  $p_type.=', ' @endphp
                        @endif
                        @php
                            $p_date.= $payment_line->paid_on;
                            $p_type.= $payment_methods[$payment_line->method];
                        @endphp
                    @endforeach
                @endif
                <b>Payment @lang('messages.date'):</b> {{$p_date}}<br/>
                <b>Payment Type:</b> {{$p_type}}
                <br> &nbsp;
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table class="table bg-gray">
                    <thead>
                    <tr class="bg-blue">
                        <th>#</th>
                        <th>Item</th>
                        <th>Memory</th>
                        <th>Color</th>
                        <th>Condition</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    @php
                        $total_before_tax = 0.00;

                        $p_purchases=$purchase->purchase_lines->groupBy('product_id');
                        $key=1;
                    @endphp
                    @foreach($p_purchases as $purchase_line)
                        @php
                            $line_quantity=0;
                            $line_total=0;
                        @endphp
                        @foreach($purchase_line as $p)
                            @php
                                $total_before_tax += ($p->quantity * $p->purchase_price);
                                $line_quantity+=$p->quantity;
                                $line_total+=($p->purchase_price_inc_tax * $p->quantity);
                            @endphp
                        @endforeach
                        <tr>
                            <td>{{ $key++ }}</td>
                            <td>
                                {{ $purchase_line->first()->product->product_custom_field1 }}
                            </td>
                            <td>
                                {{ $purchase_line->first()->product->product_custom_field2 }}
                            </td>
                            <td>
                                {{ $purchase_line->first()->product->product_custom_field3 }}
                            </td>
                            <td>
                                {{ $purchase_line->first()->product->product_custom_field4 }}
                            </td>
                            <td>
                                <span class="display_currency" data-is_quantity="true"
                                      data-currency_symbol="false">
                                    {{ $line_quantity }}</span>
                                @if(!empty($purchase_line->first()->sub_unit))
                                    {{$purchase_line->first()->sub_unit->short_name}}
                                @else
                                    {{$purchase_line->first()->product->unit->short_name}}
                                @endif
                            </td>
                            <td class="text-right">
                                <span class="display_currency" data-currency_symbol="true">
                                    {{$line_total}}
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <!--list if summary-->
    <br>
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table class="table">
                <!-- <tr class="hide">
            <th>@lang('purchase.total_before_tax'): </th>
            <td></td>
            <td><span class="display_currency pull-right">{{ $total_before_tax }}</span></td>
          </tr> -->
                    <tr>
                        <th>@lang('purchase.net_total_amount'):</th>
                        <td></td>
                        <td><span class="display_currency pull-right"
                                  data-currency_symbol="true">{{ $total_before_tax }}</span></td>
                    </tr>
                    <tr>
                        <th>@lang('purchase.discount'):</th>
                        <td>
                            <b>(-)</b>
                            @if($purchase->discount_type == 'percentage')
                                ({{$purchase->discount_amount}} %)
                            @endif
                        </td>
                        <td>
              <span class="display_currency pull-right" data-currency_symbol="true">
                @if($purchase->discount_type == 'percentage')
                      {{$purchase->discount_amount * $total_before_tax / 100}}
                  @else
                      {{$purchase->discount_amount}}
                  @endif
              </span>
                        </td>
                    </tr>
                    <tr>
                        <th>@lang('purchase.purchase_tax'):</th>
                        <td><b>(+)</b></td>
                        <td class="text-right">
                            @if(!empty($purchase_taxes))
                                @foreach($purchase_taxes as $k => $v)
                                    <strong>
                                        <small>{{$k}}</small>
                                    </strong> - <span class="display_currency pull-right"
                                                      data-currency_symbol="true">{{ $v }}</span><br>
                                @endforeach
                            @else
                                0.00
                            @endif
                        </td>
                    </tr>
                    @if( !empty( $purchase->shipping_charges ) )
                        <tr>
                            <th>@lang('purchase.additional_shipping_charges'):</th>
                            <td><b>(+)</b></td>
                            <td><span class="display_currency pull-right">{{ $purchase->shipping_charges }}</span></td>
                        </tr>
                    @endif
                    <tr>
                        <th>@lang('purchase.purchase_total'):</th>
                        <td></td>
                        <td><span class="display_currency pull-right"
                                  data-currency_symbol="true">{{ $purchase->final_total }}</span></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!--list if imei-->
    <br>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="table-responsive">
                <p class="text-bold">List of Purchased Phones IMEI:</p>
                <table class="table bg-gray">
                    <thead>
                    <tr class="bg-blue">
                        <th>#</th>
                        <th>Model</th>
                        <th>IMEI</th>
                        <th>Memory</th>
                        <th>Color</th>
                        <th>Condition</th>
                    </tr>
                    </thead>
                    @foreach($purchase->purchase_lines as $purchase_line)
                        @if( $purchase_line->product->type == 'variable')
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $purchase_line->product->product_custom_field1 }}
                                </td>
                                <td>
                                    {{ $purchase_line->variations->name}}
                                </td>
                                <td>
                                    {{ $purchase_line->product->product_custom_field2 }}
                                </td>
                                <td>
                                    {{ $purchase_line->product->product_custom_field3 }}
                                </td>
                                <td>
                                    {{ $purchase_line->product->product_custom_field4 }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <!--list if payment info-->
    @if(sizeof($purchase->payment_lines)!=0)
        <br>
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <h4>{{ __('sale.payment_info') }}:</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table">
                        <tr class="bg-blue">
                            <th>#</th>
                            <th>{{ __('messages.date') }}</th>
                            <th>{{ __('purchase.ref_no') }}</th>
                            <th>{{ __('sale.amount') }}</th>
                            <th>{{ __('sale.payment_mode') }}</th>
                            <th>{{ __('sale.payment_note') }}</th>
                        </tr>
                        @php
                            $total_paid = 0;
                        @endphp
                        @forelse($purchase->payment_lines as $payment_line)
                            @php
                                $total_paid += $payment_line->amount;
                            @endphp
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ @format_date($payment_line->paid_on) }}</td>
                                <td>{{ $payment_line->payment_ref_no }}</td>
                                <td><span class="display_currency"
                                          data-currency_symbol="true">{{ $payment_line->amount }}</span></td>
                                <td>{{ $payment_methods[$payment_line->method] }}</td>
                                <td>@if($payment_line->note)
                                        {{ ucfirst($payment_line->note) }}
                                    @else
                                        --
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    @lang('purchase.no_payments')
                                </td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    @endif
<!--list if shipping cost//additional info-->
    <br>
    <div class="row">
        @if($purchase->shipping_details)
            <div class="col-sm-6">
                <strong>@lang('purchase.shipping_details'):</strong><br>
                <p class="well well-sm no-shadow bg-gray">
                    @if($purchase->shipping_details)
                        {{ $purchase->shipping_details }}
                    @else
                        --
                    @endif
                </p>
            </div>
        @endif
        @if($purchase->additional_notes)
            <div class="col-sm-6">
                <strong>@lang('purchase.additional_notes'):</strong><br>
                <p class="well well-sm no-shadow bg-gray">
                    @if($purchase->additional_notes)
                        {{ $purchase->additional_notes }}
                    @else
                        --
                    @endif
                </p>
            </div>
        @endif
    </div>
    {{-- Barcode --}}
    <div class="row print_section">
        <div class="col-xs-12">
            <img class="center-block"
                 src="data:image/png;base64,{{DNS1D::getBarcodePNG($purchase->ref_no, 'C128', 2,30,array(39, 48, 54), true)}}">
        </div>
    </div>
</div>