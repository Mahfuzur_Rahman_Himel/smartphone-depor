<div class="modal-dialog modal-xl no-print" role="document">
    <div class="modal-content">
        <div class="modal-header no-print">
            <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalTitle"> @lang('lang_v1.sell_return') (<b>@lang('sale.invoice_no')
                    :</b> {{ $sell->return_parent->invoice_no }})
            </h4>
        </div>
        <div class="modal-body">
            @include('layouts.partials.sell-report-header')
            <div class="row invoice-info">
                <div class="col-sm-9 invoice-col-n">
                    <b>Bill To,</b>
                    <br>
                    {{ $sell->contact->name }}<br>
                    @if(!empty($sell->billing_address()))
                        {{$sell->billing_address()}}
                    @else
                        @if($sell->contact->landmark)
                            {{ $sell->contact->landmark }},
                        @endif

                        {{ $sell->contact->city }}

                        @if($sell->contact->state)
                            {{ ', ' . $sell->contact->state }}
                        @endif
                        <br>
                        @if($sell->contact->country)
                            {{ $sell->contact->country }}
                        @endif
                    @endif
                </div>
                <div class="col-sm-3 invoice-col-n">
                    <b>Return Date :</b> {{@format_date($sell->return_parent->transaction_date)}}<br>
                    <b>{{ __('sale.invoice_no') }}:</b> #{{ $sell->invoice_no }}<br>
                    <b>Invoice Date :</b> {{@format_date($sell->transaction_date)}}<br>
                    <b>Payment Due :</b> <span id="payment_due_span" class="display_currency"
                                               data-currency_symbol="true">0</span><br>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <table class="table bg-gray">
                        <thead>
                        <tr class="bg-blue">
                            <th>#</th>
                            <th>Items</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Amount</th>
                            {{--                            <th>@lang('product.product_name')</th>--}}
                            {{--                            <th>@lang('sale.unit_price')</th>--}}
                            {{--                            <th>@lang('lang_v1.return_quantity')</th>--}}
                            {{--                            <th>@lang('lang_v1.return_subtotal')</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @php
                            $total_before_tax = 0;
                            $p_sell=$sell->sell_lines->groupBy('product_id');
                            $key=1;
                        @endphp

                        @foreach($p_sell as $sell_line)
                            @php
                                $is_row_flag=false;
                                $line_quantity=0;
                                $line_total=0;
                                $line_discount=0;
                                $line_counter=0;
                            @endphp
                            @foreach($sell_line as $p)
                                @if($p->quantity_returned != 0)
                                    @php
                                        $line_counter++;
                                        $is_row_flag=true;
                                        $line_quantity+=$p->quantity_returned;
                                        $line_total+=($p->unit_price_inc_tax * $p->quantity_returned)-$p->line_return_discount_amount;
                                        $line_discount+=$p->line_return_discount_amount;
                                    //echo $line_total.'==';
                                    @endphp
                                @endif
                            @endforeach
{{--                            {{dd('hh')}}--}}
                            @if($is_row_flag)
                                @php
                                    $total_before_tax += $line_total ;
                                    $unit_name = $sell_line->first()->product->unit->short_name;
                                    if(!empty($sell_line->first()->sub_unit)) {
                                      $unit_name = $sell_line->first()->sub_unit->short_name;
                                    }
                                    $line_discount= $line_discount/$line_counter;
                                @endphp
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>
                                        {{ $sell_line->first()->product->name }}
                                    </td>
                                    <td>{{$line_quantity}} {{$unit_name}}</td>
                                    <td><span class="display_currency"
                                              data-currency_symbol="true">{{ $sell_line->first()->unit_price_inc_tax }}</span>
                                    </td>
                                    <td><span class="display_currency"
                                              data-currency_symbol="true">{{ $line_discount }}</span>
                                    </td>
                                    <td class="text-right">
                                        <span class="display_currency"
                                              data-currency_symbol="true">{{$line_total}}</span>
                                    </td>
                                </tr>
                            @endif

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6">
                    <table class="table">
                        {{--                        <tr>--}}
                        {{--                            <th>@lang('purchase.net_total_amount'):</th>--}}
                        {{--                            <td></td>--}}
                        {{--                            <td><span class="display_currency pull-right"--}}
                        {{--                                      data-currency_symbol="true">{{ $total_before_tax }}</span></td>--}}
                        {{--                        </tr>--}}

                        {{--                        <tr>--}}
                        {{--                            <th>@lang('lang_v1.return_discount'):</th>--}}
                        {{--                            <td><b>(-)</b></td>--}}
                        {{--                            <td class="text-right">@if($sell->return_parent->discount_type == 'percentage')--}}
                        {{--                                    @<strong>--}}
                        {{--                                        <small>{{$sell->return_parent->discount_amount}}%</small>--}}
                        {{--                                    </strong> ---}}
                        {{--                                @endif--}}
                        {{--                                <span class="display_currency pull-right"--}}
                        {{--                                      data-currency_symbol="true">{{ $total_discount }}</span></td>--}}
                        {{--                        </tr>--}}

                        {{--                        <tr>--}}
                        {{--                            <th>@lang('lang_v1.total_return_tax'):</th>--}}
                        {{--                            <td><b>(+)</b></td>--}}
                        {{--                            <td class="text-right">--}}
                        {{--                                @if(!empty($sell_taxes))--}}
                        {{--                                    @foreach($sell_taxes as $k => $v)--}}
                        {{--                                        <strong>--}}
                        {{--                                            <small>{{$k}}</small>--}}
                        {{--                                        </strong> - <span class="display_currency pull-right"--}}
                        {{--                                                          data-currency_symbol="true">{{ $v }}</span><br>--}}
                        {{--                                    @endforeach--}}
                        {{--                                @else--}}
                        {{--                                    0.00--}}
                        {{--                                @endif--}}
                        {{--                            </td>--}}
                        {{--                        </tr>--}}
                        <tr>
                            <th>@lang('lang_v1.return_total'):</th>
                            <td></td>
                            <td><span class="display_currency pull-right"
                                      data-currency_symbol="true">{{ $sell->return_parent->final_total }}</span></td>
                        </tr>
                        @php($ttlPAY=0)
                        @if($sell->return_parent && $sell->return_parent->payment_lines)
                            @foreach($sell->return_parent->payment_lines as $payment)
                                @php($ttlPAY+=$payment->amount)
                                <tr>
                                    <th>Payment on {{@format_date($payment->paid_on)}} using {{$payment->method}}:</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $payment->amount }}</span>
                                    </td>
                                </tr>
                            @endforeach
                            <script>
                                var ttlDue = '{{$sell->return_parent->final_total-$ttlPAY}}';
                                console.log(ttlDue);
                                document.getElementById("payment_due_span").innerHTML = ttlDue;
                            </script>
                            <tr>
                                <th>Total Remaining:</th>
                                <td></td>
                                <td><span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $sell->return_parent->final_total-$ttlPAY }}</span>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h4>List of Returned phones IMEI number(s):</h4>
                </div>
                <div class="col-sm-12">
                    <table class="table bg-gray">
                        <thead>
                        <tr class="bg-blue">
                            <th>#</th>
                            <th>NAME</th>
                            <th>IMEI</th>
                            <th>DISCOUNT</th>
                            <th>COLOR</th>
                            <th>MEMORY</th>
                            <th>CONDITION</th>

                        </tr>
                        </thead>
                        <tbody>

                        @php($sell_index=1)
                        @foreach($sell->sell_lines as $sell_line)
                            @if($sell_line->product->type=='variable')
                                @if($sell_line->quantity_returned == 0)
                                    @continue
                                @endif

                                <tr>
                                    <td>{{ $sell_index++ }}</td>
                                    <td>
                                        {{ $sell_line->product->product_custom_field1 }}
                                    </td>
                                    <td>
                                        {{ $sell_line->variations->name }}
                                    </td>
                                    <td>
                                        <span class="display_currency "
                                              data-currency_symbol="true">{{ $sell_line->line_return_discount_amount }}</span>
                                    </td>
                                    <td>
                                        {{ $sell_line->product->product_custom_field3 }}
                                    </td>
                                    <td>
                                        {{ $sell_line->product->product_custom_field2 }}
                                    </td>
                                    <td>
                                        {{ $sell_line->product->product_custom_field4 }}
                                    </td>

                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="no-print modal-footer">
                <a href="#" class="print-invoice btn btn-primary"
                   data-href="{{action('SellReturnController@printInvoice', [$sell->return_parent->id])}}"><i
                            class="fa fa-print" aria-hidden="true"></i> @lang("messages.print")</a>


                <button type="button" class="btn btn-default no-print"
                        data-dismiss="modal">@lang( 'messages.close' )</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var element = $('div.modal-xl');
            __currency_convert_recursively(element);
        });
    </script>