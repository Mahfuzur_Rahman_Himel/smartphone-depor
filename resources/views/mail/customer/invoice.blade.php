<div style="font-size: 12px!important;">
    <table style="width: 100%;">
        <tr>
            <td style="width: 20%">
                <img src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}"
                     alt="Logo">
            </td>
            <td style="width: 8%"></td>
            <td style="width: 45%;">
                <strong>
                    @if(!empty($receipt_details->business_name))
                        {{$receipt_details->business_name}}
                    @endif
                </strong>
                <br>
                @if(!empty($receipt_details->address))
                    {!! $receipt_details->address !!}
                @endif
            </td>
            <td style="text-align: right">
                <strong>Contact Information :</strong>
                <br> {!! $receipt_details->contact !!}
                @if(!empty($receipt_details->website))
                    {{ $receipt_details->website }}
                @endif
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td style="width: 60%">
                <b>Bill to:</b>
                <!-- customer info -->
                @if(!empty($receipt_details->customer_name))
                    <br/>
                    <b>{{ $receipt_details->customer_name }}</b> <br>
                @endif
                @if(!empty($receipt_details->customer_info))
                    {!! $receipt_details->customer_info !!}
                @endif
                @if(!empty($receipt_details->client_id_label))
                    <br/>
                    <b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
                @endif
                @if(!empty($receipt_details->customer_tax_label))
                    <br/>
                    <b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
                @endif
                @if(!empty($receipt_details->customer_custom_fields))
                    <br/>{!! $receipt_details->customer_custom_fields !!}
                @endif
                @if(!empty($receipt_details->sales_person_label))
                    <br/>
                    <b>{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
                @endif
            </td>
            <td style="">
                @if(!empty($receipt_details->invoice_no_prefix))
                    <b>{!! $receipt_details->invoice_no_prefix !!}</b>
                @endif
                {{$receipt_details->invoice_no}}
                <br>
                <b>Invoice {{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}
                <br>
                <b>Payment Due :</b>
                <span>{{$receipt_details->total_due}}</span>
            </td>
        </tr>
    </table>

    <br>
    <table style="width: 100%">
        <tr style="background-color: #0073b7 !important">
            <th>Items</th>
            <th>Quantity</th>
            <th>Price</th>
            <th style="text-align: right">Amount</th>
        </tr>
        @forelse($p_array as $line)
            <tr>
                <td style="word-break: break-all;">
                    {{$line['name']}}
                </td>
                <td>{{$line['quantity']}} {{$line['units']}} </td>
                <td>{{$line['unit_price_inc_tax']}}</td>
                <td style="text-align: right"><span id="payment_due_span">${{$line['line_total']}}</span></td>
            </tr>
        @empty
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
        @endforelse
    </table>
    <br>
    <table style="width: 100%">
        <tr>
            <th style="width: 50%"></th>
            <th>Sub Total:</th>
            <td></td>
            <td style="text-align: right"><span>{{ $receipt_details->subtotal }}</span></td>
        </tr>
        <tr>
            <th style="width: 50%"></th>
            <th>{{ __('sale.discount') }}:</th>
            <td><b>(-)</b></td>
            <td style="text-align: right">
                <span>${{ $receipt_details->discount }}</span>
            </td>
        </tr>
        <tr>
            <th style="width: 50%"></th>
            <th>{{ __('sale.order_tax') }}:</th>
            <td><b>(+)</b></td>
            <td style="text-align: right;">
                <span>$ {{$receipt_details->tax}}</span>
            </td>
        </tr>
        <tr>
            <th style="width: 50%"></th>
            <th>{{ __('sale.shipping') }}: @if($receipt_details->shipping_details)
                    ({{$receipt_details->shipping_details}}
                    ) @endif <br> Track Number: @if($receipt_details->shipping_track_number)
                    ({{$receipt_details->shipping_track_number}}) @endif</th>
            <td><b>(+)</b></td>
            <td style="text-align: right"><span>{{ $receipt_details->shipping_charges }}</span></td>
        </tr>
        <tr>
            <th style="width: 50%"></th>
            <th>{{ __('sale.total_payable') }}:</th>
            <td></td>
            <td style="text-align: right"><span>{{ $receipt_details->total }}</span></td>
        </tr>
        @foreach($receipt_details->payments as $payment)
            <tr>
                <th style="width: 50%"></th>
                <th>Payment on {{$payment['date']}} using {{$payment['method']}}</th>
                <td></td>
                <td style="text-align: right"><span> {{ $payment['amount'] }}</span></td>
            </tr>
        @endforeach
        <tr>
            <th style="width: 50%"></th>
            <th>{{ __('sale.total_remaining') }}:</th>
            <td></td>
            <td style="text-align: right"><span>{{ $receipt_details->total_due }}</span></td>
        </tr>
    </table>

    <br>
    <p class="text-bold">List of Sold Phones IMEI number(s) :</p>
    <table style="width: 100%; text-align: center">
        <tr style="background-color: #0073b7 !important">
            <th>#</th>
            <th>NAME</th>
            <th>IMEI</th>
            <th>COLOR</th>
            <th>MEMORY</th>
            <th>CONDITION</th>
        </tr>
        @php($imei_index=1)
        @forelse($receipt_details->lines as $line)
            @if($line['type']=='variable')
                <tr>
                    <td>{{$imei_index++}}</td>
                    <td style="word-break: break-all;">
                        {{$line['product_custom_fields_1']}}
                    </td>
                    <td style="word-break: break-all;">
                        {{$line['variation']}}
                    </td>
                    <td style="word-break: break-all;">
                        {{$line['product_custom_fields_3']}}
                    </td>
                    <td style="word-break: break-all;">
                        {{$line['product_custom_fields_2']}}
                    </td>
                    <td style="word-break: break-all;">
                        {{$line['product_custom_fields_4']}}
                    </td>
                </tr>
            @endif
        @empty
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
        @endforelse
    </table>

    @if($receipt_details->show_barcode)
        <br>
        <p style="text-align: center">
            <img class="center-block"
                 src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
        </p>
    @endif

    @if(!empty($receipt_details->footer_text))
        <br>
        <p>
            {!! $receipt_details->footer_text !!}
        </p>
    @endif

</div>