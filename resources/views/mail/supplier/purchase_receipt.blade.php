<div>
    <table  style="width: 100%">
        <tr>
            <td style="width: 22%">
                <img src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}" alt="Logo">
            </td>
            <td style="width: 47%;">
                <address>
                    <strong>{{ $purchase->business->name }}</strong>
                    @if(!empty($purchase->location->landmark))
                        <br>{{$purchase->location->landmark}}
                    @endif
                    @if(!empty($purchase->location->mobile))
                        <br>Tel: {{$purchase->location->mobile}}
                    @endif
                    @if(!empty($purchase->location->city) || !empty($purchase->location->state) || !empty($purchase->location->country))
                        <br>{{implode(',', array_filter([$purchase->location->city, $purchase->location->state, $purchase->location->country]))}}
                    @endif

                    @if(!empty($purchase->business->tax_number_1))
                        <br>{{$purchase->business->tax_label_1}}: {{$purchase->business->tax_number_1}}
                    @endif

                    @if(!empty($purchase->business->tax_number_2))
                        <br>{{$purchase->business->tax_label_2}}: {{$purchase->business->tax_number_2}}
                    @endif
                </address>
            </td>
            <td style="text-align: right">
                <strong>Contact Information :</strong>
                @if(!empty($purchase->location->mobile))
                    <br>{{$purchase->location->mobile}}
                @endif
                @if(!empty($purchase->location->email))
                    <br>{{$purchase->location->email}}
                @endif
                @if(!empty($purchase->location->website))
                    <br>{{str_replace('http://','',$purchase->location->website)}}
                @endif
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td style="width: 60%">
                @lang('purchase.supplier'):
                <address>
                    <strong>{{ $purchase->contact->supplier_business_name }}</strong>
                    {{ $purchase->contact->name }}
                    @if(!empty($purchase->contact->landmark))
                        <br>{{$purchase->contact->landmark}}
                    @endif
                    @if(!empty($purchase->contact->city) || !empty($purchase->contact->state) || !empty($purchase->contact->country))
                        <br>{{implode(',', array_filter([$purchase->contact->city, $purchase->contact->state, $purchase->contact->country]))}}
                    @endif
                    @if(!empty($purchase->contact->tax_number))
                        <br>@lang('contact.tax_no'): {{$purchase->contact->tax_number}}
                    @endif
                    @if(!empty($purchase->contact->mobile))
                        <br>@lang('contact.mobile'): {{$purchase->contact->mobile}}
                    @endif
                    @if(!empty($purchase->contact->email))
                        <br>Email: {{$purchase->contact->email}}
                    @endif
                </address>
            </td>
            <td style="">
                <div style="border: 1px solid black; padding-left: 10px">
                    <br>
                    <b>@lang('purchase.invoice'):</b> #{{ $purchase->ref_no }}<br/>
                    <br>
                    <b>Invoice @lang('messages.date'):</b> {{ @format_date($purchase->transaction_date) }}<br/>
                    <b>Invoice @lang('purchase.amount'):</b>
                    <span>${{ $purchase->final_total }}</span><br/>
                    @php
                        $p_date='';
                        $p_type='';
                    @endphp
                    @if(sizeof($purchase->payment_lines)!=0)
                        @foreach($purchase->payment_lines as $payment_line)
                            @if(!$loop->first)
                                @php $p_date.=', ';  $p_type.=', ' @endphp
                            @endif
                            @php
                                $p_date.= $payment_line->paid_on;
                                $p_type.= $payment_methods[$payment_line->method];
                            @endphp
                        @endforeach
                    @endif
                    <b>Payment @lang('messages.date'):</b> {{$p_date}}<br/>
                    <b>Payment Type:</b> {{$p_type}}
                    <br> &nbsp;
                </div>
            </td>
        </tr>
    </table>

    <br>
    <table style="width: 100%">
        <tr style="background-color: #0073b7 !important">
            <th>#</th>
            <th>Item</th>
            <th>Memory</th>
            <th>Color</th>
            <th>Condition</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        @php
            $total_before_tax = 0.00;

            $p_purchases=$purchase->purchase_lines->groupBy('product_id');
            $key=1;
        @endphp
        @foreach($p_purchases as $purchase_line)
            @php
                $line_quantity=0;
                $line_total=0;
            @endphp
            @foreach($purchase_line as $p)
                @php
                    $total_before_tax += ($p->quantity * $p->purchase_price);
                    $line_quantity+=$p->quantity;
                    $line_total+=($p->purchase_price_inc_tax * $p->quantity);
                @endphp
            @endforeach
            <tr>
                <td>{{ $key++ }}</td>
                <td>
                    {{ $purchase_line->first()->product->product_custom_field1 }}
                </td>
                <td>
                    {{ $purchase_line->first()->product->product_custom_field2 }}
                </td>
                <td>
                    {{ $purchase_line->first()->product->product_custom_field3 }}
                </td>
                <td>
                    {{ $purchase_line->first()->product->product_custom_field4 }}
                </td>
                <td>
                    <span>${{ $line_quantity }}</span>
                    @if(!empty($purchase_line->first()->sub_unit))
                        {{$purchase_line->first()->sub_unit->short_name}}
                    @else
                        {{$purchase_line->first()->product->unit->short_name}}
                    @endif
                </td>
                <td style="text-align: right">
                    <span>${{$line_total}}</span>
                </td>
            </tr>
        @endforeach
    </table>
    <br>
    <table style="width: 100%">
        <tr>
            <th>@lang('purchase.net_total_amount'):</th>
            <td></td>
            <td style="text-align: right">${{ $total_before_tax }}</td>
        </tr>
        <tr>
            <th>@lang('purchase.discount'):</th>
            <td>
                <b>(-)</b>
                @if($purchase->discount_type == 'percentage')
                    ({{$purchase->discount_amount}} %)
                @endif
            </td>
            <td style="text-align: right">
              <span >$
                @if($purchase->discount_type == 'percentage')
                      {{$purchase->discount_amount * $total_before_tax / 100}}
                  @else
                      {{$purchase->discount_amount}}
                  @endif
              </span>
            </td>
        </tr>
        <tr>
            <th>@lang('purchase.purchase_tax'):</th>
            <td><b>(+)</b></td>
            <td style="text-align: right">
                @if(!empty($purchase_taxes))
                    @foreach($purchase_taxes as $k => $v)
                        <strong>
                            <small>{{$k}}</small>
                        </strong> - <span style="text-align: right">${{ $v }}</span><br>
                    @endforeach
                @else
                    $0.00
                @endif
            </td>
        </tr>
        @if( !empty( $purchase->shipping_charges ) )
            <tr>
                <th>@lang('purchase.additional_shipping_charges'):</th>
                <td><b>(+)</b></td>
                <td style="text-align: right">${{ $purchase->shipping_charges }}</td>
            </tr>
        @endif
        <tr>
            <th>@lang('purchase.purchase_total'):</th>
            <td></td>
            <td style="text-align: right">${{ $purchase->final_total }}</td>
        </tr>
    </table>

    <br>
    <p class="text-bold">List of Purchased Phones IMEI:</p>
    <table style="width: 100%; text-align: center">
        <tr style="background-color: #0073b7 !important">
            <th>#</th>
            <th>Model</th>
            <th>IMEI</th>
            <th>Memory</th>
            <th>Color</th>
            <th>Condition</th>
        </tr>
        @foreach($purchase->purchase_lines as $purchase_line)
            @if( $purchase_line->product->type == 'variable')
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        {{ $purchase_line->product->product_custom_field1 }}
                    </td>
                    <td>
                        {{ $purchase_line->variations->name}}
                    </td>
                    <td>
                        {{ $purchase_line->product->product_custom_field2 }}
                    </td>
                    <td>
                        {{ $purchase_line->product->product_custom_field3 }}
                    </td>
                    <td>
                        {{ $purchase_line->product->product_custom_field4 }}
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
    <!--list if payment info-->
    @if(sizeof($purchase->payment_lines)!=0)
        <br>
        <h4>{{ __('sale.payment_info') }}:</h4>
        <table style="width: 100%">
            <tr style="background-color: #0073b7 !important">
                <th>#</th>
                <th>{{ __('messages.date') }}</th>
                <th>{{ __('purchase.ref_no') }}</th>
                <th>{{ __('sale.amount') }}</th>
                <th>{{ __('sale.payment_mode') }}</th>
                <th>{{ __('sale.payment_note') }}</th>
            </tr>
            @php
                $total_paid = 0;
            @endphp
            @forelse($purchase->payment_lines as $payment_line)
                @php
                    $total_paid += $payment_line->amount;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ @format_date($payment_line->paid_on) }}</td>
                    <td>{{ $payment_line->payment_ref_no }}</td>
                    <td><span>${{ $payment_line->amount }}</span></td>
                    <td>{{ $payment_methods[$payment_line->method] }}</td>
                    <td>@if($payment_line->note)
                            {{ ucfirst($payment_line->note) }}
                        @else
                            --
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        @lang('purchase.no_payments')
                    </td>
                </tr>
            @endforelse
        </table>
    @endif
<!--list if shipping cost//additional info-->
    <br>
    <table style="width: 100%">
        <tr>
            <td>
                @if($purchase->shipping_details)
                    <strong>@lang('purchase.shipping_details'):</strong><br>
                    <p class="well well-sm no-shadow bg-gray">
                        @if($purchase->shipping_details)
                            {{ $purchase->shipping_details }}
                        @else
                            --
                        @endif
                    </p>
                @endif
                @if($purchase->additional_notes)
                    <br>
                    <strong>@lang('purchase.additional_notes'):</strong><br>
                    <p class="well well-sm no-shadow bg-gray">
                        @if($purchase->additional_notes)
                            {{ $purchase->additional_notes }}
                        @else
                            --
                        @endif
                    </p>
                @endif
            </td>
        </tr>
    </table>
    <br>
    {{-- Barcode --}}
    <p style="text-align: center">
        <img class="center-block"
             src="data:image/png;base64,{{DNS1D::getBarcodePNG($purchase->ref_no, 'C128', 2,30,array(39, 48, 54), true)}}">
    </p>
</div>