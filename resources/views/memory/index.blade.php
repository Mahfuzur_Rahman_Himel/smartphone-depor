@extends('layouts.app')
@section('title', 'Memories')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang( 'memory.memories' )
        <small>@lang( 'memory.manage_your_memories' )</small>
    </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">
    @component('components.widget', ['class' => 'box-primary', 'title' => __( 'memory.all_your_memories' )])
        @can('memory.create')
            @slot('tool')
                <div class="box-tools">
                    <button type="button" class="btn btn-block btn-primary btn-modal" 
                        data-href="{{action('MemoryController@create')}}"
                        data-container=".memories_modal">
                        <i class="fa fa-plus"></i> @lang( 'messages.add' )</button>
                </div>
            @endslot
        @endcan
        @can('product.view')
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="memories_table">
                    <thead>
                        <tr>
                            <th>@lang( 'memory.memories' )</th>
{{--                            <th>@lang( 'memory.note' )</th>--}}
                            <th>@lang( 'messages.action' )</th>
                        </tr>
                    </thead>
                </table>
            </div>
        @endcan
    @endcomponent

    <div class="modal fade memories_modal" tabindex="-1" role="dialog"
    	aria-labelledby="gridSystemModalLabel">
    </div>

</section>
<!-- /.content -->

@endsection
