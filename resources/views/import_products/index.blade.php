@extends('layouts.app')
@section('title', __('product.import_products'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@lang('product.import_products')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        @if (session('notification') || !empty($notification))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        @if(!empty($notification['msg']))
                            {{$notification['msg']}}
                        @elseif(session('notification.msg'))
                            {{ session('notification.msg') }}
                        @endif
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary'])
                    {!! Form::open(['url' => action('ImportProductsController@store'), 'method' => 'post', 'enctype' => 'multipart/form-data' ]) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    {!! Form::label('name', __( 'product.file_to_import' ) . ':') !!}
                                    {!! Form::file('products_csv', ['accept'=> '.csv', 'required' => 'required']); !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <br>
                                <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <br><br>
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{{ asset('uploads/files/import_products_csv_template.csv') }}"
                               class="btn btn-success" download><i
                                        class="fa fa-download"></i> @lang('product.download_csv_file_template')</a>
                        </div>
                    </div>
                @endcomponent
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary', 'title' => __('lang_v1.instructions')])
                    <strong>@lang('lang_v1.instruction_line1')</strong><br>
                    @lang('lang_v1.instruction_line2')
                    <br><br>
                    <table class="table table-bordered text-center">
                        <tr>
                            <th rowspan="2">@lang('lang_v1.col_no')</th>
                            <th rowspan="2">@lang('lang_v1.col_name')</th>
                            <th colspan="2" class="text-center">Required</th>
                            <th rowspan="2">@lang('lang_v1.instruction')</th>
                        </tr>
                        <tr>
                            <th>IMEI MANDATORY</th>
                            <th>IMEI NOT MANDATORY</th>
                        </tr>
                        @php $i=1;@endphp
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.brand')</td>
                            <td>YES</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.brand_ins') <br>
                                <small class="text-muted">(@lang('lang_v1.brand_ins2'))</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.product_custom_field1')</td>
                            <td>YES</td>
                            <td>YES</td>
                            <td>@lang('lang_v1.product_custom_field1_ins')</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.product_custom_field2')</td>
                            <td>YES</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.product_custom_field2_ins') <br>
                                <small class="text-muted">(@lang('lang_v1.product_custom_field2_ins2'))</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.product_custom_field3')</td>
                            <td>YES</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.product_custom_field3_ins') <br>
                                <small class="text-muted">(@lang('lang_v1.product_custom_field3_ins2'))</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.product_custom_field4')</td>
                            <td>YES</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.product_custom_field4_ins') <br>
                                <small class="text-muted">(@lang('lang_v1.product_custom_field4_ins2'))</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.sku')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.sku_ins')</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.category')</td>
                            <td>YES</td>
                            <td>YES</td>
                            <td>@lang('lang_v1.category_ins') <br>
                                <small class="text-muted">(Category Must Exists In System)</small>
                                <br>
                                <small class="text-muted">(Category defines if IMEI is mandatory or not)</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.sub_category')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.sub_category_ins') <br>
                                <small class="text-muted">({!! __('lang_v1.sub_category_ins2') !!})</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.unit')</td>
                            <td>YES</td>
                            <td>YES</td>
                            <td>@lang('lang_v1.unit_ins') <br>
                                <small class="text-muted">(UNIT Must Exists In System)</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.product_description') </td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>Product Description</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td> @lang('lang_v1.purchase_price_inc_tax')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>{!! __('lang_v1.purchase_price_inc_tax_ins2') !!}</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.purchase_price_exc_tax')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>{!! __('lang_v1.purchase_price_exc_tax_ins2') !!}</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.applicable_tax')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.applicable_tax_ins') {!! __('lang_v1.applicable_tax_help') !!}</td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('product.selling_price_tax_type')</td>
                            <td>YES</td>
                            <td>YES</td>
                            <td>@lang('product.selling_price_tax_type') <br>
                                <strong>@lang('lang_v1.available_options'): inclusive, exclusive</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.profit_margin')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.profit_margin_ins')<br>
                                <small class="text-muted">{!! __('lang_v1.profit_margin_ins1') !!}</small>
                            </td>
                        </tr>
                        <tr>
                            <td>{{$i++}}</td>
                            <td>@lang('lang_v1.selling_price')</td>
                            <td>NO</td>
                            <td>NO</td>
                            <td>@lang('lang_v1.selling_price_ins')<br>
                                <small class="text-muted">{!! __('lang_v1.selling_price_ins1') !!}</small>
                            </td>
                        </tr>
                    </table>
                @endcomponent
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection