@extends('layouts.app')
@section('title', 'Search')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Search</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary'])
                    {!! Form::open([ 'method' => 'post','id'=>'search_form']) !!}
                    <div style="display: none" id="Err" class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" id="ErrText">
                                please
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('name','Search By :') !!} @show_tooltip("Choose any option")
                                    <div class="input-group col-sm-12">
                                        <select name="search_by" id="search_by" class="form-control">
                                            <option value="IMEI">IMEI</option>

{{--                                            <option value="SELL_INVOICE">SELL INVOICE</option>--}}
{{--                                            <option value="SELL_RETURN_INVOICE">SELL RETURN INVOICE</option>--}}

{{--                                            <option value="PURCHASE_INVOICE">PURCHASE INVOICE</option>--}}
{{--                                            <option value="PURCHASE_RETURN_INVOICE">PURCHASE RETURN INVOICE</option>--}}

                                            <option value="SKU">PRODUCT SKU/CODE</option>

{{--                                            <option value="IMEI_N_SELL_INVOICE">IMEI && SELL INVOICE</option>--}}
{{--                                            <option value="IMEI_N_SELL_RETURN_INVOICE">IMEI && SELL RETURN INVOICE--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_PURCHASE_INVOICE">IMEI && PURCHASE INVOICE</option>--}}
{{--                                            <option value="IMEI_N_PURCHASE_RETURN_INVOICE">IMEI && PURCHASE RETURN--}}
{{--                                                INVOICE--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_SELL_INVOICE">PRODUCT SKU/CODE && SELL INVOICE--}}
{{--                                            </option>--}}
{{--                                            <option value="IMEI_N_SELL_RETURN_INVOICE">PRODUCT SKU/CODE && SELL RETURN--}}
{{--                                                INVOICE--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_PURCHASE_INVOICE">PRODUCT SKU/CODE && PURCHASE--}}
{{--                                                INVOICE--}}
{{--                                            </option>--}}
{{--                                            <option value="IMEI_N_PURCHASE_RETURN_INVOICE">PRODUCT SKU/CODE && PURCHASE--}}
{{--                                                RETURN--}}
{{--                                                INVOICE--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">IMEI && SELL INVOICE &&--}}
{{--                                                CUSTOMER--}}
{{--                                            </option>--}}
{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">PRODUCT SKU/CODE && SELL--}}
{{--                                                INVOICE &&--}}
{{--                                                CUSTOMER--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">IMEI && SELL RETURN INVOICE--}}
{{--                                                &&--}}
{{--                                                CUSTOMER--}}
{{--                                            </option>--}}
{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">PRODUCT SKU/CODE && SELL--}}
{{--                                                RETURN INVOICE &&--}}
{{--                                                CUSTOMER--}}
{{--                                            </option>--}}

{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">IMEI && PURCHASE INVOICE &&--}}
{{--                                                SUPPLIER--}}
{{--                                            </option>--}}
{{--                                            <option value="IMEI_N_SELL_INVOICE_N_CUSTOMER">PRODUCT SKU/CODE && PURCHASE--}}
{{--                                                INVOICE &&--}}
{{--                                                SUPPLIER--}}
{{--                                            </option>--}}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="input-div" class="row">
                        {{-- field value should change according to seach_by value--}}
                        <div id="val_1" style="display: none" class="col-sm-4">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="" class=" control-label" id="input_label_1">kk</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                        {!! Form::text('search_by_input_value_1', null, ['class' => 'form-control','id'=>'search_by_input_value_1','required']); !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="val_2" style="display: none" class="col-sm-4">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('name','Input IMEI') !!}
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                        {!! Form::text('search_by_input_value_2', null, ['class' => 'form-control','id'=>'search_by_input_value_2']); !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="val_3" style="display: none" class="col-sm-4">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('name','Input IMEI') !!}
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                        {!! Form::text('search_by_input_value_3', null, ['class' => 'form-control','id'=>'search_by_input_value_3']); !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 ">
                            <button type="button" onclick="call_submit()" class="btn btn-primary pull-right"><i
                                        class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                @endcomponent
            </div>
        </div>

        <div class="row" id="result_div">
        </div>
    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            getInputField();
        });

        $('#search_by').change(function () {
            getInputField();
        });
        var input_field_1 = ['IMEI', 'SKU'];
        var input_field_2 = [];
        var input_field_3 = [];
        var input_label = {
            IMEI: "Input IMEI",
            SKU: "Input SKU"
        };

        function getInputField() {
            var tempSearchBy = $('#search_by').val();

            if (tempSearchBy == '' || tempSearchBy == null || tempSearchBy == 'undefine' || tempSearchBy == 'undifined') {
                $('#ErrText').text('Please Select a Search Option');
                $('#Err').show();
            } else {
                $('#Err').hide();
                if (input_field_1.includes(tempSearchBy)) {
                    $('#val_2').hide();
                    $('#val_3').hide();
                    $('#input_label_1').text(input_label[tempSearchBy]);
                    $('#val_1').show();
                    // console.log(input_label[tempSearchBy]);
                } else if (input_field_2.includes(tempSearchBy)) {
                    $('#val_3').hide();
                    $('#val_1').show();
                    $('#val_2').show();
                } else if (input_field_3.includes(tempSearchBy)) {
                    $('#val_1').show();
                    $('#val_2').show();
                    $('#val_3').show();
                } else {
                    $('#val_1').hide();
                    $('#val_2').hide();
                    $('#val_3').hide();
                    $('#ErrText').text('Something Went Wrong. Please Refresh(ctrl+f5) the page and try again.');
                    $('#Err').show();
                }
            }
        }

        function call_submit() {
            // console.log('in');
            var tempVal1 = null;
            var tempVal2 = null;
            var tempVal3 = null;
            var is_ajax = true;
            var tempSearchBy = $('#search_by').val();
            if (tempSearchBy == '' || tempSearchBy == null || tempSearchBy == 'undefine' || tempSearchBy == 'undifined') {
                $('#ErrText').text('Please Select a Search Option');
                $('#Err').show();
            } else {
                // console.log('in2');
                $('#Err').hide();
                if (input_field_1.includes(tempSearchBy)) {
                    // console.log('in3');
                    // console.log(tempVal1);
                    tempVal1 = $('#search_by_input_value_1').val();
                    if (tempVal1 == '' || tempVal1 == null || tempVal1 == 'null' || tempVal1 == 'undefine' || tempVal1 == 'undifined') {
                        $('#ErrText').text('Please ' + input_label[tempSearchBy]);
                        $('#Err').show();
                        is_ajax = false;
                        // console.log('in4');
                    }
                    // console.log(is_ajax);
                    if (is_ajax == true) {
                        // console.log('in5');
                        $.ajax({
                            url: '{{url('search')}}',
                            type: 'post',
                            data: {
                                search_by: tempSearchBy,
                                search_by_input_value_1: tempVal1
                            }, success: function (result) {
                                // console.log(result);

                                if (result.success) {
                                    $('#result_div').html(result.html_view);
                                }
                            }
                        });
                    }
                } else if (input_field_2.includes(tempSearchBy)) {

                } else if (input_field_3.includes(tempSearchBy)) {

                } else {
                    $('#val_1').hide();
                    $('#val_2').hide();
                    $('#val_3').hide();
                    $('#ErrText').text('Something Went Wrong. Please Refresh(ctrl+f5) the page and try again.');
                    $('#Err').show();
                }
            }
        }
    </script>
@endsection