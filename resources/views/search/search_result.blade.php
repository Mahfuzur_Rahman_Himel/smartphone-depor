@if($no_found=='--')
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#info_basic" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-th-list" aria-hidden="true"></i> Basic Info
                    </a>
                </li>

                <li>
                    <a href="#info_product" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-cubes" aria-hidden="true"></i> Product Info
                    </a>
                </li>

                <li>
                    <a href="#info_purchase" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Purchase Info
                    </a>
                </li>

                <li>
                    <a href="#info_purchase_return" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> Purchase Return Info
                    </a>
                </li>

                <li>
                    <a href="#info_sell" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Sell Info
                    </a>
                </li>
                <li>
                    <a href="#info_sell_return" data-toggle="tab" aria-expanded="true">
                        <i class="fa fa-minus-circle" aria-hidden="true"></i> Sell Return Info
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="info_basic">
                    @include('search.partials.info_basic')
                </div>

                <div class="tab-pane" id="info_product">
                    @include('search.partials.info_product')
                </div>

                <div class="tab-pane" id="info_purchase">
                    @include('search.partials.info_purchase')
                </div>

                <div class="tab-pane" id="info_purchase_return">
                    @include('search.partials.info_purchase_return')
                </div>

                <div class="tab-pane" id="info_sell">
                    @include('search.partials.info_sell')
                </div>
                <div class="tab-pane" id="info_sell_return">
                    @include('search.partials.info_sell_return')
                </div>

            </div>
        </div>
    </div>
@else
    <div class="col-sm-12">
        @component('components.widget', ['class' => 'box-primary','title' => 'Search Result'])
            <h2 class="text-center">{{$no_found}}</h2>
        @endcomponent
    </div>
@endif