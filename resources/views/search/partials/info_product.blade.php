@if($search_by=='imei')
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-9">
                <div class="col-sm-6 ">
                    <b>@lang('product.sku'):</b>
                    {{$product_info->product->sku }}
                    @if(!empty($product_info->product->product_custom_field1))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field1'): </b>
                        {{$product_info->product->product_custom_field1 }}
                    @endif

                    @if(!empty($product_info->product->product_custom_field2))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field2'): </b>
                        {{$product_info->product->product_custom_field2 }}
                    @endif

                    @if(!empty($product_info->product->product_custom_field3))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field3'): </b>
                        {{$product_info->product->product_custom_field3 }}
                    @endif

                    @if(!empty($product_info->product->product_custom_field4))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field4'): </b>
                        {{$product_info->product->product_custom_field4 }}
                    @endif
                </div>
                {{--            {{dd($product_info)}}--}}
                <div class="col-sm-6 ">
                    <b>@lang('product.brand'): </b>
                    {{$product_info->product->brand->name or '--' }}<br>
                    <b>@lang('product.unit'): </b>
                    {{$product_info->product->unit->short_name or '--' }}<br>
                    <b>@lang('product.category'): </b>
                    {{$product_info->product->category->name or '--' }}<br>
                    <b>@lang('product.sub_category'): </b>
                    {{$product_info->product->sub_category->name or '--' }}<br>

                    {{--						<b>@lang('product.barcode_type'): </b>--}}
                    {{--						{{$product_info->product->barcode_type or '--' }} <br>--}}
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-sm-12">
                    {!! $product_info->product->product_description !!}
                </div>
            </div>
            <div class="col-sm-3 text-right">
                <div class="thumbnail">
                    <img src="{{$product_info->product->image_url}}" alt="Product image">
                </div>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-9">
                <div class="col-sm-6 ">
                    <b>@lang('product.sku'):</b>
                    {{$product_info->sku }}
{{--                    {{dd($product_info)}}--}}
                    @if(!empty($product_info->product_custom_field1))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field1'): </b>
                        {{$product_info->product_custom_field1 }}
                    @endif

                    @if(!empty($product_info->product_custom_field2))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field2'): </b>
                        {{$product_info->product_custom_field2 }}
                    @endif

                    @if(!empty($product_info->product_custom_field3))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field3'): </b>
                        {{$product_info->product_custom_field3 }}
                    @endif

                    @if(!empty($product_info->product_custom_field4))
                        <br/>
                        <b>@lang('lang_v1.product_custom_field4'): </b>
                        {{$product_info->product_custom_field4 }}
                    @endif
                </div>
                {{--            {{dd($product_info)}}--}}
                <div class="col-sm-6 ">
                    <b>@lang('product.brand'): </b>
                    {{$product_info->brand or '--' }}<br>
                    <b>@lang('product.unit'): </b>
                    {{$product_info->unit or '--' }}<br>
                    <b>@lang('product.category'): </b>
                    {{$product_info->category or '--' }}<br>
                    <b>@lang('product.sub_category'): </b>
                    {{$product_info->sub_category or '--' }}<br>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-sm-12">
                    {!! $product_info->product_description !!}
                </div>
            </div>
            <div class="col-sm-3 text-right">
                <div class="thumbnail">
                    <img src="{{$product_info->image_url}}" alt="Product image">
                </div>
            </div>
        </div>
    </div>
@endif