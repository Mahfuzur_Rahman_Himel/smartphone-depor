<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>@lang('messages.date')</th>
                    <th>Invoice No</th>
                    <th>Parent Sell</th>
                    <th>Customer Name</th>
                    <th>Sell Price(Unit)</th>
                    <th>Quantity</th>
                    <th>@lang('messages.action')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sell_return_info as $sell)
                    <tr>
                        <td>{{@format_date($sell->has_return_parent->transaction_date)}}</td>
                        <td>{{$sell->has_return_parent->invoice_no}}</td>
                        <td>
                            <a href="#"
                               data-href="{{action('SellController@show', [$sell->transaction->id])}}"
                               class="btn-modal" data-container=".view_modal">
                               {{$sell->transaction->invoice_no}}
                            </a></td>
                        <td>{{$sell->has_return_parent->contact->name}}</td>
                        <td><span>$ {{$sell->unit_price_inc_tax-$sell->line_return_discount_amount}} </span></td>
                        <td>{{@number_format($sell->quantity_returned)}}</td>
                        <td>
                            <a href="#"
                               data-href="{{action('SellReturnController@show', [$sell->transaction->id])}}"
                               class="btn-modal label label-info" data-container=".view_modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7"> No Data Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>