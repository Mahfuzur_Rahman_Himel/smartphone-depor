<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>@lang('messages.date')</th>
                    <th>@lang('purchase.ref_no')</th>
                    <th>Parent Purchase</th>
                    <th>@lang('purchase.supplier')</th>
                    <th>Purchase Return Price(Unit)</th>
                    <th>Quantity</th>
                    <th>@lang('messages.action')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($purchase_return_info as $purchase)
                    <tr>
                        <td>{{@format_date($purchase->has_return_parent->transaction_date)}}</td>
                        <td>{{$purchase->has_return_parent->ref_no}}</td>
                        <td>
                            <a href="#" data-href="{{action('PurchaseController@show', [$purchase->transaction->id])}}"
                               class="btn-modal" data-container=".view_modal">
                                {{$purchase->transaction->ref_no}}
                            </a>
                        </td>
                        <td>{{$purchase->has_return_parent->contact->name}}</td>
                        <td><span>$ {{$purchase->purchase_price_inc_tax}} </span></td>
                        <td>{{@number_format($purchase->quantity_returned)}}</td>
                        <td>
                            <a href="#"
                               data-href="{{action('PurchaseReturnController@show', [$purchase->transaction->id])}}"
                               class="btn-modal label label-info" data-container=".view_modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7"> No Data Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>