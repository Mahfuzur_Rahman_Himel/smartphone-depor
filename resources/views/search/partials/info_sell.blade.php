<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>@lang('messages.date')</th>
                    <th>Invoice No</th>
                    <th>Customer Name</th>
                    <th>Sell Price(Unit)</th>
                    <th>Quantity</th>
                    <th>@lang('messages.action')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sell_info as $sell)
                    <tr>
                        <td>{{@format_date($sell->transaction->transaction_date)}}</td>
                        <td>{{$sell->transaction->invoice_no}}</td>
                        <td>{{$sell->transaction->contact->name}}</td>
                        <td><span>$ {{$sell->unit_price_inc_tax}} </span></td>
                        <td>{{@number_format($sell->quantity)}}</td>
                        <td>
                            <a href="#"
                               data-href="{{action('SellController@show', [$sell->transaction->id])}}"
                               class="btn-modal label label-info" data-container=".view_modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6"> No Data Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>