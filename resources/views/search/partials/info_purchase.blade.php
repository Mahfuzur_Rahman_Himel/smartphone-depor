<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>@lang('messages.date')</th>
                    <th>@lang('purchase.ref_no')</th>
                    <th>@lang('purchase.supplier')</th>
                    <th>Purchase Price(Unit)</th>
                    <th>Quantity</th>
                    <th>@lang('messages.action')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($purchase_info as $purchase)
                    <tr>
                        <td>{{@format_date($purchase->transaction->transaction_date)}}</td>
                        <td>{{$purchase->transaction->ref_no}}</td>
                        <td>{{$purchase->transaction->contact->name}}</td>
                        <td><span>$ {{$purchase->purchase_price_inc_tax}} </span></td>
                        <td>{{@number_format($purchase->quantity)}}</td>
                        <td>
                            <a href="#" data-href="{{action('PurchaseController@show', [$purchase->transaction->id])}}"
                               class="btn-modal label label-info" data-container=".view_modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6"> No Data Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>