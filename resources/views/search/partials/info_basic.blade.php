<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-6">
            <h4>Product Name : {{$name}}</h4>
            <h5>Purchase Date: {{($purchased_date=='--'?$purchased_date:@format_date($purchased_date))}}</h5>
            <h5>Purchase Return Date: {{($purchase_return_date=='--'?$purchase_return_date:@format_date($purchase_return_date))}}</h5>
            <h5>Sell Date: {{($sell_date=='--'?$sell_date:@format_date($sell_date))}}</h5>
            <h5>Sell Return Date: {{($sell_return_date=='--'?$sell_return_date:@format_date($sell_return_date))}}</h5>
        </div>
        <div class="col-sm-6 text-right">
            <h4>In Stock : <span class="label label-info">{{$in_stock}}</span></h4>
            <h4>Quantity : <span class="label label-info">{{@number_format($quantity)}}</span></h4>
        </div>
    </div>
</div>