@extends('layouts.app')
@section('title', __( 'report.profit_loss' ))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@lang( 'report.profit_loss' )
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="print_section"><h2>{{session()->get('business.name')}} - @lang( 'report.profit_loss' )</h2></div>

        <div class="row no-print">
            <div class="col-md-3 col-md-offset-7 col-xs-6">
                <div class="input-group">
                    <span class="input-group-addon bg-light-blue"><i class="fa fa-map-marker"></i></span>
                    <select class="form-control select2" id="profit_loss_location_filter">
                        @foreach($business_locations as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="form-group pull-right">
                    <div class="input-group">
                        <button type="button" class="btn btn-primary" id="profit_loss_date_filter">
                    <span>
                      <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                    </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-6">
                @component('components.widget')
                    <table class="table table-striped">
                        <tr>
                            <th>
                                @show_tooltip("<i>Opening stock price on selected date</i>")
                                {{ __('report.opening_stock') }}:</th>
                            <td>
                            <span class="opening_stock">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Purchase=SUM(Grand Total in List Purchases)</i>")
                                {{ __('home.total_purchase') }}:</th>
                            <td>
                             <span class="total_purchase">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                @show_tooltip("<i>Total Expense=SUM(Total Amount in List Expense)</i>")
                                {{ __('report.total_expense') }}:</th>
                            <td>
                             <span class="total_expense">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Purchase Shipping Charge=SUM(Shipping Amount in Purchase Invoice)</i>")
                                Total Purchase Shipping Charges:</th>
                            <td>
                             <span class="total_transfer_shipping_charges">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Sell Discount=SUM(Discount in Sell Invoice)</i>")
                                {{ __('lang_v1.total_sell_discount') }}:</th>
                            <td>
                             <span class="total_sell_discount">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Sell Return=SUM(Total Amount in Sell Return)</i>")
                                {{ __('lang_v1.total_sell_return') }}:</th>
                            <td>
                             <span class="total_sell_return">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                    </table>
                @endcomponent
            </div>

            <div class="col-xs-6">
                @component('components.widget')
                    <table class="table table-striped">
                        <tr>
                            <th>
                                @show_tooltip("<i>Closing Stock Price on selected date</i>")
                                {{ __('report.closing_stock') }}:</th>
                            <td>
                            <span class="closing_stock">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Sell=SUM(Total Amount on POS List)</i>")
                                {{ __('home.total_sell') }}:</th>
                            <td>
                             <span class="total_sell">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                @show_tooltip("<i>Total Purchase Return=SUM(Grand Total on Purchase Return List)</i>")
                                {{ __('lang_v1.total_purchase_return') }}:</th>
                            <td>
                             <span class="total_purchase_return">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Purchase Discount=SUM(Discount on Purchase Invoice)</i>")
                                {{ __('lang_v1.total_purchase_discount') }}:
                            </th>
                            <td>
                             <span class="total_purchase_discount">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Current Stock Price on Present date</i>")
                                &nbsp;Current Stock Price:
                            </th>
                            <td>
                             <span class="total_current_stock_price">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                @show_tooltip("<i>Total Sell Shipping Charge=SUM(Shipping Amount in Sell Invoice)</i>")
                                Total Sell Shipping Charge:</th>
                            <td>
                             <span class="total_sell_shipping_charges">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </span>
                            </td>
                        </tr>
                    </table>
                @endcomponent
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                @component('components.widget')
                    <h3 class="text-muted mb-0">
                        {{ __('lang_v1.gross_profit') }}:
                        <span class="gross_profit">
                        <i class="fa fa-refresh fa-spin fa-fw"></i>
                    </span>
                    </h3>
                    <small class="help-block">(@lang('lang_v1.total_sell_price') - @lang('lang_v1.total_purchase_price')
                        )
                    </small>

                    <h3 class="text-muted mb-0">
                        {{ __('report.net_profit') }}:
                        <span class="net_profit2">
                        <i class="fa fa-refresh fa-spin fa-fw"></i>
                    </span>
                    </h3>
                    <small class="help-block">(Profit - Expense)</small>
                    {{--                    <small class="help-block">(@lang('report.closing_stock') + @lang('home.total_sell')--}}
                    {{--                        + @lang('report.total_stock_recovered') + @lang('lang_v1.total_purchase_return')--}}
                    {{--                        + @lang('lang_v1.total_purchase_discount')) <br> - (@lang('report.opening_stock')--}}
                    {{--                        + @lang('home.total_purchase') + @lang('report.total_expense')--}}
                    {{--                        + @lang('lang_v1.total_shipping_charges') + @lang('lang_v1.total_sell_discount'))--}}
                    {{--                    </small>--}}
                @endcomponent
            </div>
        </div>

        <div class="row no-print">
            <div class="col-sm-12">
                <button type="button" class="btn btn-primary pull-right"
                        aria-label="Print" onclick="window.print();"
                ><i class="fa fa-print"></i> @lang( 'messages.print' )</button>
            </div>
        </div>
        <br>
        <div class="row no-print">
            <div class="col-xs-12">
                <div class="form-group pull-right">
                    <div class="input-group">
                        <button type="button" class="btn btn-primary" id="profit_tabs_filter">
                    <span>
                      <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                    </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-print">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#profit_by_products" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes"
                                                                                                    aria-hidden="true"></i> @lang('lang_v1.profit_by_products')
                            </a>
                        </li>

                        <li>
                            <a href="#profit_by_categories" data-toggle="tab" aria-expanded="true"><i class="fa fa-tags"
                                                                                                      aria-hidden="true"></i> @lang('lang_v1.profit_by_categories')
                            </a>
                        </li>

                        <li>
                            <a href="#profit_by_brands" data-toggle="tab" aria-expanded="true"><i class="fa fa-diamond"
                                                                                                  aria-hidden="true"></i> @lang('lang_v1.profit_by_brands')
                            </a>
                        </li>

                        <li>
                            <a href="#profit_by_locations" data-toggle="tab" aria-expanded="true"><i
                                        class="fa fa-map-marker"
                                        aria-hidden="true"></i> @lang('lang_v1.profit_by_locations')</a>
                        </li>

                        <li>
                            <a href="#profit_by_models" data-toggle="tab" aria-expanded="true"><i class="fa fa-modx"
                                                                                                  aria-hidden="true"></i>
                                Profit By Models</a>
                        </li>
                        <li>
                            <a href="#profit_by_invoice" data-toggle="tab" aria-expanded="true"><i
                                        class="fa fa-file-text-o"
                                        aria-hidden="true"></i> @lang('lang_v1.profit_by_invoice')</a>
                        </li>

                        <li>
                            <a href="#profit_by_date" data-toggle="tab" aria-expanded="true"><i class="fa fa-calendar"
                                                                                                aria-hidden="true"></i> @lang('lang_v1.profit_by_date')
                            </a>
                        </li>
                        <li>
                            <a href="#profit_by_customer" data-toggle="tab" aria-expanded="true"><i class="fa fa-user"
                                                                                                    aria-hidden="true"></i> @lang('lang_v1.profit_by_customer')
                            </a>
                        </li>
                        <li>
                            <a href="#profit_by_day" data-toggle="tab" aria-expanded="true"><i class="fa fa-calendar"
                                                                                               aria-hidden="true"></i> @lang('lang_v1.profit_by_day')
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="profit_by_products">
                            @include('report.partials.profit_by_products')
                        </div>

                        <div class="tab-pane" id="profit_by_categories">
                            @include('report.partials.profit_by_categories')
                        </div>

                        <div class="tab-pane" id="profit_by_brands">
                            @include('report.partials.profit_by_brands')
                        </div>

                        <div class="tab-pane" id="profit_by_locations">
                            @include('report.partials.profit_by_locations')
                        </div>

                        <div class="tab-pane" id="profit_by_models">
                            @include('report.partials.profit_by_models')
                        </div>
                        <div class="tab-pane" id="profit_by_invoice">
                            @include('report.partials.profit_by_invoice')
                        </div>

                        <div class="tab-pane" id="profit_by_date">
                            @include('report.partials.profit_by_date')
                        </div>

                        <div class="tab-pane" id="profit_by_customer">
                            @include('report.partials.profit_by_customer')
                        </div>

                        <div class="tab-pane" id="profit_by_day">

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#profit_tabs_filter').daterangepicker(dateRangeSettings, function (start, end) {
                $('#profit_tabs_filter span').html(
                    start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format)
                );
                $('.nav-tabs li.active').find('a[data-toggle="tab"]').trigger('shown.bs.tab');
            });
            $('#profit_tabs_filter').on('cancel.daterangepicker', function (ev, picker) {
                $('#profit_tabs_filter').html(
                    '<i class="fa fa-calendar"></i> ' + LANG.filter_by_date
                );
                $('.nav-tabs li.active').find('a[data-toggle="tab"]').trigger('shown.bs.tab');
            });

            profit_by_products_table = $('#profit_by_products_table').DataTable({
                processing: true,
                serverSide: true,
                "ajax": {
                    "url": "/reports/get-profit/product",
                    "data": function (d) {
                        d.start_date = $('#profit_tabs_filter')
                            .data('daterangepicker')
                            .startDate.format('YYYY-MM-DD');
                        d.end_date = $('#profit_tabs_filter')
                            .data('daterangepicker')
                            .endDate.format('YYYY-MM-DD');
                    }
                },
                columns: [
                    {data: 'product', name: 'P.name'},
                    {data: 'gross_profit', "searchable": false},
                ],
                fnDrawCallback: function (oSettings) {
                    var total_profit = sum_table_col($('#profit_by_products_table'), 'gross-profit');
                    $('#profit_by_products_table .footer_total').text(total_profit);

                    __currency_convert_recursively($('#profit_by_products_table'));
                },
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr('href');
                if (target == '#profit_by_categories') {
                    if (typeof profit_by_categories_datatable == 'undefined') {
                        profit_by_categories_datatable = $('#profit_by_categories_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/category",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'category', name: 'C.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_categories_table'), 'gross-profit');
                                $('#profit_by_categories_table .footer_total').text(total_profit);

                                __currency_convert_recursively($('#profit_by_categories_table'));
                            },
                        });
                    } else {
                        profit_by_categories_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_brands') {
                    if (typeof profit_by_brands_datatable == 'undefined') {
                        profit_by_brands_datatable = $('#profit_by_brands_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/brand",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'brand', name: 'B.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_brands_table'), 'gross-profit');
                                $('#profit_by_brands_table .footer_total').text(total_profit);

                                __currency_convert_recursively($('#profit_by_brands_table'));
                            },
                        });
                    } else {
                        profit_by_brands_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_locations') {
                    if (typeof profit_by_locations_datatable == 'undefined') {
                        profit_by_locations_datatable = $('#profit_by_locations_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/location",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'location', name: 'L.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_locations_table'), 'gross-profit');
                                $('#profit_by_locations_table .footer_total').text(total_profit);

                                __currency_convert_recursively($('#profit_by_locations_table'));
                            },
                        });
                    } else {
                        profit_by_locations_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_models') {
                    if (typeof profit_by_models_datatable == 'undefined') {
                        profit_by_models_datatable = $('#profit_by_models_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/models",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                    d.customer_id = $('#sell_list_filter_customer_id').val();
                                }
                            },
                            columns: [
                                {data: 'model_name', name: 'P.product_custom_field1'},
                                {data: 'customer', name: 'CU.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_models_table'), 'gross-profit');
                                $('#profit_by_models_table .footer_total').text(total_profit);

                                __currency_convert_recursively($('#profit_by_models_table'));
                            },
                        });
                    } else {
                        profit_by_models_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_invoice') {
                    if (typeof profit_by_invoice_datatable == 'undefined') {
                        profit_by_invoice_datatable = $('#profit_by_invoice_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/invoice",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'invoice_no', name: 'sale.invoice_no'},
                                {data: 't_type', name: 'sale.type'},
                                {data: 'customer', name: 'CU.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            "columnDefs": [
                                {
                                    // The `data` parameter refers to the data for the cell (defined by the
                                    // `data` option, which defaults to the column being worked with, in
                                    // this case `data: 0`.
                                    "render": function (data, type, row) {
                                        return data.replace("_", " ");
                                        // return data ;
                                    },
                                    "targets": 1
                                },
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_invoice_table'), 'gross-profit');
                                $('#profit_by_invoice_table .footer_total').text(total_profit);

                                __currency_convert_recursively($('#profit_by_invoice_table'));
                            },
                        });
                    } else {
                        profit_by_invoice_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_date') {
                    if (typeof profit_by_date_datatable == 'undefined') {
                        profit_by_date_datatable = $('#profit_by_date_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/date",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'transaction_date', name: 'sale.transaction_date'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_date_table'), 'gross-profit');
                                $('#profit_by_date_table .footer_total').text(total_profit);
                                __currency_convert_recursively($('#profit_by_date_table'));
                            },
                        });
                    } else {
                        profit_by_date_datatable.ajax.reload();
                    }
                } else if (target == '#profit_by_customer') {
                    if (typeof profit_by_customers_table == 'undefined') {
                        profit_by_customers_table = $('#profit_by_customer_table').DataTable({
                            processing: true,
                            serverSide: true,
                            "ajax": {
                                "url": "/reports/get-profit/customer",
                                "data": function (d) {
                                    d.start_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .startDate.format('YYYY-MM-DD');
                                    d.end_date = $('#profit_tabs_filter')
                                        .data('daterangepicker')
                                        .endDate.format('YYYY-MM-DD');
                                }
                            },
                            columns: [
                                {data: 'customer', name: 'CU.name'},
                                {data: 'gross_profit', "searchable": false},
                            ],
                            fnDrawCallback: function (oSettings) {
                                var total_profit = sum_table_col($('#profit_by_customer_table'), 'gross-profit');
                                $('#profit_by_customer_table .footer_total').text(total_profit);
                                __currency_convert_recursively($('#profit_by_customer_table'));
                            },
                        });
                    } else {
                        profit_by_customers_table.ajax.reload();
                    }
                } else if (target == '#profit_by_day') {
                    var start_date = $('#profit_tabs_filter')
                        .data('daterangepicker')
                        .startDate.format('YYYY-MM-DD');

                    var end_date = $('#profit_tabs_filter')
                        .data('daterangepicker')
                        .endDate.format('YYYY-MM-DD');
                    var url = '/reports/get-profit/day?start_date=' + start_date + '&end_date=' + end_date;
                    $.ajax({
                        url: url,
                        dataType: 'html',
                        success: function (result) {
                            $('#profit_by_day').html(result);
                            profit_by_days_table = $('#profit_by_day_table').DataTable({
                                "searching": false,
                                'paging': false,
                                'ordering': false,
                            });
                            var total_profit = sum_table_col($('#profit_by_day_table'), 'gross-profit');
                            $('#profit_by_day_table .footer_total').text(total_profit);
                            __currency_convert_recursively($('#profit_by_day_table'));
                        },
                    });
                } else if (target == '#profit_by_products') {
                    profit_by_products_table.ajax.reload();
                }
            });

            $('#sell_list_filter_customer_id').on('change', function () {
                profit_by_models_datatable.ajax.reload();
            });
        });
    </script>

@endsection
