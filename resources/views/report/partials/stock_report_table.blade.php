<div class="table-responsive">
    <table class="table table-bordered table-striped" id="stock_report_table">
        <thead>
            <tr>
                <th>SKU</th>
                <th>@lang('business.product')</th>
                <th>IMEI</th>
                <th>Purchase Price</th>
                <th>Sell Price</th>
                <th>@lang('report.current_stock')</th>
                <th>@lang('report.total_unit_sold')</th>
            </tr>
        </thead>
        <tfoot>
            <tr class="bg-gray font-17 text-center footer-total">
                <td colspan="5"><strong>@lang('sale.total'):</strong></td>
                <td id="footer_total_stock"></td>
                <td id="footer_total_sold"></td>
            </tr>
        </tfoot>
    </table>
</div>