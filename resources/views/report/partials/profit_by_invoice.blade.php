{{--<div class="row">--}}
{{--    <div class="col-md-3">--}}
{{--        <div class="form-group">--}}
{{--            {!! Form::label('filter_by', 'Filter By:') !!}--}}
{{--            <select name="filter_by_invoice" id="filter_by_invoice" class="form-control">--}}
{{--                <option value="all">All</option>--}}
{{--                <option value="sell">Sell Invoice</option>--}}
{{--                <option value="return">Sell Return Invoice</option>--}}
{{--            </select>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="table-responsive">
    <table class="table table-bordered table-striped table-text-center" id="profit_by_invoice_table">
        <thead>
        <tr>
            <th>@lang('sale.invoice_no')</th>
            <th>Invoice Type</th>
            <th>Customer Name</th>
            <th>@lang('lang_v1.gross_profit')</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="bg-gray font-17 footer-total">
            <td colspan="3"><strong>@lang('sale.total'):</strong></td>
            <td><span class="display_currency footer_total" data-currency_symbol="true"></span></td>
        </tr>
        </tfoot>
    </table>
</div>