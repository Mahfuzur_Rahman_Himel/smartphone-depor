<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-4">
                @if(!empty(Session::get('business.logo')))
                    <img src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}"
                         alt="Logo"></span>
                @endif
            </div>
            <div class="col-sm-8 text-right" id="businessInfo">
                <address>
                    <strong>{{ $business_info->name }}</strong>
                    @if(!empty($business_info->locations->first()->landmark))
                        <br>{{$business_info->locations->first()->landmark}}
                    @endif
                    @if(!empty($business_info->locations->first()->city) || !empty($business_info->locations->first()->state) || !empty($business_info->locations->first()->country))
                        <br>{{implode(',', array_filter([$business_info->locations->first()->city, $business_info->locations->first()->state, $business_info->locations->first()->country]))}}
                    @endif

                    @if(!empty($business_info->locations->first()->mobile))
                        <br>{{$business_info->locations->first()->mobile}}
                    @endif
                    @if(!empty($business_info->locations->first()->email))
                        <br>{{$business_info->locations->first()->email}}
                    @endif
                    @if(!empty($business_info->locations->first()->website))
                        <br>{{str_replace('http://','',$business_info->locations->first()->website)}}
                    @endif
                </address>
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <span>Bill To : </span>
            <address id="customerInfo">
                <br>
                {{ $customer_info->name }}<br>
                @if($customer_info->landmark)
                    {{ $customer_info->landmark }},
                @endif

                {{ $customer_info->city }}

                @if($customer_info->state)
                    {{ ', ' .$customer_info->state }}
                @endif
                <br>
                @if($customer_info->country)
                    {{ $customer_info->country }}
                @endif
            </address>
        </div>
        <div class="col-sm-3 text-right">
            <table class="text-right" style="width: 100%">
                <tr>
                    <th class="text-right">Account Summary:</th>
                    <th class="text-right" style="width: 20%">Sell</th>
                    <th class="text-right">Sell Return</th>
                </tr>
{{--                <tr>--}}
{{--                    <td>Previous Due :</td>--}}
{{--                    <td> <span class="display_currency" data-currency_symbol="true"--}}
{{--                               id="previousOverDue">${{$beginning_balance['sell']}}</span></td>--}}
{{--                    <td> <span class="display_currency" data-currency_symbol="true"--}}
{{--                               id="previousOverDue">${{$beginning_balance['sell_return']}}</span></td>--}}
{{--                </tr>--}}
                <tr>
                    <td>1-30 days OverDue : </td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$first_overDue['sell']}}</span></td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$first_overDue['sell_return']}}</span></td>
                </tr>
                <tr>
                    <td>31-60 days OverDue :</td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$second_overDue['sell']}}</span></td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$second_overDue['sell_return']}}</span></td>
                </tr>
                <tr>
                    <td>61-90 days OverDue :</td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$third_overDue['sell']}}</span></td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$third_overDue['sell_return']}}</span></td>
                </tr>
                <tr>
                    <td> >90 days OverDue :</td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$fourth_overDue['sell']}}</span></td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$fourth_overDue['sell_return']}}</span></td>
                </tr>
                <tr>
                    <td>Total Due :</td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$fifth_overDue['sell']}}</span></td>
                    <td> <span class="display_currency" data-currency_symbol="true"
                               id="previousOverDue">${{$fifth_overDue['sell_return']}}</span></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <br>
        <div class="col-sm-12">
            <div class="alert text-center" style="background: lightgray; font-weight: 700">
                SHOWING ALL OUTSTANDING INVOICES BETWEEN {{$startDate}} AND {{$endDate}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Due Date</th>
                    <th>Details</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right">Total Paid</th>
                    <th class="text-right">Amount Due</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $sellDue=0;
                    $sellReturnDue=0;
                @endphp
                @foreach($invoice_info as $invoice)
                    <tr>
                        <td>{{ @format_date($invoice->transaction_date) }}</td>
                        <td>
                            @if(sizeof($invoice->payment_lines)!=0)
                                {{ @format_date($invoice->payment_lines->first()->paid_on)}}
                            @else
                                {{ @format_date($invoice->transaction_date) }}
                            @endif
                        </td>
                        @php
                            $line_amount=$invoice->final_total;
                            $line_total_paid=$invoice->payment_lines->sum('amount');
                            $line_amount_due=$line_amount-$line_total_paid;
                        @endphp
                        @if($invoice->type=='sell')
                            @php
                                $sellDue+=$line_amount_due;
                            @endphp
                            <td>
                                <a href="#" class="btn-modal" data-href="{{action("SellController@show", [$invoice->id])}}"
                                   data-container=".view_modal">INVOICE#{{$invoice->invoice_no}}</a>
                            </td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_amount}}</span></td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_total_paid}}</span>
                            </td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_amount_due}}</span>
                            </td>
                        @else
                            @php
                                $sellReturnDue+=$line_amount_due;
                            @endphp
                            <td>
                                <a href="#" class="btn-modal" data-container=".view_modal"
                                   data-href="{{action('SellReturnController@show', [$invoice->return_parent_id])}}">RETURN
                                    INVOICE#{{$invoice->invoice_no}}</a>
                            </td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_amount}}</span></td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_total_paid}}</span>
                            </td>
                            <td class="text-right"><span class="display_currency"
                                                         data-currency_symbol="true">${{$line_amount_due}}</span>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
        <div class="col-sm-12">
            <hr>
            <p class="text-right" style="font-size: 16px">
                <sub>Customer have to pay</sub>Total Sell Due(USD)
                <br>
                <span class="display_currency" data-currency_symbol="true" id="totalDue">${{$sellDue}}</span>
                <br>
                <sub>{{$business_info->name}} Have To Pay</sub>Total Sell Return Due(USD)
                <br>
                <span class="display_currency" data-currency_symbol="true" id="totalDue">${{$sellReturnDue}}</span>
            </p>
        </div>
    </div>
</div>