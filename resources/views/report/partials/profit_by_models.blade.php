<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('sell_list_filter_customer_id',  __('contact.customer') . ':') !!}
            {!! Form::select('sell_list_filter_customer_id', $customers, null, ['placeholder' =>'All','class' => 'form-control select2', 'style' => 'width:100%']); !!}
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-text-center" id="profit_by_models_table">
        <thead>
        <tr>
            <th>Model</th>
            <th>Customer Name</th>
            <th>@lang('lang_v1.gross_profit')</th>
        </tr>
        </thead>
        <tfoot>
        <tr class="bg-gray font-17 footer-total">
            <td colspan="2"><strong>@lang('sale.total'):</strong></td>
            <td><span class="display_currency footer_total" data-currency_symbol="true"></span></td>
        </tr>
        </tfoot>
    </table>
</div>