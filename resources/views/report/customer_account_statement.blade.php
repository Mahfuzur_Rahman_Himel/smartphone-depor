@extends('layouts.app')
@section('title', __( 'Customer Account Statement'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        <h1>Customer Account Statement</h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">
        @component('components.filters', ['title' => __('report.filters')])
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter_customer_id',  __('contact.customer') . ':') !!}
                    {!! Form::select('filter_customer_id', $customers, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => 'None']); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter_date_range', __('report.date_range') . ':') !!}
                    {!! Form::text('filter_date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'readonly']); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <br>
                    <label>
                        <input type="checkbox" name="filter_is_unpaid" id="filter_is_unpaid" checked> Show Unpaid Invoice Only
{{--                        {!! Form::checkbox('filter_is_unpaid', 'active', true, ['class' => ' status','id'=> 'filter_is_unpaid']); !!} {{ 'Show Unpaid Invoice Only' }}--}}
                    </label>
                </div>
            </div>
        @endcomponent
        @component('components.widget', ['class' => 'box-primary', 'title' =>''])
            <div class="row" id="initial_div">
                <h2 class="text-center">Select Customer to See Account Statement</h2>
            </div>
            <div class="row" id="result_div">

            </div>

        @endcomponent
    </section>
    <!-- /.content -->
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            // $('#filter_is_unpaid:checkbox').bind('change', function(e) {
            //     if ($(this).is(':checked')) {
            //         refreshStatementData();
            //     }
            //     else {
            //         refreshStatementData();
            //     }
            // });
            //Date range as a button
            $('#filter_date_range').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#filter_date_range').val(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    // sell_table.ajax.reload();
                    refreshStatementData();
                }
            );
            $('#filter_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $('#filter_date_range').val('');
                // sell_table.ajax.reload();
                refreshStatementData()
            });

            $(document).on('change', '#filter_customer_id, #filter_is_unpaid', function () {
                // sell_table.ajax.reload();
                refreshStatementData();
            });


        });

        var statement_data;

        function refreshStatementData() {
            // console.log('in');
            let customer_id = $("#filter_customer_id").val();
            if (!customer_id)
                customer_id = -1;
            if (customer_id == -1) {
                $('#result_div').hide();
                $('#initial_div').show();
            } else {
                $('#initial_div').hide();
                let start_date = $('#filter_date_range').data('daterangepicker').startDate.format('YYYY-MM-DD');
                let end_date = $('#filter_date_range').data('daterangepicker').endDate.format('YYYY-MM-DD');
                if (statement_data) {
                    statement_data.abort();
                }
                statement_data = $.ajax(
                    {
                        url: "{{route('/reports/customer-account-statement')}}",
                        data: {
                            'customer_id': customer_id,
                            'start_date': start_date,
                            'end_date': end_date,
                            'unpaid_flag': $("#filter_is_unpaid").prop("checked")
                        },
                        dataType: 'json'
                    }).done(
                    function (result) {
                        if (result.success) {
                            $('#result_div').html(result.html_view);
                            $('#result_div').show();

                            // let amount = parseFloat(result.beginning_balance);
                            // let table_body = "<tr> <td>" + start_date + "</td><td>Beginning Balance</td><td></td><td class='text-right'>" + amount + "</td></tr>";
                            //
                            // $('#previousOverDue').text(amount);
                            //
                            // let table_data = [];
                            // $.each(result.data, function (k, v) {
                            //     let invoice_data = {};
                            //     invoice_data.date = v.transaction_date;
                            //     invoice_data.amount = parseFloat(v.final_total);
                            //     if (v.type == 'sell') {
                            //         invoice_data.details = "Invoice #" + v.invoice_no + "(due " + v.transaction_date + ")";
                            //         invoice_data.isSum = true;
                            //     } else {
                            //         invoice_data.details = "Return Invoice #" + v.invoice_no + "(due " + v.transaction_date + ")";
                            //         invoice_data.isSum = false;
                            //     }
                            //     table_data.push(invoice_data);
                            //     $.each(v.payment_lines, function (key, val) {
                            //
                            //         let payment_data = {};
                            //         payment_data.date = val.paid_on;
                            //         payment_data.amount = parseFloat(val.amount);
                            //         if (v.type == 'sell') {
                            //             payment_data.details = "Payment to Invoice #" + v.invoice_no;
                            //             payment_data.isSum = false;
                            //         } else {
                            //             payment_data.details = "Refund to Invoice #" + v.invoice_no;
                            //             payment_data.isSum = true;
                            //         }
                            //         table_data.push(payment_data);
                            //     })
                            // });
                            //
                            // table_data = table_data.sort((a, b) => new Date(a.date) - new Date(b.date));
                            //
                            // $.each(table_data, function (k, v) {
                            //
                            //     table_body += "<tr>";
                            //     table_body += "<td>";
                            //     table_body += v.date;
                            //     table_body += "</td>";
                            //     table_body += "<td>";
                            //     table_body += v.details;
                            //     table_body += "</td>";
                            //
                            //     if (v.isSum) {
                            //         table_body += "<td class='text-right'>";
                            //         // table_body += v.amount;
                            //         table_body += '<span class="display_currency" data-currency_symbol="true">'+v.amount+'</span>';
                            //         table_body += "</td>";
                            //         amount += v.amount;
                            //     } else {
                            //         table_body += "<td class='text-right'>";
                            //         // table_body += "(" + v.amount + ")";
                            //         table_body += '<span class="display_currency" data-currency_symbol="true">('+v.amount+')</span>';
                            //         table_body += "</td>";
                            //         amount -= v.amount;
                            //     }
                            //     ;
                            //     table_body += "<td class='text-right'>";
                            //     // table_body += amount;
                            //     table_body += '<span class="display_currency" data-currency_symbol="true">'+amount+'</span>';
                            //     table_body += "</td>";
                            //     table_body += "<tr>";
                            //
                            //
                            // });
                            // $("#details-table tbody").html(table_body);
                            //
                            //
                            // //customer info
                            // let customerINFOS=result.customer_info;
                            // let htmlVal='<strong>'+customerINFOS.name+'</strong><br>';
                            // htmlVal+=customerINFOS.landmark+',';
                            // htmlVal+=customerINFOS.city+',';
                            // htmlVal+=customerINFOS.state+'<br>';
                            // htmlVal+=customerINFOS.country+'<br>';
                            // htmlVal+='<br>'+customerINFOS.mobile+'<br>';
                            // htmlVal+=customerINFOS.email+'<br>';
                            //
                            // $('#customerInfo').html(htmlVal);
                            //
                            // //business info
                            // let businessINFOS = result.business_info;
                            // htmlVal='';
                            // htmlVal = '<strong>' + businessINFOS.name + '</strong>';
                            // if (businessINFOS.locations[0].landmark)
                            //     htmlVal += '<br>' + businessINFOS.locations[0].landmark;
                            // htmlVal += '<br>' + businessINFOS.locations[0].city;
                            // htmlVal += ',' + businessINFOS.locations[0].state;
                            // htmlVal += ',' + businessINFOS.locations[0].country;
                            //
                            // htmlVal += '<br><br>' + businessINFOS.locations[0].mobile;
                            // htmlVal += '<br>' + businessINFOS.locations[0].email;
                            // htmlVal += '<br>' + businessINFOS.locations[0].website;
                            //
                            // $('#businessInfo').html(htmlVal);
                            //
                            // $('#result_div').show();
                        }
                    }
                ).always(function () {
                    statement_data = undefined;
                });
            }
        }

    </script>
    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
@endsection