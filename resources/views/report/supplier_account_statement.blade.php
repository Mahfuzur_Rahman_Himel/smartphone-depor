@extends('layouts.app')
@section('title', __( 'Supplier Account Statement'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        <h1>Supplier Account Statement</h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">
        @component('components.filters', ['title' => __('report.filters')])
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter_supplier_id',  'Supplier :') !!}
                    {!! Form::select('filter_supplier_id', $suppliers, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => 'None']); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter_date_range', __('report.date_range') . ':') !!}
                    {!! Form::text('filter_date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'readonly']); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <br>
                    <label>
                        <input type="checkbox" name="filter_is_unpaid" id="filter_is_unpaid" checked> Show Unpaid Invoice Only
                        {{--                        {!! Form::checkbox('filter_is_unpaid', 'active', true, ['class' => ' status','id'=> 'filter_is_unpaid']); !!} {{ 'Show Unpaid Invoice Only' }}--}}
                    </label>
                </div>
            </div>
        @endcomponent
        @component('components.widget', ['class' => 'box-primary', 'title' =>''])
            <div class="row" id="initial_div">
                <h2 class="text-center">Select Supplier to See Account Statement</h2>
            </div>
            <div class="row" id="result_div">

            </div>
        @endcomponent
    </section>
    <!-- /.content -->
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            //Date range as a button
            $('#filter_date_range').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#filter_date_range').val(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    // sell_table.ajax.reload();
                    refreshStatementData();
                }
            );
            $('#filter_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $('#filter_date_range').val('');
                // sell_table.ajax.reload();
                refreshStatementData()
            });

            $(document).on('change', '#filter_supplier_id, #filter_is_unpaid', function () {
                // sell_table.ajax.reload();
                refreshStatementData();
            });


        });

        var statement_data;

        function refreshStatementData() {
            // console.log('in');
            let supplier_id = $("#filter_supplier_id").val();
            if (!supplier_id)
                supplier_id = -1;
            if (supplier_id == -1) {
                $('#result_div').hide();
                $('#initial_div').show();
            } else {
                $('#initial_div').hide();
                let start_date = $('#filter_date_range').data('daterangepicker').startDate.format('YYYY-MM-DD');
                let end_date = $('#filter_date_range').data('daterangepicker').endDate.format('YYYY-MM-DD');
                if (statement_data) {
                    statement_data.abort();
                }
                statement_data = $.ajax(
                    {
                        url: "{{route('/reports/supplier-account-statement')}}",
                        data: {
                            'supplier_id': supplier_id,
                            'start_date': start_date,
                            'end_date': end_date,
                            'unpaid_flag': $("#filter_is_unpaid").prop("checked")
                        },
                        dataType: 'json'
                    }).done(
                    function (result) {
                        if (result.success) {
                            $('#result_div').html(result.html_view);
                            $('#result_div').show();
                        }
                    }
                ).always(function () {
                    statement_data = undefined;
                });
            }
        }

    </script>
    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
@endsection