@extends('layouts.app')
@section('title', 'Stock Match Report')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Stock Match</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary', 'title' => 'Search IMEI Number'])
                    <div class="form-group">
                        @php
                            $tmpSearchImei=$page_data->where('is_search',1);
                        @endphp

                        @forelse ($tmpSearchImei as $p)
                            <div class="col-sm-2">{{$p->imei}}</div>
                        @empty
                            <p>No Search Imei Found</p>
                        @endforelse
                    </div>
                @endcomponent
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary', 'title' => 'Stock-Match Report'])
                    <div class="table-responsive">
                        @php
                            $totalNotInSystem=0;
                            $totalPurchaseReturn=0;
                            $totalSell=0;
                            $totalInStock=0;
                            $totalError=0;
                            $totalNotSearch=0;
                            $total_imei=0;
                        @endphp
                        <table class="table table-bordered table-striped" id="stock_match_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>IMEI</th>
                                <th>STATUS</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($page_data as $p)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$p->imei}}</td>
                                    <td><strong>{{$p->result}}</strong></td>
                                </tr>
                                @if($p->result=='NOT FOUND IN SYSTEM')
                                    @php($totalNotInSystem++)
                                @elseif($p->result=='PURCHASED RETURNED')
                                    @php($totalPurchaseReturn++)
                                @elseif($p->result=='SOLD')
                                    @php($totalSell++)
                                @elseif($p->result=='IN STOCK(PURCHASED)')
                                    @php($totalInStock++)
                                @elseif($p->result=='IN STOCK(SELL RETURNED)')
                                    @php($totalInStock++)
                                @elseif($p->result=='EXISTS IN SYSTEM BUT COULD NOT FOUND ANY RECORD')
                                    @php($totalError++)
                                @elseif($p->result=='EXISTS IN SYSTEM(not search)')
                                    @php($totalNotSearch++)
                                @endif
                            @endforeach
                            @php($total_imei=$totalNotInSystem + $totalPurchaseReturn + $totalSell + $totalInStock + $totalError)
                            </tbody>
                            <tfoot>

                            <tr class="bg-gray font-17 text-center footer-total">
                                <td><strong>Summary</strong></td>
                                <td>Total Search IMEI: {{$total_imei}} <br>
                                    Total Exists In System IMEI(Not Search) : {{$totalNotSearch}}

                                </td>
                                <td>
                                    Total Not In Sysyem : {{$totalNotInSystem}} <br>
                                    Total Purchase Return : {{$totalPurchaseReturn}} <br>
                                    Total Sell : {{$totalSell}} <br>
                                    Total In Stock : {{$totalInStock}} <br>
                                    Total Other Status : {{$totalError}}
                                </td>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                @endcomponent
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#stock_match_table').dataTable();
        });
    </script>
@endsection