@extends('layouts.app')
@section('title', 'Stock Match List')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Stock Match</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary', 'title' => 'Stock-Match List'])
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="stock_match_list_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($match_list as $p)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{@format_date($p->created_at)}}</td>
                                    <td>
                                        <a href="{{url('reports/stock-match-list-details/'.$p->flag)}}" class="btn btn-sm btn-primary">Details</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                @endcomponent
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#stock_match_list_table').dataTable();
        });

    </script>
@endsection