@extends('layouts.app')
@section('title', 'Stock Match')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Stock Match</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary'])
                    {!! Form::open(['url' => action('ReportController@getStockMatch'), 'method' => 'post', 'enctype' => 'multipart/form-data' ,'id'=>'form-stock-match']) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('name','IMEI:') !!} @show_tooltip("Press <i><b>tab</b></i> or click
                                    <i class='fa fa-arrow-circle-right'></i> to add more than one IMEI number")

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                        {!! Form::text('imei', null, ['class' => 'form-control','id'=>'imei']); !!}
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default bg-white btn-flat next_imei"
                                                    id="next_imei"
                                                    data-name=""><i
                                                        class="fa fa-arrow-circle-right text-primary fa-lg"></i></button>
                                        </span>
                                    </div>
                                    <br>
                                    <span id="imeiErr" style="display: none; font-size: 14px"
                                          class="label label-danger"></span>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <button id="submit-btn" type="button"
                                        class="btn btn-primary">@lang('messages.submit')</button>
                            </div>
                        </div>
                        <div class="col-sm-6" id="allIMEI">
                            <table class="table">
                                <tr>
                                    <th>LIST OF IMEI</th>
                                    <th class="text-left" style="width: 20%">TOTAL <span id="totalIMEICount">0</span>
                                    </th>
                                </tr>
                            </table>
                            <div class="table-responsive" style="overflow-y: auto; max-height: 300px">
                                <table class="table" id="IMEItable">
                                </table>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                @endcomponent
            </div>
        </div>
    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#stock_match_table').dataTable();
            $('#imeiErr').hide();
        });
        $('#submit-btn').click(function () {
            // console.log('gg');
            $('#form-stock-match').submit();
        });
        $(document).on('click', '.imei-close', function () {
            $(this).closest('tr').remove();
        });
        $('#next_imei').click(function () {
            update_imei_list();
        });
        $('#imei').focusout(function () {
            // console.log('focusout')
            update_imei_list();
        });

        $('#imei').keypress(function(event) {
            if (event.keyCode == 13 || event.which == 13) {
                update_imei_list();
                event.preventDefault();
            }
        });

        function update_imei_list() {
            var tempImei = $('#imei').val();
            if (tempImei.trim() == null || tempImei.trim() == '') {
                $('#imeiErr').text('IMEI Cannot Be Empty');
                $('#imeiErr').show();
            } else {
                var isDuplicate = false;
                $('#IMEItable tbody tr').each(function () {
                    var temp_table_imei = $(this).find('td:first').text();
                    // console.log('=='+tempImei+'=='+temp_table_imei+'==');
                    if (temp_table_imei.trim() == tempImei.trim()) {
                        isDuplicate = true;
                    }
                });
                // console.log(isDuplicate);
                if (isDuplicate == true) {
                    $('#imeiErr').text('Duplicate IMEI');
                    $('#imeiErr').show();
                } else {
                    $('#imeiErr').hide();
                    var trow = '<tr>' +
                        '<td>' + tempImei + ' <input type="hidden" name="imei_number[]" value="' + tempImei.trim() + '"></td>' +
                        '<td style="width: 20%"><span class="label label-danger imei-close"><i class="fa fa-trash-o"> </i></span></td>' +
                        '</tr>';
                    $('table#IMEItable').append(trow);
                    $('#imei').val('');
                    $('#imei').focus();
                }
            }
            var rowCount = $('#IMEItable tr').length;
            $("#totalIMEICount").text(rowCount);
        }
    </script>
@endsection