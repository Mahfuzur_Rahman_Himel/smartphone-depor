@extends('layouts.app')
@section('title', 'Stock Check')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Stock Check</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        @if (session('notification') || !empty($notification))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        @if(!empty($notification['msg']))
                            {{$notification['msg']}}
                        @elseif(session('notification.msg'))
                            {{ session('notification.msg') }}
                        @endif
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-12">
                @component('components.widget', ['class' => 'box-primary'])
                    {!! Form::open(['url' => action('ReportController@getStockCheck'), 'method' => 'post', 'enctype' => 'multipart/form-data' ]) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    {!! Form::label('name', __( 'product.file_to_import' ) . ':') !!}
                                    {!! Form::file('products_csv', ['accept'=> '.csv', 'required' => 'required']); !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <br>
                                <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="col-sm-6 pull-right">
                                <br>
                                <a href="{{ asset('uploads/files/import_stock_check_csv_template.csv') }}"
                                   class="btn btn-success" download><i
                                            class="fa fa-download"></i> @lang('product.download_csv_file_template')</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                @endcomponent
            </div>
        </div>
        @if($is_post==1)
            <div class="row">
                <div class="col-sm-12">
                    @component('components.widget', ['class' => 'box-primary', 'title' => 'Stock-Check Report'])
                        <br><br>
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>IMEI</th>
                                <th>STATUS</th>
                            </tr>
                            @php
                                $i=1;
                                $totalNotInSystem=sizeof($not_in_system);
                                $totalPurchaseReturn=sizeof($in_purchase_return);
                                $totalSell=sizeof($in_sell);
                                $totalInStock=sizeof($in_stock)+sizeof($in_sell_return);
                                $totalError=sizeof($error1);
                                $total_imei=$totalNotInSystem+$totalPurchaseReturn+$totalSell+$totalInStock+$totalError;
                            @endphp
                            @foreach($not_in_system as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>NOT FOUND IN SYSTEM</strong></td>
                                </tr>
                            @endforeach
                            @foreach($in_purchase_return as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>PURCHASED RETURNED</strong></td>
                                </tr>
                            @endforeach
                            @foreach($in_sell as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>SOLD</strong></td>
                                </tr>
                            @endforeach
                            @foreach($in_stock as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>IN STOCK(PURCHASED)</strong></td>
                                </tr>
                            @endforeach
                            @foreach($in_sell_return as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>IN STOCK(SELL RETURNED)</strong></td>
                                </tr>
                            @endforeach
                            @foreach($error1 as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p}}</td>
                                    <td><strong>EXISTS IN SYSTEM BUT COULD NOT FOUND ANY RECORD</strong></td>
                                </tr>
                            @endforeach
                            <tr style="font-size: 16px">
                                <td>Summary</td>
                                <td>
                                    <strong>Total IMEI:</strong>{{$total_imei}}
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong>Total Not In Sysyem</strong>
                                        </div>
                                        <div class="col-sm-1">: {{$totalNotInSystem}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong>Total Purchase Return:</strong>
                                        </div>
                                        <div class="col-sm-1">: {{$totalPurchaseReturn}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong>Total Sell</strong>
                                        </div>
                                        <div class="col-sm-1">: {{$totalSell}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong>Total In Stock</strong>
                                        </div>
                                        <div class="col-sm-1">: {{$totalInStock}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <strong>Total Other Status</strong>
                                        </div>
                                        <div class="col-sm-1">: {{$totalError}}</div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="stock_check_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>IMEI</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i=1;
                                    $totalNotInSystem=sizeof($not_in_system);
                                    $totalPurchaseReturn=sizeof($in_purchase_return);
                                    $totalSell=sizeof($in_sell);
                                    $totalInStock=sizeof($in_stock)+sizeof($in_sell_return);
                                    $totalError=sizeof($error1);
                                    $total_imei=$totalNotInSystem+$totalPurchaseReturn+$totalSell+$totalInStock+$totalError;
                                @endphp
                                @foreach($not_in_system as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>NOT FOUND IN SYSTEM</strong></td>
                                    </tr>
                                @endforeach
                                @foreach($in_purchase_return as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>PURCHASED RETURNED</strong></td>
                                    </tr>
                                @endforeach
                                @foreach($in_sell as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>SOLD</strong></td>
                                    </tr>
                                @endforeach
                                @foreach($in_stock as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>IN STOCK(PURCHASED)</strong></td>
                                    </tr>
                                @endforeach
                                @foreach($in_sell_return as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>IN STOCK(SELL RETURNED)</strong></td>
                                    </tr>
                                @endforeach
                                @foreach($error1 as $p)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$p}}</td>
                                        <td><strong>EXISTS IN SYSTEM BUT COULD NOT FOUND ANY RECORD</strong></td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-gray font-17 text-center footer-total">
                                    <td><strong>Summary</strong></td>
                                    <td>Total IMEI: {{$total_imei}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <strong>Total Not In Sysyem</strong>
                                            </div>
                                            <div class="col-sm-1">: {{$totalNotInSystem}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <strong>Total Purchase Return:</strong>
                                            </div>
                                            <div class="col-sm-1">: {{$totalPurchaseReturn}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <strong>Total Sell</strong>
                                            </div>
                                            <div class="col-sm-1">: {{$totalSell}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <strong>Total In Stock</strong>
                                            </div>
                                            <div class="col-sm-1">: {{$totalInStock}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <strong>Total Other Status</strong>
                                            </div>
                                            <div class="col-sm-1">: {{$totalError}}</div>
                                        </div>
                                    </td>

                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    @endcomponent
                </div>
            </div>
        @endif
    </section>
    <!-- /.content -->

@endsection