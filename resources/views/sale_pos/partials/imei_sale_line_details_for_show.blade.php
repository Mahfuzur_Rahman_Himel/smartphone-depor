<table class="table bg-gray">
    <tr class="bg-blue">
        <th>#</th>
        <th>NAME</th>
        <th>IMEI</th>
        <th>COLOR</th>
        <th>MEMORY</th>
        <th>CONDITION</th>
    </tr>
    @php($sell_index=1)
    @foreach($sell->sell_lines as $sell_line)
        @if($sell_line->product->type=='variable')
            <tr>
                <td>{{ $sell_index++ }}</td>
                <td>
                    {{ $sell_line->product->product_custom_field1 }}
                </td>
                <td>
                    {{ $sell_line->variations->name }}
                </td>
                <td>
                    {{ $sell_line->product->product_custom_field3 }}
                </td>
                <td>
                    {{ $sell_line->product->product_custom_field2 }}
                </td>
                <td>
                    {{ $sell_line->product->product_custom_field4 }}
                </td>
            </tr>
        @endif
    @endforeach
</table>