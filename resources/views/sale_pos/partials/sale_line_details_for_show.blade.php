<table class="table bg-gray">
    <tr class="bg-blue">
        <th>#</th>
        <th>Item</th>
        <th>{{ __('sale.qty') }}</th>
        <th>Price</th>
        <th>Amount</th>
    </tr>
    @php
        $tmp_sell=$sell->sell_lines->groupBy('product_id');
        $sell_index=1;
    @endphp
    @foreach($tmp_sell as $sell_line)
        @php
            $line_quantity=$sell_line->sum('quantity');
            $line_unit_price_inc_tax=$sell_line->avg('unit_price_inc_tax');
            $line_total=$line_quantity*$line_unit_price_inc_tax;
        @endphp
        <tr>
            <td>{{ $sell_index++ }}</td>
            <td>
                {{ $sell_line->first()->product->name }}
            </td>
            <td>
                <span class="display_currency" data-currency_symbol="false"
                      data-is_quantity="true">{{ $line_quantity }}</span> @if(!empty($sell_line->first()->sub_unit)) {{$sell_line->first()->sub_unit->short_name}} @else {{$sell_line->first()->product->unit->short_name}} @endif
            </td>
            <td>
                    <span class="display_currency"
                          data-currency_symbol="true">{{ $line_unit_price_inc_tax }}</span>
            </td>
            <td class="text-right">
                <span class="display_currency"
                      data-currency_symbol="true">{{ $line_total }}</span>
            </td>
        </tr>
    @endforeach
</table>