<div class="modal-header no-print">
  <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="modalTitle"> @lang('sale.sell_details') (<b>@lang('sale.invoice_no')
      :</b> {{ $sell->invoice_no }})
  </h4>
</div>
<div class="modal-body">
  @include('layouts.partials.sell-report-header')

  <div class="row invoice-info">
    <div class="col-sm-9 invoice-col-n">
      <b>Bill To,</b>
      <br>
      {{ $sell->contact->name }}<br>
      @if(!empty($sell->billing_address()))
        {{$sell->billing_address()}}
      @else
        @if($sell->contact->landmark)
          {{ $sell->contact->landmark }},
        @endif

        {{ $sell->contact->city }}

        @if($sell->contact->state)
          {{ ', ' . $sell->contact->state }}
        @endif
        <br>
        @if($sell->contact->country)
          {{ $sell->contact->country }}
        @endif
      @endif
    </div>
    <div class="col-sm-3 invoice-col-n">
      <b>{{ __('sale.invoice_no') }}:</b> #{{ $sell->invoice_no }}<br>
      <b>Invoice Date :</b> {{ @format_date($sell->transaction_date) }}<br>
      <b>Payment Due :</b> <span id="payment_due_span" class="display_currency"
                                 data-currency_symbol="true">0</span><br>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h4>{{ __('sale.products') }}:</h4>
    </div>

    <div class="col-sm-12 col-xs-12">
      <div class="table-responsive">
        @include('sale_pos.partials.sale_line_details_for_show')
      </div>
    </div>
  </div>

  <div class="row">
    <div class="no-print col-sm-12 col-xs-12">
      <h4>{{ __('sale.payment_info') }}:</h4>
    </div>
    <div class="no-print col-md-6 col-sm-12 col-xs-12">
      <div class="table-responsive">
        <table class="table bg-gray">
          <tr class="bg-blue">
            <th>#</th>
            <th>{{ __('messages.date') }}</th>
            <th>{{ __('purchase.ref_no') }}</th>
            <th>{{ __('sale.amount') }}</th>
            <th>{{ __('sale.payment_mode') }}</th>
            <th>{{ __('sale.payment_note') }}</th>
          </tr>
          @php
            $total_paid = 0;
            $payMSG = '';
          @endphp
          @foreach($sell->payment_lines as $payment_line)
            @php
              if($payment_line->is_return == 1){
                $total_paid -= $payment_line->amount;
              } else {
                $total_paid += $payment_line->amount;
              }
            @endphp
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ @format_date($payment_line->paid_on) }}
                @php
                  $payMSG ='Payment on '. explode(' ',$payment_line->paid_on)[0];
                @endphp
              </td>
              <td>{{ $payment_line->payment_ref_no }}</td>
              <td><span class="display_currency"
                        data-currency_symbol="true">{{ $payment_line->amount }}</span></td>
              <td>
                {{ $payment_types[$payment_line->method] or $payment_line->method }}
                @php
                  $payMSG.=' using '.($payment_types[$payment_line->method]?$payment_types[$payment_line->method]:$payment_line->method)
                @endphp
                @if($payment_line->is_return == 1)
                  <br/>
                  ( {{ __('lang_v1.change_return') }} )
                @endif
              </td>
              <td>@if($payment_line->note)
                  {{ ucfirst($payment_line->note) }}
                @else
                  --
                @endif
              </td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="table-responsive">
        @php $totalDUE=0; @endphp
        <table class="table bg-gray">
          <tr>
            <th>{{ __('sale.total') }}:</th>
            <td></td>
            <td><span class="display_currency pull-right"
                      data-currency_symbol="true">{{ $sell->total_before_tax }}</span></td>
          </tr>
          <tr>
            <th>{{ __('sale.discount') }}:</th>
            <td><b>(-)</b></td>
            <td>
              <span class="pull-right">{{ $sell->discount_amount }} @if( $sell->discount_type == 'percentage') {{ '%'}} @endif</span>
            </td>
          </tr>
          <tr>
            <th>{{ __('sale.order_tax') }}:</th>
            <td><b>(+)</b></td>
            <td class="text-right">
              @if(!empty($order_taxes))
                @foreach($order_taxes as $k => $v)
                  <strong>
                    <small>{{$k}}</small>
                  </strong> - <span class="display_currency pull-right"
                                    data-currency_symbol="true">{{ $v }}</span><br>
                @endforeach
              @else
                0.00
              @endif
            </td>
          </tr>
          <tr>
            <th>{{ __('sale.shipping') }}: @if($sell->shipping_details)({{$sell->shipping_details}}
              ) @endif <br> Track Number: @if($sell->shipping_track_number)
                ({{$sell->shipping_track_number}}) @endif</th>
            <td><b>(+)</b></td>
            <td><span class="display_currency pull-right"
                      data-currency_symbol="true">{{ $sell->shipping_charges }}</span></td>
          </tr>
          <tr>
            <th>{{ __('sale.total_payable') }}:</th>
            <td></td>
            <td><span class="display_currency pull-right">{{ $sell->final_total }}</span></td>
          </tr>
          <tr>
            <th>{{ __('sale.total_paid') }}:
              <br>{{$payMSG}}</th>
            <td></td>
            <td><span class="display_currency pull-right"
                      data-currency_symbol="true">{{ $total_paid }}</span></td>
          </tr>
          <tr>
            <th>{{ __('sale.total_remaining') }}:</th>
            <td></td>
            <td><span class="display_currency pull-right"
                      data-currency_symbol="true">{{ $sell->final_total - $total_paid }}</span></td>
            @php($totalDUE=$sell->final_total - $total_paid)
          </tr>
        </table>
        <script>
          var ttlDue = '{{$totalDUE}}';
          document.getElementById("payment_due_span").innerHTML = ttlDue;
        </script>
      </div>
    </div>
  </div>

  <br>
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h4>List of sold phones IMEI number(s):</h4>
    </div>

    <div class="col-sm-12 col-xs-12">
      <div class="table-responsive">
        @include('sale_pos.partials.imei_sale_line_details_for_show')
      </div>
    </div>
  </div>

  <div class="row no-print">
    <div class="col-sm-6">
      <strong>{{ __( 'sale.sell_note')}}:</strong><br>
      <p class="well well-sm no-shadow bg-gray">
        @if($sell->additional_notes)
          {{ $sell->additional_notes }}
        @else
          --
        @endif
      </p>
    </div>
    <div class="col-sm-6">
      <strong>{{ __( 'sale.staff_note')}}:</strong><br>
      <p class="well well-sm no-shadow bg-gray">
        @if($sell->staff_note)
          {{ $sell->staff_note }}
        @else
          --
        @endif
      </p>
    </div>
  </div>

  <hr>
  <div class="row">
    <div class="col-sm-12">
      <p class="">
        If you are not satisfied with your purchased items,
        you may return the product(s) for a full refund or
        exchange the product(s) within <b>30</b> day of purchase.
        Refunds will be processed through the original forms of payment.
        Any product you return must be in the same condition you received
        it and in the original packaging. Please keep the receipt.
        Please contact <b>sales@smartphone-depot.com</b> for return &
        exchange inquiries and to receive further instructions on how to
        ship products back to us.
      </p>
      <p class="text-bold">
        Smartphone-Depot reserve the rights to accept, deny or exchange any RMA. <br>
        If paying via Paypal or Credit Card standard 3.5% fee are applied to the invoice.
      </p>
      <p>
        Wire Instructions: <br>
        Wells Fargo <br>
        420 Montgomery Street, San Francisco, CA 94101 <br>
        Account Number: 9868767352 <br>
        Routing Number: 121000248 <br>
        SWIFT/BIC Code: WFBIUS6S (International Wire Only)
      </p>
    </div>
  </div>
</div>