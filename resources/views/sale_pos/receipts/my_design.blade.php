<!-- business information here -->
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        @if(!empty(Session::get('business.logo')))
            <img style="width: 50%" src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}"
                 alt="Logo"></span>
        @endif
    </div>
    <div class="col-sm-4 invoice-col">
        <address>
            <strong>{{ $receipt_details->business_name }}</strong> <br>

            @if(!empty($receipt_details->address))
                <small class="text-center">
                    {!! $receipt_details->address !!}
                </small>
            @endif
        </address>
    </div>
    <div class="col-sm-4 text-right invoice-col">
        <strong>Contact Information :</strong>
        @if(!empty($receipt_details->contact))
            <br/>{{ $receipt_details->contact }}
        @endif
        @if(!empty($receipt_details->contact) && !empty($receipt_details->website))
            ,
        @endif
        @if(!empty($receipt_details->website))
            {{ $receipt_details->website }}
        @endif
    </div>
</div>


<div class="row">
    <!-- business information here -->
    <div class="col-xs-12 text-center">

        <!-- Invoice  number, Date  -->
        <p style="width: 100% !important" class="word-wrap">
			<span class="pull-left text-left word-wrap">
				@if(!empty($receipt_details->invoice_no_prefix))
                    <b>{!! $receipt_details->invoice_no_prefix !!}</b>
                @endif
                {{$receipt_details->invoice_no}}

            <!-- Table information-->
                @if(!empty($receipt_details->table_label) || !empty($receipt_details->table))
                    <br/>
                    <span class="pull-left text-left">
						@if(!empty($receipt_details->table_label))
                            <b>{!! $receipt_details->table_label !!}</b>
                    @endif
                    {{$receipt_details->table}}

                    <!-- Waiter info -->
					</span>
                @endif

            <!-- customer info -->
                @if(!empty($receipt_details->customer_name))
                    <br/>
                    <b>{{ $receipt_details->customer_label }}</b> {{ $receipt_details->customer_name }} <br>
                @endif
                @if(!empty($receipt_details->customer_info))
                    {!! $receipt_details->customer_info !!}
                @endif
                @if(!empty($receipt_details->client_id_label))
                    <br/>
                    <b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
                @endif
                @if(!empty($receipt_details->customer_tax_label))
                    <br/>
                    <b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
                @endif
                @if(!empty($receipt_details->customer_custom_fields))
                    <br/>{!! $receipt_details->customer_custom_fields !!}
                @endif
                @if(!empty($receipt_details->sales_person_label))
                    <br/>
                    <b>{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
                @endif
			</span>

            <span class="pull-right text-left">
				<b>{{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}

                @if(!empty($receipt_details->serial_no_label) || !empty($receipt_details->repair_serial_no))
                    <br>
                    @if(!empty($receipt_details->serial_no_label))
                        <b>{!! $receipt_details->serial_no_label !!}</b>
                    @endif
                    {{$receipt_details->repair_serial_no}}<br>
                @endif
                @if(!empty($receipt_details->repair_status_label) || !empty($receipt_details->repair_status))
                    @if(!empty($receipt_details->repair_status_label))
                        <b>{!! $receipt_details->repair_status_label !!}</b>
                    @endif
                    {{$receipt_details->repair_status}}<br>
                @endif

                @if(!empty($receipt_details->repair_warranty_label) || !empty($receipt_details->repair_warranty))
                    @if(!empty($receipt_details->repair_warranty_label))
                        <b>{!! $receipt_details->repair_warranty_label !!}</b>
                    @endif
                    {{$receipt_details->repair_warranty}}
                    <br>
                @endif

            <!-- Waiter info -->
                @if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff))
                    <br/>
                    @if(!empty($receipt_details->service_staff_label))
                        <b>{!! $receipt_details->service_staff_label !!}</b>
                    @endif
                    {{$receipt_details->service_staff}}
                @endif
			</span>
        </p>
    </div>

    @if(!empty($receipt_details->defects_label) || !empty($receipt_details->repair_defects))
        <div class="col-xs-12">
            <br>
            @if(!empty($receipt_details->defects_label))
                <b>{!! $receipt_details->defects_label !!}</b>
            @endif
            {{$receipt_details->repair_defects}}
        </div>
@endif
<!-- /.col -->
</div>


<div class="row">
    <div class="col-xs-12">
        <br/><br/>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>Items</th>
                <th>{{$receipt_details->table_qty_label}}</th>
                <th>Price</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @php
                $tmp_sell=$receipt_details->lines->groupBy('product_id');
                $sell_index=1;
            @endphp
            @forelse($tmp_sell as $line)
                <tr>
                    <td style="word-break: break-all;">
                        {{$line['name']}}
                    </td>
                    <td>{{$line['quantity']}} {{$line['units']}} </td>
                    <td>{{$line['unit_price_inc_tax']}}</td>
                    <td>{{$line['line_total']}}</td>
                </tr>
                @if(!empty($line['modifiers']))
                    @foreach($line['modifiers'] as $modifier)
                        <tr>
                            <td>
                                {{$modifier['name']}} {{$modifier['variation']}}
                                @if(!empty($modifier['sub_sku']))
                                    , {{$modifier['sub_sku']}} @endif @if(!empty($modifier['cat_code']))
                                    , {{$modifier['cat_code']}}@endif
                                @if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif
                            </td>
                            <td>{{$modifier['quantity']}} {{$modifier['units']}} </td>
                            <td>{{$modifier['unit_price_inc_tax']}}</td>
                            <td>{{$modifier['line_total']}}</td>
                        </tr>
                    @endforeach
                @endif
            @empty
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <br/>
    <div class="col-md-12">
        <hr/>
    </div>
    <br/>

    <div class="col-xs-6">

        <table class="table table-condensed">

            @if(!empty($receipt_details->payments))
                @foreach($receipt_details->payments as $payment)
                    <tr>
                        <td>{{$payment['method']}}</td>
                        <td>{{$payment['amount']}}</td>
                        <td>{{$payment['date']}}</td>
                    </tr>
                @endforeach
            @endif

        <!-- Total Paid-->
            @if(!empty($receipt_details->total_paid))
                <tr>
                    <th>
                        {!! $receipt_details->total_paid_label !!}
                    </th>
                    <td>
                        {{$receipt_details->total_paid}}
                    </td>
                </tr>
            @endif

        <!-- Total Due-->
            @if(!empty($receipt_details->total_due))
                <tr>
                    <th>
                        {!! $receipt_details->total_due_label !!}
                    </th>
                    <td>
                        {{$receipt_details->total_due}}
                    </td>
                </tr>
            @endif

            @if(!empty($receipt_details->all_due))
                <tr>
                    <th>
                        {!! $receipt_details->all_bal_label !!}
                    </th>
                    <td>
                        {{$receipt_details->all_due}}
                    </td>
                </tr>
            @endif
        </table>

        {{$receipt_details->additional_notes}}
    </div>

    <div class="col-xs-6">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                    <th style="width:70%">
                        {!! $receipt_details->subtotal_label !!}
                    </th>
                    <td>
                        {{$receipt_details->subtotal}}
                    </td>
                </tr>

                <!-- Shipping Charges -->
                @if(!empty($receipt_details->shipping_charges))
                    <tr>
                        <th style="width:70%">
                            {!! $receipt_details->shipping_charges_label !!}
                        </th>
                        <td>
                            {{$receipt_details->shipping_charges}}
                        </td>
                    </tr>
                @endif

                <!-- Discount -->
                @if( !empty($receipt_details->discount) )
                    <tr>
                        <th>
                            {!! $receipt_details->discount_label !!}
                        </th>

                        <td>
                            (-) {{$receipt_details->discount}}
                        </td>
                    </tr>
                @endif

                <!-- Tax -->
                @if( !empty($receipt_details->tax) )
                    <tr>
                        <th>
                            {!! $receipt_details->tax_label !!}
                        </th>
                        <td>
                            (+) {{$receipt_details->tax}}
                        </td>
                    </tr>
                @endif

                <!-- Total -->
                <tr>
                    <th>
                        {!! $receipt_details->total_label !!}
                    </th>
                    <td>
                        {{$receipt_details->total}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


@if($receipt_details->show_barcode)
    <div class="row">
        <div class="col-xs-12">
            {{-- Barcode --}}
            <img class="center-block"
                 src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
        </div>
    </div>
@endif
<hr>
@if(!empty($receipt_details->footer_text))
    <div class="row">
        <div class="col-xs-12">
            {!! $receipt_details->footer_text !!}
        </div>
    </div>
@endif
