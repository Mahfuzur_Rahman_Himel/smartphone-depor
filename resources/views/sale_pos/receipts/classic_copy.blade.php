<!-- business information here -->

<div class="row invoice-info">
    <!-- Logo -->
    <div class="col-sm-4 invoice-col">
        @if(!empty(Session::get('business.logo')))
            <img style="width: 40%" src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}"
                 alt="Logo"
                 class="img img-responsive">
        @endif
    </div>
    <div class="col-sm-4 invoice-col">
        <address>
            <strong>
                @if(!empty($receipt_details->business_name))
                    {{$receipt_details->business_name}}
                @endif
            </strong>
            <br>
            @if(!empty($receipt_details->address))
                {!! $receipt_details->address !!}
            @endif

        </address>
    </div>

    <div class="col-sm-4 invoice-col text-right">
        <strong>Contact Information :</strong>
        <br> {!! $receipt_details->contact !!}
        @if(!empty($receipt_details->website))
            {{ $receipt_details->website }}
        @endif
    </div>
</div>

<div class="row">
    <!-- business information here -->
    <div class="col-xs-12 text-center">
        <!-- customer info -->
        <p style="width: 100% !important" class="word-wrap">
			<span class="pull-left text-left word-wrap">
                <b>Bill to:</b>
                <!-- customer info -->
                @if(!empty($receipt_details->customer_name))
                    <br/>
                    <b>{{ $receipt_details->customer_name }}</b> <br>
                @endif
                @if(!empty($receipt_details->customer_info))
                    {!! $receipt_details->customer_info !!}
                @endif
                @if(!empty($receipt_details->client_id_label))
                    <br/>
                    <b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
                @endif
                @if(!empty($receipt_details->customer_tax_label))
                    <br/>
                    <b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
                @endif
                @if(!empty($receipt_details->customer_custom_fields))
                    <br/>{!! $receipt_details->customer_custom_fields !!}
                @endif
                @if(!empty($receipt_details->sales_person_label))
                    <br/>
                    <b>{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
                @endif
			</span>

            <span class="pull-right text-left">
                @if(!empty($receipt_details->invoice_no_prefix))
                    <b>{!! $receipt_details->invoice_no_prefix !!}</b>
                @endif
                {{$receipt_details->invoice_no}}
                    <br>
				<b>Invoice {{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}
                    <br>
                <b>Payment Due :</b>
                    <span class="display_currency"
                          data-currency_symbol="true">{{$receipt_details->total_due}}</span>
                    <span class="display_currency" data-currency_symbol="true">
                                            @if(property_exists($receipt_details,'total_due'))
                            {{$receipt_details->total_due}}
                        @else
                            {{'0'}}
                        @endif
					</span>
			</span>
        </p>
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-xs-12">
        <br>
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>Items</th>
                <th>Quantity</th>
                <th>Price</th>
                <th class="text-right">Amount</th>
            </tr>
            </thead>
            <tbody>
            @forelse($p_array as $line)
                <tr>
                    <td style="word-break: break-all;">
                        {{$line['name']}}
                    </td>
                    <td>{{$line['quantity']}} {{$line['units']}} </td>
                    <td>{{$line['unit_price_inc_tax']}}</td>
                    <td class="text-right"><span id="payment_due_span" class="display_currency"
                                                 data-currency_symbol="true">{{$line['line_total']}}</span></td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table bg-gray">
                <tr>
                    <th>Sub Total:</th>
                    <td></td>
                    <td><span class="display_currency pull-right"
                              data-currency_symbol="true">{{ $receipt_details->subtotal }}</span></td>
                </tr>
                <tr>
                    <th>{{ __('sale.discount') }}:</th>
                    <td><b>(-)</b></td>
                    <td>
                        <span class=" pull-right display_currency pull-right"
                              data-currency_symbol="true">{{ $receipt_details->discount }}</span>
                    </td>
                </tr>
                <tr>
                    <th>{{ __('sale.order_tax') }}:</th>
                    <td><b>(+)</b></td>
                    <td class="text-right">
                       <span class=" pull-right display_currency pull-right"
                             data-currency_symbol="true"> {{$receipt_details->tax}}</span>
                    </td>
                </tr>
                <tr>
                    <th>{{ __('sale.shipping') }}: @if($receipt_details->shipping_details)
                            ({{$receipt_details->shipping_details}}
                            ) @endif <br> Track Number: @if($receipt_details->shipping_track_number)
                            ({{$receipt_details->shipping_track_number}}) @endif</th>
                    <td><b>(+)</b></td>
                    <td><span class="display_currency pull-right"
                              data-currency_symbol="true">{{ $receipt_details->shipping_charges }}</span></td>
                </tr>
                <tr>
                    <th>{{ __('sale.total_payable') }}:</th>
                    <td></td>
                    <td><span class=" pull-right display_currency pull-right"
                              data-currency_symbol="true">{{ $receipt_details->total }}</span></td>
                </tr>
                @foreach($receipt_details->payments as $payment)
                    <tr>
                        <th>Payment on {{$payment['date']}} using {{$payment['method']}}</th>
                        <td></td>
                        <td><span class="display_currency pull-right"
                                  data-currency_symbol="true">{{ $payment['amount'] }}</span></td>
                    </tr>
                @endforeach
                <tr>
                    <th>{{ __('sale.total_remaining') }}:</th>
                    <td></td>
                    <td><span class="display_currency pull-right"
                              data-currency_symbol="true">{{ $receipt_details->total_due }}</span></td>
                </tr>
            </table>
        </div>
    </div>
</div>

@if($haveVariable)
    <div class="row">
        <div class="col-xs-12">
            <br/><br/>
            <p>List of Sold Phones IMEI number(s) :</p>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>NAME</th>
                    <th>IMEI</th>
                    <th>COLOR</th>
                    <th>MEMORY</th>
                    <th>CONDITION</th>
                </tr>
                </thead>
                <tbody>
                @php($imei_index=1)
                @forelse($receipt_details->lines as $line)
                    @if($line['type']=='variable')
                        <tr>
                            <td>{{$imei_index++}}</td>
                            <td style="word-break: break-all;">
                                {{$line['product_custom_fields_1']}}
                            </td>
                            <td style="word-break: break-all;">
                                {{$line['variation']}}
                            </td>
                            <td style="word-break: break-all;">
                                {{$line['product_custom_fields_3']}}
                            </td>
                            <td style="word-break: break-all;">
                                {{$line['product_custom_fields_2']}}
                            </td>
                            <td style="word-break: break-all;">
                                {{$line['product_custom_fields_4']}}
                            </td>
                        </tr>
                    @endif
                @empty
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endif


@if($receipt_details->show_barcode)
    <div class="row">
        <div class="col-xs-12">
            {{-- Barcode --}}
            <img class="center-block"
                 src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
        </div>
    </div>
@endif

@if(!empty($receipt_details->footer_text))
    <div class="row">
        <div class="col-xs-12">
            {!! $receipt_details->footer_text !!}
        </div>
    </div>
@endif
