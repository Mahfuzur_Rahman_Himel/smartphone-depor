@extends('layouts.app')
@section('title', __('product.edit_product'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@lang('product.edit_product')</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(['url' => action('ProductController@update' , [$product->id] ), 'method' => 'PUT', 'id' => 'product_add_form',
                'class' => 'product_form', 'files' => true ]) !!}
        <input type="hidden" id="product_id" value="{{ $product->id }}">

        @component('components.widget', ['class' => 'box-primary'])
            <div class="row">


                <!--brand-->
                <div class="col-sm-4 @if(!session('business.enable_brand')) hide @endif">
                    <div class="form-group">
                        {!! Form::label('brand_id', __('product.brand') . ':') !!}
                        <div class="input-group">
                            {!! Form::select('brand_id', $brands, $product->brand_id, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
                            <span class="input-group-btn">
                    <button type="button" @if(!auth()->user()->can('brand.create')) disabled
                            @endif class="btn btn-default bg-white btn-flat btn-modal"
                            data-href="{{action('BrandController@create', ['quick_add' => true])}}"
                            title="@lang('brand.add_brand')" data-container=".view_modal"><i
                                class="fa fa-plus-circle text-primary fa-lg"></i></button>
                  </span>
                        </div>
                    </div>
                </div>
                <!--Model-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('product_custom_field1',  __('lang_v1.product_custom_field1') . ':') !!}
                        {!! Form::text('product_custom_field1', $product->product_custom_field1, ['class' => 'form-control', 'placeholder' => __('lang_v1.product_custom_field1')]); !!}
                    </div>
                </div>
                <!--Memory-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('product_custom_field2',  __('lang_v1.product_custom_field2') . ':') !!}
                        {!! Form::text('product_custom_field2', $product->product_custom_field2, ['class' => 'form-control', 'placeholder' => __('lang_v1.product_custom_field2')]); !!}
                    </div>
                </div>

                <div class="clearfix"></div>
                <!--Color-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('product_custom_field3',  __('lang_v1.product_custom_field3') . ':') !!}
                        {!! Form::text('product_custom_field3', $product->product_custom_field3, ['class' => 'form-control', 'placeholder' => __('lang_v1.product_custom_field3')]); !!}
                    </div>
                </div>
                <!--Condition-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('product_custom_field4',  __('lang_v1.product_custom_field4') . ':') !!}
                        {!! Form::text('product_custom_field4', $product->product_custom_field4, ['class' => 'form-control', 'placeholder' => __('lang_v1.product_custom_field4')]); !!}
                    </div>
                </div>
                <!--SKU-->
                <div class="col-sm-4 @if(!(session('business.enable_category') && session('business.enable_sub_category'))) hide @endif">
                    <div class="form-group">
                        {!! Form::label('sku', __('product.sku')  . ':*') !!} @show_tooltip(__('tooltip.sku'))
                        {!! Form::text('sku', $product->sku, ['class' => 'form-control',
                        'placeholder' => __('product.sku'), 'required', 'readonly']); !!}
                    </div>
                </div>

                <div class="clearfix"></div>
                <!--Category-->
                <div class="col-sm-4 @if(!session('business.enable_category')) hide @endif">
                    <div class="form-group">
                        {!! Form::label('category_id', __('product.category') . ':') !!}
                        {!! Form::select('category_id', $categories, $product->category_id, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
                    </div>
                </div>
                <!--Sub category-->
                <div class="col-sm-4 @if(!(session('business.enable_category') && session('business.enable_sub_category'))) hide @endif">
                    <div class="form-group">
                        {!! Form::label('sub_category_id', __('product.sub_category')  . ':') !!}
                        {!! Form::select('sub_category_id', $sub_categories, $product->sub_category_id, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
                    </div>
                </div>
                <!--Unit-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('unit_id', __('product.unit') . ':*') !!}
                        <div class="input-group">
                            {!! Form::select('unit_id', $units, $product->unit_id, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'required']); !!}
                            <span class="input-group-btn">
                    <button type="button" @if(!auth()->user()->can('unit.create')) disabled
                            @endif class="btn btn-default bg-white btn-flat quick_add_unit btn-modal"
                            data-href="{{action('UnitController@create', ['quick_add' => true])}}"
                            title="@lang('unit.add_unit')" data-container=".view_modal"><i
                                class="fa fa-plus-circle text-primary fa-lg"></i></button>
                  </span>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <!--Product Description-->
                <div class="col-sm-8">
                    <div class="form-group">
                        {!! Form::label('product_description', __('lang_v1.product_description') . ':') !!}
                        {!! Form::textarea('product_description', $product->product_description, ['class' => 'form-control']); !!}
                    </div>
                </div>
                <!--Product image-->
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('image', __('lang_v1.product_image') . ':') !!}
                        {!! Form::file('image', ['id' => 'upload_image', 'accept' => 'image/*']); !!}
                        <small>
                            <p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)])
                                . @lang('lang_v1.aspect_ratio_should_be_1_1') @if(!empty($product->image))
                                    <br> @lang('lang_v1.previous_image_will_be_replaced') @endif</p></small>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        @endcomponent


        <div class="row">
            <input type="hidden" name="submit_type" id="submit_type">
            <div class="col-sm-12">
                <div class="text-center">
                    <div class="btn-group">
                        @if($selling_price_group_count)
{{--                            <button type="submit" value="submit_n_add_selling_prices"--}}
{{--                                    class="btn btn-warning submit_product_form">@lang('lang_v1.save_n_add_selling_price_group_prices')</button>--}}
                        @endif

{{--                        <button type="submit" @if(empty($product->enable_stock)) disabled="true"--}}
{{--                                @endif id="opening_stock_button" value="update_n_edit_opening_stock"--}}
{{--                                class="btn bg-purple submit_product_form">@lang('lang_v1.update_n_edit_opening_stock')</button>--}}

{{--                        <button type="submit" value="save_n_add_another"--}}
{{--                                class="btn bg-maroon submit_product_form">@lang('lang_v1.update_n_add_another')</button>--}}

                        <button type="submit" value="submit"
                                class="btn btn-primary submit_product_form">@lang('messages.update')</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <!-- /.content -->

@endsection

@section('javascript')
    <script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
@endsection