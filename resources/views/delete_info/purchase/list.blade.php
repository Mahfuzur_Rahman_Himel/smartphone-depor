@extends('layouts.app')
@section('title','Delete Info-Purchase List')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        <h1>Purchase List
            <small>Delete Info</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">
        @component('components.widget', ['class' => 'box-primary', 'title' => 'All Deleted Purchase'])

            @can('purchase.view')
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="delete-purchase-table">
                        <thead>
                        <tr>
                            <th>Delete Date</th>
                            <th>Deleted By</th>
                            <th>Purchase @lang('messages.date')</th>
                            <th>@lang('purchase.ref_no')</th>
                            <th>@lang('purchase.supplier')</th>
                            <th>@lang('purchase.purchase_status')</th>
                            <th>@lang('purchase.payment_status')</th>
                            <th>@lang('purchase.grand_total')</th>
                            <th>@lang('purchase.payment_due')</th>
                            <th>Delete Status</th>
                            <th>@lang('messages.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($purchaseList as $purchase)
                            <tr>
                                <td>{{@format_datetime($purchase->created_at)}}</td>
                                <td>{{$purchase->hasDeleted->name}}</td>
                                <td>{{@format_datetime($purchase->transaction_date)}}</td>
                                <td>{{$purchase->invoice}}</td>
                                <td>{{$purchase->contact->name}}</td>
                                <td>{{$purchase->status}}</td>
                                <td>{{$purchase->payment_status}}</td>
                                <td><span class="display_currency" id="footer_purchase_total"
                                          data-currency_symbol="true">{{$purchase->grand_total}}</span></td>
                                <td><span class="display_currency" id="footer_purchase_total"
                                          data-currency_symbol="true">{{$purchase->payment_due}}</span></td>

                                <td>{{$purchase->current_status}}</td>
                                <td>
                                    <a href="#" data-href="{{url('delete-purchase-list-show/'.$purchase->id)}}" class="btn-modal label label-info" data-container=".view_modal" ><i class="fa fa-eye"></i> Details</a> &nbsp;
                                    <a onclick="return confirm('It will be Permanantly deleted. Perform this action?')" href="{{url('delete-purchase-list/'.$purchase->id)}}" class="label label-danger delete-purchase"><i class="fa fa-trash-o"></i>Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No Data Found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            @endcan
        @endcomponent
    </section>
    <!-- /.content -->
@stop

@section('javascript')
    <script>
        $('#delete-purchase-table').dataTable();
    </script>

@endsection