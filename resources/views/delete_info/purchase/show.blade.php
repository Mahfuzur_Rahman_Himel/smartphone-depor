<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

        <div class="modal-header no-print">
            <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title " id="modalTitle">Delete @lang('purchase.purchase_receipt')
                (<b>@lang('purchase.ref_no')
                    :</b>
                #{{ $purchase->invoice }})
            </h4>
        </div>
        <div class="modal-body">


            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    @if(!empty(Session::get('business.logo')))
                        <img src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}" alt="Logo"></span>
                    @endif
                </div>
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>{{ $business->name }}</strong>
                        @if(!empty($business->landmark))
                            <br>{{$business->landmark}}
                        @endif
                        @if(!empty($business->mobile))
                            <br>Tel: {{$business->mobile}}
                        @endif
                        @if(!empty($business->city) || !empty($business->state) || !empty($business->country))
                            <br>{{implode(',', array_filter([$business->city, $business->state, $business->country]))}}
                        @endif

                        @if(!empty($business->tax_number_1))
                            <br>{{$business->tax_label_1}}: {{$business->tax_number_1}}
                        @endif

                        @if(!empty($business->tax_number_2))
                            <br>{{$business->tax_label_2}}: {{$business->tax_number_2}}
                        @endif
                    </address>
                </div>
                <div class="col-sm-4 text-right invoice-col">
                    <strong>Contact Information :</strong>
                    @if(!empty($business->mobile))
                        <br>{{$business->mobile}}
                    @endif
                    @if(!empty($business->email))
                        <br>{{$business->email}}
                    @endif
                    @if(!empty($business->website))
                        <br>{{str_replace('http://','',$business->website)}}
                    @endif
                </div>
            </div>



            <div class="row invoice-info">
                <div class="col-sm-7 invoice-col-n">
                    @lang('purchase.supplier'):
                    <address>
                        <strong>{{ $purchase->contact->supplier_business_name }}</strong>
                        {{ $purchase->contact->name }}
                        @if(!empty($purchase->contact->landmark))
                            <br>{{$purchase->contact->landmark}}
                        @endif
                        @if(!empty($purchase->contact->city) || !empty($purchase->contact->state) || !empty($purchase->contact->country))
                            <br>{{implode(',', array_filter([$purchase->contact->city, $purchase->contact->state, $purchase->contact->country]))}}
                        @endif
                        @if(!empty($purchase->contact->tax_number))
                            <br>@lang('contact.tax_no'): {{$purchase->contact->tax_number}}
                        @endif
                        @if(!empty($purchase->contact->mobile))
                            <br>@lang('contact.mobile'): {{$purchase->contact->mobile}}
                        @endif
                        @if(!empty($purchase->contact->email))
                            <br>Email: {{$purchase->contact->email}}
                        @endif
                    </address>
                    @if($purchase->document_path)

                        <a href="{{$purchase->document_path}}"
                           download="{{$purchase->document_name}}" class="btn btn-sm btn-success pull-left no-print">
                            <i class="fa fa-download"></i>
                            &nbsp;{{ __('purchase.download_document') }}
                        </a>
                    @endif
                </div>
                <div class="col-sm-5 invoice-col-n">
                    <div class="col-sm-12" style="border: 1px solid black;">
                        <br>
                        <b>@lang('purchase.invoice'):</b> #{{ $purchase->invoice }}<br/>
                        <br>
                        <b>Invoice @lang('messages.date'):</b> {{ @format_date($purchase->transaction_date) }}<br/>
                        <b>Invoice @lang('purchase.amount'):</b>
                        <span class="display_currency"
                              data-currency_symbol="true">{{ $purchase->grand_total }}</span><br/>
                        <br> &nbsp;
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="table-responsive">
                        <table class="table bg-gray">
                            <thead>
                            <tr class="bg-blue">
                                <th>#</th>
                                <th>Deleted At</th>
                                <th>Deleted By</th>
                                <th>Item</th>
                                <th>IMEI</th>
                                <th>Memory</th>
                                <th>Color</th>
                                <th>Condition</th>
                                <th>Quantity</th>
                                <th>Price</th>

                            </tr>
                            </thead>

                            @foreach($purchase->transaction_items as $purchase_line)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{@format_datetime($purchase_line->deleted_at)}}</td>
                                    <td>{{$purchase_line->hasDeleted->name}}</td>
                                    <td>
                                        {{ $purchase_line->item_brand }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->item_imei }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->item_memory }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->item_color}}
                                    </td>
                                    <td>
                                        {{ $purchase_line->item_condition}}
                                    </td>
                                    <td>
                                        <span class="display_currency" data-is_quantity="true"
                                              data-currency_symbol="false">{{ $purchase_line->item_quantity.' '.$purchase_line->item_quantity }}</span>
                                    </td>
                                    <td class="text-right">
                                        <span class="display_currency"
                                              data-currency_symbol="true">{{$purchase_line->item_price}}</span>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <!--list if summary-->
            <br>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12"></div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>@lang('purchase.net_total_amount'):</th>
                                <td></td>
                                <td><span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $purchase->net_total }}</span></td>
                            </tr>
                            <tr>
                                <th>@lang('purchase.discount'):</th>
                                <td>
                                    <b>(-)</b>
                                </td>
                                <td>
                                    <span class="display_currency pull-right" data-currency_symbol="true">
                                        {{$purchase->discount}}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th>@lang('purchase.purchase_tax'):</th>
                                <td><b>(+)</b></td>
                                <td class="text-right">
                                    <span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $purchase->tax }}</span>

                                </td>
                            </tr>
                            @if( !empty( $purchase->shipping_charges ) )
                                <tr>
                                    <th>@lang('purchase.additional_shipping_charges'):</th>
                                    <td><b>(+)</b></td>
                                    <td>
                                        <span class="display_currency pull-right">{{ $purchase->shipping_charges }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <th>@lang('purchase.purchase_total'):</th>
                                <td></td>
                                <td><span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $purchase->grand_total }}</span></td>
                            </tr>
                            @if($purchase->grand_total==$purchase->payment_due)
                            @else
                                <tr>
                                    <th>Total Paid:</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $purchase->grand_total-$purchase->payment_due }}</span>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <th>Payment Due:</th>
                                <td></td>
                                <td><span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $purchase->payment_due }}</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!--list if payment info-->
            @if(sizeof($purchase->payments)!=0)
                <br>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <h4>{{ __('sale.payment_info') }}:</h4>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr class="bg-blue">
                                    <th>#</th>
                                    <th>{{ __('messages.date') }}</th>
                                    <th>{{ __('purchase.ref_no') }}</th>
                                    <th>{{ __('sale.amount') }}</th>
                                    <th>{{ __('sale.payment_mode') }}</th>
                                    <th>{{ __('sale.payment_note') }}</th>
                                    <th>Deleted Date</th>
                                    <th>Deleted By</th>
                                </tr>
                                @forelse($purchase->payments as $payment_line)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ @format_date($payment_line->paid_on) }}</td>
                                        <td>{{ $payment_line->ref_no }}</td>
                                        <td><span class="display_currency"
                                                  data-currency_symbol="true">{{ $payment_line->amount }}</span></td>
                                        <td>{{ $payment_line->payment_method }}</td>
                                        <td>@if($payment_line->payment_note)
                                                {{ ucfirst($payment_line->payment_note) }}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td>{{ @format_date($payment_line->deleted_at) }}</td>
                                        <td>{{$payment_line->hasDeleted->name }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            @lang('purchase.no_payments')
                                        </td>
                                    </tr>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
        @endif
        <!--list if shipping cost//additional info-->
            <br>
            <div class="row">
                @if($purchase->shipping_details)
                    <div class="col-sm-6">
                        <strong>@lang('purchase.shipping_details'):</strong><br>
                        <p class="well well-sm no-shadow bg-gray">
                            @if($purchase->shipping_details)
                                {{ $purchase->shipping_details }}
                            @else
                                --
                            @endif
                        </p>
                    </div>
                @endif
                {{--                @if($purchase->additional_notes)--}}
                {{--                    <div class="col-sm-6">--}}
                {{--                        <strong>@lang('purchase.additional_notes'):</strong><br>--}}
                {{--                        <p class="well well-sm no-shadow bg-gray">--}}
                {{--                            @if($purchase->additional_notes)--}}
                {{--                                {{ $purchase->additional_notes }}--}}
                {{--                            @else--}}
                {{--                                ----}}
                {{--                            @endif--}}
                {{--                        </p>--}}
                {{--                    </div>--}}
                {{--                @endif--}}
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var element = $('div.modal-xl');
        __currency_convert_recursively(element);
    });
</script>