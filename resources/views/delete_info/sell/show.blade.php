<div class="modal" id="{{$id}}">
    <div class=" modal-dialog modal-xl" role="document">
        <div class="modal-content">

            <div class="modal-header no-print">
                <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title " id="modalTitle">Deleted Sell Deteails (<b>Invoice No : </b>
                    #{{ $sell_info->invoice_no }})
                </h4>
            </div>
            <div class="modal-body">
                <div class="row invoice-info">
                    <div class="col-sm-7 invoice-col-n">
                        Bill To,
                        <address>
                            <strong>{{ $sell_info->contact->supplier_business_name }}</strong>
                            {{ $sell_info->contact->name }}
                            @if(!empty($sell_info->contact->landmark))
                                <br>{{$sell_info->contact->landmark}}
                            @endif
                            @if(!empty($sell_info->contact->city) || !empty($sell_info->contact->state) || !empty($sell_info->contact->country))
                                <br>{{implode(',', array_filter([$sell_info->contact->city, $sell_info->contact->state, $sell_info->contact->country]))}}
                            @endif
                            @if(!empty($sell_info->contact->tax_number))
                                <br>@lang('contact.tax_no'): {{$sell_info->contact->tax_number}}
                            @endif
                            @if(!empty($sell_info->contact->mobile))
                                <br>@lang('contact.mobile'): {{$sell_info->contact->mobile}}
                            @endif
                            @if(!empty($sell_info->contact->email))
                                <br>Email: {{$sell_info->contact->email}}
                            @endif
                        </address>
                        @if($sell_info->document)

                            <a href="{{$sell_info->document}}"
                               download="{{$sell_info->document}}"
                               class="btn btn-sm btn-success pull-left no-print">
                                <i class="fa fa-download"></i>
                                &nbsp;{{ __('sell.download_document') }}
                            </a>
                        @endif
                    </div>
                    <div class="col-sm-5 invoice-col-n">
                        <div class="col-sm-12" style="border: 1px solid black;">
                            <br>
                            <b>Invoice :</b> #{{ $sell_info->invoice_no }}<br/>
                            <br>
                            <b>Invoice @lang('messages.date') :</b> {{ @format_date($sell_info->transaction_date) }}
                            <br/>
                            <b>Invoice @lang('purchase.amount') :</b>
                            <span class="display_currency"
                                  data-currency_symbol="true">{{ $sell_info->final_total }}</span><br/>
                            <b>Payment Due :</b>
                            <span class="display_currency"
                                  data-currency_symbol="true">{{ $sell_info->total_due }}</span><br/>
                            <br> &nbsp;
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table bg-gray">
                                <thead>
                                <tr class="bg-blue">
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>IMEI</th>
                                    <th>Memory</th>
                                    <th>Color</th>
                                    <th>Condition</th>
                                    <th>Quantity</th>
                                    <th>Price</th>

                                </tr>
                                </thead>

                                @foreach($sell_info->sell_lines as $sell_info_line)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            {{ $sell_info_line->product->product_custom_field1 }}
                                        </td>
                                        <td>
                                            {{ $sell_info_line->variations->name }}
                                        </td>
                                        <td>
                                            {{ $sell_info_line->product->product_custom_field2 }}
                                        </td>
                                        <td>
                                            {{ $sell_info_line->product->product_custom_field3}}
                                        </td>
                                        <td>
                                            {{ $sell_info_line->product->product_custom_field4}}
                                        </td>
                                        <td>
                                            @php
                                                $unit_name = $sell_info_line->product->unit->short_name;
                                                if(!empty($sell_info_line->sub_unit)) {
                                                    $unit_name = $sell_line->sub_unit->short_name;
                                                }
                                            @endphp
                                            <span class="display_currency" data-is_quantity="true"
                                                  data-currency_symbol="false">{{ $sell_info_line->quantity.' '.$unit_name }}</span>
                                        </td>
                                        <td class="text-right">
                                        <span class="display_currency"
                                              data-currency_symbol="true">{{$sell_info_line->unit_price_inc_tax}}</span>
                                        </td>

                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <!--list if summary-->
                <br>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12"></div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>@lang('purchase.net_total_amount'):</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $sell_info->id }}</span></td>
                                </tr>
                                <tr>
                                    <th>@lang('purchase.discount'):</th>
                                    <td>
                                        <b>(-)</b>
                                    </td>
                                    <td>
                                        <span class="display_currency pull-right" data-currency_symbol="true">
                                            {{$sell_info->discount_amount}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>@lang('purchase.purchase_tax'):</th>
                                    <td><b>(+)</b></td>
                                    <td class="text-right">
                                        <span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $sell_info->tax_amount }}</span>

                                    </td>
                                </tr>
                                @if( !empty( $sell_info->shipping_charges ) )
                                    <tr>
                                        <th>@lang('purchase.additional_shipping_charges'):
                                            @if($sell_info->shipping_track_number)
                                                <br>(Track Number:{{$sell_info->shipping_track_number}})
                                            @endif
                                        </th>
                                        <td><b>(+)</b></td>
                                        <td>
                                            <span class="display_currency pull-right">{{ $sell_info->shipping_charges }}</span>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Sell Total:</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $sell_info->final_total }}</span></td>
                                </tr>
                                <tr>
                                    <th>Total Paid:</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $sell_info->total_paid }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Payment Due:</th>
                                    <td></td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $sell_info->total_due }}</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!--list if payment info-->
                @if(sizeof($sell_info->payment_lines)!=0)
                    <br>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <h4>{{ __('sale.payment_info') }}:</h4>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr class="bg-blue">
                                        <th>#</th>
                                        <th>{{ __('messages.date') }}</th>
                                        <th>{{ __('purchase.ref_no') }}</th>
                                        <th>{{ __('sale.amount') }}</th>
                                        <th>{{ __('sale.payment_mode') }}</th>
                                        <th>{{ __('sale.payment_note') }}</th>
                                    </tr>
                                    @forelse($sell_info->payment_lines as $payment_line)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ @format_date($payment_line->paid_on) }}</td>
                                            <td>{{ $payment_line->payment_ref_no }}</td>
                                            <td><span class="display_currency"
                                                      data-currency_symbol="true">{{ $payment_line->amount }}</span>
                                            </td>
                                            <td>{{ $payment_line->method }}</td>
                                            <td>@if($payment_line->note)
                                                    {{ ucfirst($payment_line->note) }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                @lang('purchase.no_payments')
                                            </td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>
                        </div>
                    </div>
            @endif
            <!--list if shipping cost//additional info-->
                <br>
                <div class="row">
                    @if($sell_info->shipping_details)
                        <div class="col-sm-6">
                            <strong>@lang('purchase.shipping_details'):</strong><br>
                            <p class="well well-sm no-shadow bg-gray">
                                @if($sell_info->shipping_details)
                                    {{ $sell_info->shipping_details }}
                                @else
                                    --
                                @endif
                            </p>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var element = $('div.modal-xl');
        __currency_convert_recursively(element);
    });
</script>