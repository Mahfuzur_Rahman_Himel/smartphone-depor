@extends('layouts.app')
@section('title','Delete Info-Sell Return List')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        <h1>Sell Return List
            <small>Delete Info</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">
        @component('components.widget', ['class' => 'box-primary', 'title' => 'All Deleted Sell Return'])

            @can('sell.view')
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="delete-sell-table">
                        <thead>
                        <tr>
                            <th>Delete Date</th>
                            <th>Deleted By</th>
                            <th>Sell Return @lang('messages.date')</th>
                            <th>Invoice</th>
                            <th>Parent Sale</th>
                            <th>Customer Name</th>
                            <th>Payment Status</th>
                            <th>Total Amount</th>
                            <th>Total Paid</th>
                            <th>Payment Due</th>
                            <th>@lang('messages.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($sellList as $sell)
                            @php
                                $tempVal=json_decode($sell->details);
                            @endphp
                            <tr>
                                <td>{{@format_datetime($sell->created_at)}}</td>
                                <td>{{$sell->hasDeleted->name}}</td>
                                <td>{{@format_datetime($tempVal->transaction_date)}}</td>
                                <td>{{$tempVal->invoice_no}}</td>
                                <td>{{$tempVal->contact->name}}</td>
                                <td>{{$tempVal->payment_status}}</td>
                                <td><span class="display_currency" id="footer_purchase_total"
                                          data-currency_symbol="true">{{$tempVal->final_total}}</span></td>
                                <td><span class="display_currency" id="footer_purchase_total"
                                          data-currency_symbol="true">{{$tempVal->total_paid}}</span></td>
                                <td><span class="display_currency" id="footer_purchase_total"
                                          data-currency_symbol="true">{{$tempVal->total_due}}</span></td>
                                <td>
                                    <a data-toggle="modal" data-target="#details_modal_{{$sell->id}}" class="btn-modal label label-info"><i
                                                class="fa fa-eye"></i> Details</a> &nbsp;
                                    <a onclick="return confirm('It will be Permanantly deleted. Perform this action?')"
                                       href="{{url('delete-sell-list/'.$sell->id)}}"
                                       class="label label-danger delete-purchase"><i
                                                class="fa fa-trash-o"></i>Delete</a>
                                    @include('delete_info.sell.show',['id'=>'details_modal_'.$sell->id,'sell_info'=>$tempVal])
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No Data Found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            @endcan
        @endcomponent
    </section>
    <!-- /.content -->
@stop

@section('javascript')
    <script>
        $('#delete-purchase-table').dataTable();
    </script>

@endsection