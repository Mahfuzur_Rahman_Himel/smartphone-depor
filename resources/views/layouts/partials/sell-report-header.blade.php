<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        @if(!empty(Session::get('business.logo')))
            <img style="width:40%" src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}"
                 alt="Logo"></span>
        @endif
    </div>
    <div class="col-sm-4 invoice-col">
        <address>
            <strong>{{ $sell->business->name }}</strong>
            @if(!empty($sell->location->landmark))
                <br>{{$sell->location->landmark}}
            @endif
            {{--            @if(!empty($sell->location->mobile))--}}
            {{--                <br>Tel:{{$sell->location->mobile}}--}}
            {{--            @endif--}}
            @if(!empty($sell->location->zip_code) || !empty($sell->location->city))
                <br>{{implode(', ', array_filter([$sell->location->city, $sell->location->zip_code]))}}
            @endif

            {{--            @if(!empty($sell->business->tax_number_1))--}}
            {{--                <br>{{$sell->business->tax_label_1}}: {{$sell->business->tax_number_1}}--}}
            {{--            @endif--}}

            {{--            @if(!empty($sell->business->tax_number_2))--}}
            {{--                <br>{{$sell->business->tax_label_2}}: {{$sell->business->tax_number_2}}--}}
            {{--            @endif--}}
        </address>
    </div>
    <div class="col-sm-4 text-right invoice-col">
        <strong>Contact Information :</strong>
        @if(!empty($sell->location->mobile))
            <br>{{$sell->location->mobile}}
        @endif
        @if(!empty($sell->location->email))
            <br>{{$sell->location->email}}
        @endif
        @if(!empty($sell->location->website))
            <br>{{str_replace('http://','',$sell->location->website)}}
        @endif
    </div>
</div>