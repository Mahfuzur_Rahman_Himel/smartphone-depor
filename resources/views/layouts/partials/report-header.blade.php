<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        @if(!empty(Session::get('business.logo')))
            <img src="{{ url( 'uploads/business_logos/' . Session::get('business.logo') ) }}" alt="Logo"></span>
        @endif
    </div>
    <div class="col-sm-4 invoice-col">
        <address>
            <strong>{{ $purchase->business->name }}</strong>
            @if(!empty($purchase->location->landmark))
                <br>{{$purchase->location->landmark}}
            @endif
            @if(!empty($purchase->location->mobile))
                <br>Tel: {{$purchase->location->mobile}}
            @endif
            @if(!empty($purchase->location->city) || !empty($purchase->location->zip_code))
                <br>{{implode(', ', array_filter([$purchase->location->city, $purchase->location->zip_code]))}}
            @endif

            @if(!empty($purchase->business->tax_number_1))
                <br>{{$purchase->business->tax_label_1}}: {{$purchase->business->tax_number_1}}
            @endif

            @if(!empty($purchase->business->tax_number_2))
                <br>{{$purchase->business->tax_label_2}}: {{$purchase->business->tax_number_2}}
            @endif
        </address>
    </div>
    <div class="col-sm-4 text-right invoice-col">
        <strong>Contact Information :</strong>
        @if(!empty($purchase->location->mobile))
            <br>{{$purchase->location->mobile}}
        @endif
        @if(!empty($purchase->location->email))
            <br>{{$purchase->location->email}}
        @endif
        @if(!empty($purchase->location->website))
            <br>{{str_replace('http://','',$purchase->location->website)}}
        @endif
    </div>
</div>