<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
        <div class="modal-header no-print">
            <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalTitle"> @lang('lang_v1.purchase_return_details')
                (<b>@lang('purchase.ref_no'):</b> #{{ $purchase->return_parent->ref_no ?? $purchase->ref_no}})
            </h4>
        </div>

        <div class="modal-body">
            @include('layouts.partials.report-header')
            <div class="row invoice-info">
                @if(!empty($purchase->return_parent))
                    <div class="col-sm-7 invoice-col-n">
                        @lang('purchase.supplier'):
                        <address>
                            <strong>{{ $purchase->contact->supplier_business_name }}</strong>
                            {{ $purchase->contact->name }}
                            @if(!empty($purchase->contact->landmark))
                                <br>{{$purchase->contact->landmark}}
                            @endif
                            @if(!empty($purchase->contact->city) || !empty($purchase->contact->state) || !empty($purchase->contact->country))
                                <br>{{implode(',', array_filter([$purchase->contact->city, $purchase->contact->state, $purchase->contact->country]))}}
                            @endif
                            @if(!empty($purchase->contact->tax_number))
                                <br>@lang('contact.tax_no'): {{$purchase->contact->tax_number}}
                            @endif
                            @if(!empty($purchase->contact->mobile))
                                <br>@lang('contact.mobile'): {{$purchase->contact->mobile}}
                            @endif
                            @if(!empty($purchase->contact->email))
                                <br>Email: {{$purchase->contact->email}}
                            @endif
                        </address>
                        @if($purchase->document_path)

                            <a href="{{$purchase->document_path}}"
                               download="{{$purchase->document_name}}"
                               class="btn btn-sm btn-success pull-left no-print">
                                <i class="fa fa-download"></i>
                                &nbsp;{{ __('purchase.download_document') }}
                            </a>
                        @endif
                    </div>
                    <div class="col-sm-5 invoice-col-n">
                        <div class="col-sm-12" style="border: 1px solid black;">
                            <br>
                            <b>Return @lang('purchase.invoice'):</b> #{{ $purchase->return_parent->ref_no }}<br/>
                            <br>
                            <b>Return Invoice @lang('messages.date')
                                :</b> {{ @format_date($purchase->return_parent->transaction_date) }}<br/>
                            <b>Return Invoice @lang('purchase.amount'):</b>
                            <span class="display_currency"
                                  data-currency_symbol="true">{{ $purchase->return_parent->final_total }}</span><br/>
                            @php
                                $p_date='';
                                $p_type='';
                            //dd($purchase->return_parent->payment_lines);
                            @endphp
                            @if($purchase->return_parent->payment_lines!=null)
                                @foreach($purchase->return_parent->payment_lines as $payment_line)
                                    @if(!$loop->first)
                                        @php $p_date.=', ';  $p_type.=', ' @endphp
                                    @endif
                                    @php
                                        $p_date.= $payment_line->paid_on;
                                        $p_type.= $payment_line->method;
                                    @endphp
                                @endforeach
                            @endif
                            <b>Payment @lang('messages.date'):</b> {{$p_date}}<br/>
                            <b>Payment Type:</b> {{$p_type}}
                            <br> &nbsp;
                        </div>
                    </div>
                @else
                    <div class="col-sm-6 col-xs-6">
                        <h4>@lang('lang_v1.purchase_return_details'):</h4>
                        <strong>@lang('lang_v1.return_date'):</strong> {{@format_date($purchase->transaction_date)}}<br>
                        <strong>@lang('purchase.supplier'):</strong> {{ $purchase->contact->name ?? '' }} <br>
                        <strong>@lang('purchase.business_location'):</strong> {{ $purchase->location->name }}
                    </div>
                @endif
            </div>
            <div class="row">
                @if(empty($purchase->return_parent))
                    @if($purchase->document_path)
                        <div class="col-md-12">
                            <a href="{{$purchase->document_path}}"
                               download="{{$purchase->document_name}}"
                               class="btn btn-sm btn-success pull-right no-print">
                                <i class="fa fa-download"></i>
                                &nbsp;{{ __('purchase.download_document') }}
                            </a>
                        </div>
                    @endif
                @endif
            </div>
            <!--list-->
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <table class="table bg-gray">
                        <thead>
                        <tr class="bg-blue">
                            <th>#</th>
                            <th>@lang('product.product_name')</th>
                            <th>Memory</th>
                            <th>Color</th>
                            <th>Condition</th>
                            <th>@lang('lang_v1.return_quantity')</th>
                            <th>@lang('sale.unit_price')</th>
                            <th>@lang('lang_v1.return_subtotal')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total_before_tax = 0;
                            $p_purchases=$purchase->purchase_lines->groupBy('product_id');
                            $key=1;
                        @endphp
                        @foreach($p_purchases as $purchase_line)
                            @php
                                $is_row_flag=false;
                                $line_quantity=0;
                                $line_total=0;
                            @endphp
                            @foreach($purchase_line as $p)
                                @if($p->quantity_returned != 0)
                                    @php
                                        $is_row_flag=true;

                                        $line_quantity+=$p->quantity_returned;
                                        $line_total+=($p->purchase_price_inc_tax * $p->quantity_returned);
                                    @endphp
                                @endif

                            @endforeach

                            @if($is_row_flag)
                                @php
                                    $total_before_tax += $line_total ;
                                    $unit_name = $purchase_line->first()->product->unit->short_name;
                                    if(!empty($purchase_line->first()->sub_unit)) {
                                      $unit_name = $purchase_line->first()->sub_unit->short_name;
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $key++ }}</td>
                                    <td>
                                        {{ $purchase_line->first()->product->product_custom_field1 }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->first()->product->product_custom_field2 }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->first()->product->product_custom_field3 }}
                                    </td>
                                    <td>
                                        {{ $purchase_line->first()->product->product_custom_field4 }}
                                    </td>
                                    <td>{{($line_quantity)}} {{$unit_name}}</td>
                                    <td><span class="display_currency" data-currency_symbol="true">
                                        {{ $purchase_line->first()->purchase_price_inc_tax }}</span>
                                    </td>
                                    <td class="text-right">
                                        <span class="display_currency"
                                              data-currency_symbol="true">{{$line_total}}</span>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!--list if summary-->
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6 col-xs-6 col-xs-offset-6">
                    <table class="table">
                        <tr>
                            <th>@lang('purchase.net_total_amount'):</th>
                            <td></td>
                            <td><span class="display_currency pull-right"
                                      data-currency_symbol="true">{{ $total_before_tax }}</span></td>
                        </tr>

                        <tr>
                            <th>@lang('lang_v1.total_return_tax'):</th>
                            <td><b>(+)</b></td>
                            <td class="text-right">
                                @if(!empty($purchase_taxes))
                                    @foreach($purchase_taxes as $k => $v)
                                        <strong>
                                            <small>{{$k}}</small>
                                        </strong> - <span class="display_currency pull-right"
                                                          data-currency_symbol="true">{{ $v }}</span><br>
                                    @endforeach
                                @else
                                    0.00
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td></td>
                            <td><span class="display_currency pull-right"
                                      data-currency_symbol="true">{{ $purchase->return_parent->final_total ??  $purchase->final_total }}</span>
                            </td>
                        </tr>
                        @if($purchase->return_parent->payment_lines!=null)
                            <tr>
                                <th>Total Return:</th>
                                <td></td>
                                <td></td>
                            </tr>
                            @php $ttlP=0; @endphp
                            @foreach($purchase->return_parent->payment_lines as $payment_line)
                                @php $ttlP+=$payment_line->amount; @endphp
                                <tr>
                                    <td colspan="2">Paid on {{$payment_line->paid_on}} by {{$payment_line->method}}</td>
                                    <td><span class="display_currency pull-right"
                                              data-currency_symbol="true">{{ $payment_line->amount }}</span>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <th>Return Due:</th>
                                <td></td>
                                <td><span class="display_currency pull-right"
                                          data-currency_symbol="true">{{ $purchase->return_parent->final_total- $ttlP }}</span>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <!--list if imei-->
            <br>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="table-responsive">
                        <p class="text-bold">List of Purchased Phones IMEI:</p>
                        <table class="table bg-gray">
                            <thead>
                            <tr class="bg-blue">
                                <th>#</th>
                                <th>Model</th>
                                <th>IMEI</th>
                                <th>Memory</th>
                                <th>Color</th>
                                <th>Condition</th>
                            </tr>
                            </thead>
                            @foreach($purchase->purchase_lines as $purchase_line)
                                @if( $purchase_line->product->type == 'variable')
                                    @if($purchase_line->quantity_returned == 0)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            {{ $purchase_line->product->product_custom_field1 }}
                                        </td>
                                        <td>
                                            {{ $purchase_line->variations->name}}
                                        </td>
                                        <td>
                                            {{ $purchase_line->product->product_custom_field2 }}
                                        </td>
                                        <td>
                                            {{ $purchase_line->product->product_custom_field3 }}
                                        </td>
                                        <td>
                                            {{ $purchase_line->product->product_custom_field4 }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-primary no-print" aria-label="Print"
                        onclick="$(this).closest('div.modal-content').printThis();"><i
                            class="fa fa-print"></i> @lang( 'messages.print' )
                </button>
                <button type="button" class="btn btn-default no-print"
                        data-dismiss="modal">@lang( 'messages.close' )</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var element = $('div.modal-xl');
            __currency_convert_recursively(element);
        });
    </script>